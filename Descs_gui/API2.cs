﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Descs_gui
{
    class API2
    {
        string[] Paths = new string[4];

        public API2(string[] P)
        {
            Paths = P;
        }

        //--------API2---------
        public string untaggged_path = "untaggged";
        public List<string> Noignore = new List<string>();
        
        private void MakeResultOne(string r_in, string[] plastins, string folderres, TextBox textBox1)
        {
            string b = GetSortAlbumArtist(r_in);
            Console.WriteLine("r_in= " + r_in);
            if (b == null) return;
            bool isfound = false;
            for (int i = 0; i < plastins.Length; i++)
            {

                //string[] s1 = plastins[i].Split('\\');
                string s2 = Path.GetFileName(plastins[i]);
                s2 = s2.Substring(0, s2.LastIndexOf('.'));
                if (s2.ToUpper() == b.ToUpper())
                {
                    Console.WriteLine("Found");
                    isfound = true;
                    Console.WriteLine(plastins[i]);
                    string datefolder = GetSortAlbumArtist(r_in, true);
                    string newfile = Path.Combine(Paths[2], datefolder, Path.GetFileName(plastins[i]));

                    string np = Path.Combine(Paths[2], datefolder);

                    try
                    {
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Работа с папкой " + np + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                        DirectoryInfo di = Directory.CreateDirectory(np);
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Копирование файла  " + plastins[i] + " в файл  " + newfile + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                        File.Copy(plastins[i], newfile, true);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Ошибка каталога " + np, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Console.WriteLine(e.ToString());
                        continue;
                    }
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Копирование тегов  в файл" + newfile + "\n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                    CopyMp3Tag(r_in, newfile);
                }


            }
            if (!isfound)
            {
                //copy to in=gnored folder
                Console.WriteLine("Not Found");

                /*Console.WriteLine("ignorefile= " + r_in);
                string[] newpathes = r_in.Split('\\');
                string np = newpathes[newpathes.Length - 1];
                string ignorefile = Paths[3] + "\\" + np;
                File.Copy(r_in, ignorefile, true);
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Проигнорированный файл  " + r_in + "\n"); });*/
            }



        }

        private string GetSortAlbumArtist(string file, bool isgroup = false)
        {
            try
            {
                var audioFile = TagLib.File.Create(@file);
                string res = audioFile.Tag.AlbumArtistsSort[0] + "-" + audioFile.Tag.Track;
                //string res = audioFile.Tag.ComposersSort[0] + "-" + audioFile.Tag.Track;
                if (isgroup) res = audioFile.Tag.Grouping.Replace("decks.de pre-listen", "").Trim(); ;
                Console.WriteLine(res);
                return res;
            }
            catch (Exception e)
            {
                //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.ToString());
                return null;
            }

            return null;
        }

        private string[] GetFiles(string folder, string tbFilter_1, string tbFilter_2,  int n_fold = 1)
        {
            string filter = "*";
            if (n_fold == 1) filter = tbFilter_1;
            else filter = tbFilter_2;


            string[] dirs = null;
            try
            {
                dirs = Directory.GetFiles(folder, filter, SearchOption.AllDirectories);
                Console.WriteLine("The number of files is {0}.", dirs.Length);
                foreach (string dir in dirs)
                {
                    Console.WriteLine(dir);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("The process failed: {0}", ex.ToString());
                MessageBox.Show("Не найден путь " + folder, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            return dirs;
        }

        private void CopyMp3Tag(string a, string b)
        {
            try
            {
                var a_audioFile = TagLib.File.Create(@a);
                var b_audioFile = TagLib.File.Create(@b);
                b_audioFile.Tag.Album = a_audioFile.Tag.Album;
                b_audioFile.Tag.Track = a_audioFile.Tag.Track;
                b_audioFile.Tag.Artists = a_audioFile.Tag.Artists;
                //audioFile.Tag.JoinedPerformersSort  - sort artist
                //artwork
                b_audioFile.Tag.Pictures = a_audioFile.Tag.Pictures;
                //--------------
                b_audioFile.Tag.Genres = a_audioFile.Tag.Genres;
                b_audioFile.Tag.Lyrics = a_audioFile.Tag.Lyrics;
                b_audioFile.Tag.Title = a_audioFile.Tag.Title;
                b_audioFile.Tag.ComposersSort = a_audioFile.Tag.ComposersSort;
                b_audioFile.Tag.AlbumArtistsSort = a_audioFile.Tag.AlbumArtistsSort;
                b_audioFile.Tag.AlbumSort = a_audioFile.Tag.AlbumSort;
                b_audioFile.Tag.Year = a_audioFile.Tag.Year;
                b_audioFile.Tag.Grouping = a_audioFile.Tag.Grouping.Replace("decks.de pre-listen", "full").Trim();
                b_audioFile.Tag.PerformersSort = a_audioFile.Tag.PerformersSort;
                b_audioFile.Tag.Comment = "";
                b_audioFile.Tag.AlbumArtists = GetAlbumArtists(a_audioFile.Tag.AlbumArtists);
                b_audioFile.Save();
            }
            catch (Exception e)
            {
                //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.ToString());
            }
        }

        private string[] GetAlbumArtists(string[] albumartist)
        {
            string s= String.Join("/", albumartist);
            return new string[] { s };
        }

        public void StartDo(TextBox textBox1, TextBox tbFilter_1, TextBox tbFilter_2, ProgressBar progressBar1)
        {
            try
            {
                //list files in folder 1
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Чтение файлов в папке " + Paths[0] + "\n"); });
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                string[] f1 = GetFiles(Paths[0], tbFilter_1.Text, tbFilter_2.Text, 1);
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Чтение файлов в папке " + Paths[1] + "\n"); });
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                string[] f2 = GetFiles(Paths[1], tbFilter_1.Text, tbFilter_2.Text, 2);

                progressBar1.Invoke((MethodInvoker)delegate { progressBar1.Maximum = f1.Length; });
                progressBar1.Invoke((MethodInvoker)delegate { progressBar1.Value = 0; });
                //string[] f3 = GetFiles(Paths[2]);

                //Console.WriteLine(f1[0]);
                //MakeResultOne(f1[0], f2, Paths[2]);
                foreach (string s in f1)
                {
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Обработка файла  " + s + "\n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });
                    MakeResultOne(s, f2, Paths[2], textBox1);
                    progressBar1.Invoke((MethodInvoker)delegate { progressBar1.PerformStep(); });
                    //progressBar1.PerformStep();

                }

                string[] f3 = GetFiles(Paths[2], tbFilter_1.Text, tbFilter_2.Text, 2);
                foreach (string s2 in f2)
                {
                    bool isignore = true;
                    //Console.WriteLine("GetFileName2= " + Path.GetFileName(s2));
                    string s2_name = Path.GetFileName(s2);

                    foreach (string s3 in f3.Where(s3 => Path.GetFileName(s3) == s2_name))
                    //foreach (string s3 in f3)
                    {
                        //Console.WriteLine("GetFileName3= " + Path.GetFileName(s3));
                        isignore = false;
                        break;


                    }
                    if (isignore)
                    {
                        Console.WriteLine("ignorefile= " + s2);

                        string ignorefile = Path.Combine(Paths[3], Path.GetFileName(s2));
                        File.Copy(s2, ignorefile, true);
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Проигнорированный файл  " + s2 + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("-----------------------------------------\n"); });

                    }

                }



                MessageBox.Show("Готово", "Завершено", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Noignore.Clear();
            }
        }
    }
}
