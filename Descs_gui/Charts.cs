﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Data.SQLite;
using System.Data.Common;
using System.Data;
using TagLib;
using System.ComponentModel;
using System.Threading;

namespace Descs_gui
{
    class Charts
    {
        private string[] genre = { "house", "techno", "minimal", "Итого:" };
        private string[] sub = { "Buzz", "Sales (3 wks)", "Vorverkauf", "All Times" };
        private Dictionary<string, string> sub_map = new Dictionary<string, string> {
            { "buzz", "Buzz"},
            { "sales", "Sales (3 wks)"},
            { "presale", "Vorverkauf"},
            { "alltime", "All Times"},
         };
        private Random rnd = new Random();
        private string pathDownloadFromList = Properties.Settings.Default.Path_from_list_dir;
        private uint loaded_count = 0;
        private int loaded_count_rel = 0;
        const string Path_Artworks = "Artworks";
        private int count_run_bws = 0;
        private List<BackgroundWorker> BWS = new List<BackgroundWorker>();

        public const int countrows = 13;
        private string NameBD = Properties.Settings.Default.DefaultDB;

        public int CountRows{ get { return countrows; } }
        private bool isstop = false;

        public void killthreads()
        {
            foreach (var b in BWS) {
                b.CancelAsync();
            }
            isstop = true;
        }

        private List<string> errorlist = new List<string>();

        private void SetMp3Tags(string file, string album, string[] artists, string artwork, string[] genres, string lyrics, string name,
                               string[] sort_composer, string[] albumArtistsSort, string albumSort, uint year, string grouping,
                               string[] sort_artist, uint track_no, string[] albumartist)
        {
            /*  Info of tags
            albom:		audioFile.Tag.Album	"albom_333"	string
            artist:		-		Artists	{string[1]}	string[]
            artwork:		-		Pictures	{TagLib.Id3v2.AttachedPictureFrame[1]}	TagLib.IPicture[] {TagLib.Id3v2.AttachedPictureFrame[]}
            Genre:-		Genres	{string[1]}	string[]  (		JoinedGenres	"Alternative"	string)
            Lyrics:		Lyrics	"Lyrics_333"	string
            Name:		Title	"name_333"	string
            Sort composer: -		ComposersSort	{string[1]}	string[]
            sort album artist:-		AlbumArtistsSort	{string[1]}	string[]
            sort album:		AlbumSort	"Sort_albom_333"	string
            year:		Year	1816	uint
            grouping:		Grouping	"grouping_333"	string
            sort artist:		PerformersSort	"sort_artist_333"	string

            */
            try
            {
                var audioFile = TagLib.File.Create(@file);
                audioFile.Tag.Album = album;
                audioFile.Tag.Track = track_no;
                audioFile.Tag.Artists = artists;
                //audioFile.Tag.JoinedPerformersSort  - sort artist
                //artwork
                try
                {
                    IPicture newArt = new Picture(artwork);
                    audioFile.Tag.Pictures = new IPicture[1] { newArt };
                }
                catch (Exception e)
                {
                    //tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n-----------------------------\n"); });
                    //tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНе удалось загрузить artwork=" + artwork + "\n"); });
                    //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(e.ToString());
                }

                //--------------
                audioFile.Tag.Genres = genres;
                audioFile.Tag.Lyrics = lyrics;
                audioFile.Tag.Title = name;
                audioFile.Tag.ComposersSort = sort_composer;
                audioFile.Tag.AlbumArtistsSort = albumArtistsSort;
                audioFile.Tag.AlbumSort = albumSort;
                audioFile.Tag.Year = year;
                audioFile.Tag.Grouping = grouping;
                audioFile.Tag.PerformersSort = sort_artist;
                audioFile.Tag.Comment = "";
                audioFile.Tag.AlbumArtists = albumartist;
                audioFile.Save();
            }
            catch (Exception e)
            {
                //tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText(e.ToString()); });

                //tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n-----------------------------\n"); });
                //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.ToString());
                /*count_errors++;
                lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });*/
                try
                {
                    using (StreamWriter sw = System.IO.File.AppendText("Download_error.txt"))
                    {
                        sw.WriteLine(e.ToString());
                        sw.WriteLine("--------------------------\n");
                    }
                }
                catch { }

            }

        }

        private string[] GetArrfromstr(string s)
        {
            string[] a = new string[] { s };
            return a;
        }

        private string getDirToSave(string newdate, bool is_artworks = false, bool islist = false)
        {
            string subdir = "";
            if (islist)
                subdir = pathDownloadFromList;
            else
            {
                string myeardir = newdate.Substring(2, 2) + newdate.Substring(5, 2);
                //newdate.ToString("yyMM");
                if (is_artworks)
                {
                    subdir = Path.Combine(myeardir, Path_Artworks);
                }
                else
                {
                    subdir = myeardir;
                }
            }


            DirectoryInfo di = Directory.CreateDirectory(Path.Combine(Properties.Settings.Default.Path_out_MP3, subdir));
            return di.FullName;
        }

        private string Download_one(string url, string new_date, bool is_artwork = false, bool islist = false)
        {
            string newfilepath = "";
            try
            {

                string dir_save = getDirToSave(new_date, is_artwork, islist);
                using (var client = new System.Net.WebClient())
                {
                    string link = url;
                    string[] splitlink = link.Split('/');

                    newfilepath = Path.Combine(dir_save, splitlink[splitlink.Length - 1]);
                    //autoriation
                    client.DownloadFile(link, newfilepath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Не удалось загрузить файл\n");

                /*{
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНе удалось загрузить файл " + url); });
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n----------------------\n"); });

                    lErrFromList.Text = "Список ошибок: ";

                    count_errors++;
                    lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });
                }*/


                //MessageBox.Show("Не найдена ссылка " + url, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Console.WriteLine(e.ToString());
                try
                {
                    using (StreamWriter sw = System.IO.File.AppendText("Download_error.txt"))
                    {
                        sw.WriteLine("\nНе удалось загрузить файл " + url);
                        sw.WriteLine("\n--------------------------");
                        sw.WriteLine(e.ToString());
                    }
                }
                catch { }

            }
            return newfilepath;
        }

        public void DefaultTable(DataGridView dgv)
        {
            for (int i = 0; i < countrows; i++) dgv.Rows.Add();

            int i_genre = -1, i_sub = 0;
            for (int i = 0; i < countrows; i++)
            {
                //add genre
                if (i % 4 == 0)
                {
                    i_genre++;
                }
                dgv.Rows[i].Cells[0].Value = genre[i_genre];
                //add sub
                if (i != countrows - 1) dgv.Rows[i].Cells[1].Value = sub[i_sub];
                if (i_sub > sub.Length - 2) i_sub = 0;
                else i_sub++;
            }
        }

        Process process;

        private int getrow(DataGridView dgv, string genre_, string sub_)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["Charts_genre"].Value.ToString().Equals(genre_) &&
                    row.Cells["Charts_sub"].Value.ToString().Equals(sub_)
                    )
                {
                    return row.Index;
   
                }
            }
            return 0;
        }

        private string GetnoTrack(SQLiteConnection connection, string genre, string sub, string limit)
        {
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                command.CommandText = String.Format(@"select count(*) count_tracks
                                            from Charttracks t
                                            where t.url_release in 
                                                (select tag.url
                                                from Charttag tag
                                                where tag.category = '{0}'
                                                        and tag.subcategory = '{1}'
                                                        order by tag.chartpos limit {2} )", genre, sub, limit);
                command.CommandType = CommandType.Text;
                Console.WriteLine(command.CommandText);
                SQLiteDataReader r = command.ExecuteReader();
                    
                string count_tr;
                while (r.Read())
                {
                    count_tr = r["count_tracks"].ToString();
                    Console.WriteLine(String.Format("count_tr= {0}", count_tr));
                    return count_tr;
                }
            }
            
            return "";
        }

        public void SelectData(DataGridView dgv, bool all = true)
        {
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + NameBD;
                    connection.Open();

                    if (all)
                    {
                        using (SQLiteCommand command = new SQLiteCommand(connection))
                        {
                            command.CommandText = @"select count(t.url_release) count_release, t.category,
                                    t.subcategory, sum(t.count_tracks) count_tracks
                                from
                                    (select tag.url url_release,
                                             tag.category, tag.subcategory,
                                             count(tr.url) count_tracks
                                    from Charttag tag, Charttracks tr
                                    where tag.url = tr.url_release
                                    group by tag.url, tag.category, tag.subcategory) t
                                group by t.category, t.subcategory ";
                            command.CommandType = CommandType.Text;
                            SQLiteDataReader r = command.ExecuteReader();
                            Console.WriteLine(command.CommandText);
                            string genre_, sub_, no_releases_, no_tracks_;
                            while (r.Read())
                            {
                                genre_ = r["category"].ToString();
                                sub_ = sub_map[r["subcategory"].ToString()];
                                no_releases_ = r["count_release"].ToString();
                                no_tracks_ = r["count_tracks"].ToString();

                                Console.WriteLine(String.Format("Genre= {0}; sub= {1}; no_release= {2}, no_track= {3}",
                                    genre_, sub_, no_releases_, no_tracks_));

                                var irow = getrow(dgv, genre_, sub_);
                                dgv.Rows[irow].Cells[2].Value = no_releases_;
                                dgv.Rows[irow].Cells[3].Value = no_tracks_;
                            }
                            
                        }


                    }
                    else
                    {
                        string count_tr;
                        foreach (var g in genre)
                        {
                            if (g == "Итого:") continue;
                            foreach (var d in sub_map)
                            {
                                var irow = getrow(dgv, g, d.Value);
                                var limit = dgv.Rows[irow].Cells[4].Value.ToString();
                                count_tr = GetnoTrack(connection, g, d.Key, limit);
                                dgv.Rows[irow].Cells[5].Value = count_tr;
                                /*if (bwCalcNumberrelease.CancellationPending) return;
                                CreateSelectTracks_Users(connection, g, s, "new");
                                //pDownload.PerformStep();
                                pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });*/
                            }
                            /*if (bwCalcNumberrelease.CancellationPending) return;
                            CreateSelectTracks_Users(connection, g, "back", "classix", true);
                            //pDownload.PerformStep();
                            pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });*/
                        }
                        //CreateSelectTracks_Users(connection,"house", "back", "classix", true);
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Не верный путь к БД", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            for (int i = 2; i < 6; i++)
            {
                int sum = 0;
                for (int j = 0; j < countrows - 1; j++)
                    sum += Convert.ToInt32(dgv.Rows[j].Cells[i].Value);
                dgv.Rows[countrows - 1].Cells[i].Value = sum.ToString();
            }
        }

        public void DownloadMp3(DataGridView dgv, string category, string subcategory, System.Windows.Forms.TextBox textBox1)
        {
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection conn = (SQLiteConnection)factory.CreateConnection())
            {
                conn.ConnectionString = @"Data Source = " + NameBD;
                conn.Open();

                var irow = getrow(dgv, category, sub_map[subcategory]);
                string limit = dgv.Rows[irow].Cells[4].Value == null ? "0" : dgv.Rows[irow].Cells[4].Value.ToString();
                string oldurl_release = null;
                string rndnew = rnd.Next(1, 100000000).ToString();
                using (SQLiteCommand command = new SQLiteCommand(conn))
                {
                    command.CommandText = String.Format(@"select Charttracks.url url,Charttracks.track_artist track_artist,
                    Charttracks.url_release url_release, Charttag.artwork artwork,  Charttag.title title
                     , Charttag.artist artist,Charttag.genre genres, Charttag.sub sub, Charttag.info info,Charttracks.name  name, Charttracks.no_track no_track, 
                     Charttag.new new, Charttag.catno catno, Charttag.label label, Charttag.year year, Charttag.grouping grouping 
                     from Charttracks, Charttag 
                     where Charttag.url=Charttracks.url_release 
                     and  Charttracks.url_release in ( select  url 
                                                             From Charttag mp 
                                                             where  category='{0}' and subcategory = '{1}' order by mp.chartpos limit {2})

                   ", category, subcategory, limit);
                    Console.WriteLine(command.CommandText);
                    SQLiteDataReader r = command.ExecuteReader();

                    while (r.Read())
                    {
                        if (isstop) return;

                        string sortcomposer = "";
                        try
                        {
                            string comp = r["new"].ToString();
                            sortcomposer = Convert.ToDateTime(r["new"]).ToString("yyyy.MM.dd");
                        }
                        catch
                        {
                            /*tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНеверный sortcomposer " + Convert.ToString(r["url"])); });
                            tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n----------------------\n"); });
                            count_errors++;
                            lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });*/
                            string Error = String.Format("sortcomposer Error {0}, category = {1}, subcategory = {2}",
                                Convert.ToString(r["url_release"]), category, subcategory);
                            errorlist.Add(Error);
                            Console.WriteLine(Error);
                            continue;
                        }

                        string log = String.Format("\n Download_one url = {0}, sortcomposer = {1}", Convert.ToString(r["url"]), sortcomposer);
                        Console.WriteLine(String.Format(" Download_one url = {0}, sortcomposer = {1}", Convert.ToString(r["url"]), sortcomposer));
                        //download mp3
                        string mp3_path = Download_one(Convert.ToString(r["url"]), sortcomposer);
                        if (mp3_path == "")
                        {
                            string Error = String.Format("Downloa Error {0}, category = {1}, subcategory = {2}",
                                Convert.ToString(r["url_release"]), category, subcategory);
                            errorlist.Add(Error);
                            continue;
                        }
                        Console.WriteLine("Url " + Convert.ToString(r["url"]));
                        //
                        string url_release = Convert.ToString(r["url_release"]);
                        Console.WriteLine("Url_Release " + url_release);
                        Console.WriteLine("Artwork " + Convert.ToString(r["url_release"]));

                        //download artwork
                        string artworks_path = Download_one(Convert.ToString(r["artwork"]), sortcomposer, true);
                        Console.WriteLine(artworks_path);
                        //Set mp3 tags
                        Console.WriteLine("filepath= " + mp3_path);
                        //album
                        string album_ = Convert.ToString(r["title"]);

                        if (album_.ToUpper().Trim() == "V/A" || album_.ToUpper().Trim() == "Various Artists".ToUpper())
                            album_ = "Various Artists";
                        else if (album_.Split('/').Length > 1)
                            album_ = album_.Split('/')[0];

                        Console.WriteLine("album= " + album_);

                        string artist = Convert.ToString(r["track_artist"]) == "" ? Convert.ToString(r["artist"]) : Convert.ToString(r["track_artist"]);

                        Console.WriteLine("artists= " + GetArrfromstr(artist)[0]);
                        Console.WriteLine("artwork= " + artworks_path);
                        //genre
                        string genre = Convert.ToString(r["genres"]);
                        string sub_genre = Convert.ToString(r["sub"]);
                        string res_genre = "";
                        if (genre.Trim(' ') == "")
                            res_genre = sub_genre;
                        else res_genre = genre + ", " + sub_genre;
                        Console.WriteLine("genres= " + GetArrfromstr(res_genre)[0]);
                        Console.WriteLine("lyrics= " + Convert.ToString(r["info"]));
                        Console.WriteLine("name= " + Convert.ToString(r["name"]));

                        string albumsort = Convert.ToString(r["label"]);

                        Console.WriteLine("sort_composer= " + GetArrfromstr(sortcomposer)[0]);
                        //sort album artist
                        string sortalbumartist = Convert.ToString(r["catno"]).Trim();
                        Console.WriteLine("albumArtistsSort= " + GetArrfromstr(sortalbumartist)[0]);
                        Console.WriteLine("albumSort= " + albumsort);
                        Console.WriteLine("year= " + Convert.ToString(r["year"]));
                        Console.WriteLine("grouping= " + Convert.ToString(r["grouping"]));
                        //string[] Cur_track = GetArrfromstr(Properties.Settings.Default.Current_track.ToString());

                        if (oldurl_release != null && oldurl_release != url_release)
                            rndnew = rnd.Next(1, 100000000).ToString();

                        oldurl_release = url_release;

                        string[] Cur_track = GetArrfromstr(rndnew);

                        Console.WriteLine("sort_artist= " + Cur_track);
                        //track NO
                        /*if (prev_album != null && prev_album == album_) track_no_++;
                        else track_no_ = 1;
                        prev_album= album_;*/
                        uint track_no_ = 0;
                        try
                        {
                            track_no_ = Convert.ToUInt32(r["no_track"].ToString());
                        }
                        catch
                        {
                            track_no_ = 0;
                        }
                        Console.WriteLine("track_no_= " + track_no_);

                        //--
                        /*SetMp3Tags(file: mp3_path, album: album_, artists: GetArrfromstr(artist),
                            artwork: artworks_path, genres: GetArrfromstr(res_genre),
                            lyrics: Convert.ToString(r["info"]), name: Convert.ToString(r["name"]),
                            sort_composer: GetArrfromstr(sortcomposer),
                            albumArtistsSort: GetArrfromstr(sortalbumartist), albumSort: albumsort,
                            year: Convert.ToUInt32(r["year"]), grouping: Convert.ToString(r["grouping"]), sort_artist: Cur_track,
                            track_no: track_no_, albumartist: GetArrfromstr(url_release)
                        );*/
                        //save curent NO
                        /*Properties.Settings.Default.Current_track++;
                        Properties.Settings.Default.Save();*/

                        loaded_count++;

                        //lb_ready.Invoke((MethodInvoker)delegate { lb_ready.Text = loaded_count.ToString(); });
                        
                    }

                }
            }
            //Console.WriteLine(String.Format("Demon {0} finished", ibw));
            count_run_bws--;
            Console.WriteLine(String.Format("count_run_bws = {0} ", count_run_bws));
        }

        public void DownloadAll(DataGridView dgv, System.Windows.Forms.TextBox textBox1, ProgressBar pDownload, Label lberror)
        {
            textBox1.Invoke((MethodInvoker)delegate { textBox1.Clear(); });
            isstop = false;
            lberror.Invoke((MethodInvoker)delegate { lberror.Text = ""; });
            loaded_count = 0;
            loaded_count_rel = 0;
            errorlist.Clear();
            SelectData(dgv, false);
            SelectData(dgv);
            //Progress bar

            int max_download = Convert.ToInt32(dgv.Rows[countrows - 1].Cells[5].Value);
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Maximum = max_download; });
            //lb_all.Invoke((MethodInvoker)delegate { lb_all.Text = "/ " + dgv.Rows[countrows - 1].Cells[5].Value.ToString(); });
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = 0; });

            //textBox1.Invoke((MethodInvoker)delegate { textBox1.Clear(); });*/

            //SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            BWS.Clear();
            int len_bws = (genre.Length - 1) * sub_map.Count;
            count_run_bws = len_bws;
            for (int i = 0; i < len_bws; i++)
            {
                var b = (new BackgroundWorker());
                b.WorkerSupportsCancellation = true;
                BWS.Add(b);
            }

            int ib = 0;
            // //using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                /*connection.ConnectionString = @"Data Source = " + NameBD;
                connection.Open();*/
                foreach (var g in genre)
                {
                    if (g == "Итого:") continue;
                    /*textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка категории " + g + "\n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });*/
                    //if (bwDownloadRelease.CancellationPending) return;

                    foreach (var d in sub_map)
                    {
                        /**textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка подкатегории " + d.Value + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });*/
                        //if (bwDownloadRelease.CancellationPending) return;
                        try
                        {
                            BWS[ib].DoWork += delegate {
                                DownloadMp3(dgv, g, d.Key, textBox1);
                            };
                            BWS[ib].WorkerSupportsCancellation = true;
                            BWS[ib].RunWorkerAsync();

                            //DowloadMp3(connection, g, s, "new");
                        }
                        catch (Exception ex)
                        {
                            /*textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Ошибка" + ex.ToString() + "\n"); });
                            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });*/
                            Console.WriteLine(BWS[ib].ToString() + " Ошибка" + ex.ToString());
                        }
                        finally { ib++; }

                    }
                    //if (bwDownloadRelease.CancellationPending) return;
                    /*textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка подкатегории classix \n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });*/

                    /*try
                    {
                        //var bw = new BackgroundWorker();
                        BWS[ib].DoWork += delegate {
                            DownloadMp3(g, "back", "classix", true);
                        };
                        BWS[ib].RunWorkerAsync();
                        //DowloadMp3(connection, g, "back", "classix", true);
                    }
                    catch (Exception ex)
                    {
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Ошибка" + ex.ToString() + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                        Console.WriteLine(BWS[ib].ToString() + " Ошибка" + ex.ToString());
                    }
                    finally { ib++; }*/
                }
            }
            int count_live = BWS.Count(b => b.IsBusy);
            while (count_live > 0 && count_run_bws > 0 )
            {
                Thread.Sleep(1000);
                Console.WriteLine("Sleep, Count_live= " + count_live.ToString());
                count_live = BWS.Count(b => b.IsBusy);
                //textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загружено " + loaded_count.ToString() + "\n"); });
                //pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
                pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = (int)loaded_count; });
            }

            foreach (var b in BWS) { b.CancelAsync(); }
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загружено треков " + loaded_count.ToString() + "\n"); });
            lberror.Invoke((MethodInvoker)delegate { lberror.Text = String.Format("Ошибок {0} / {1}", errorlist.Count(), loaded_count) ; });
            
            foreach (var s in errorlist)
            {
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText(s + "\n"); });
            }

            //bDownload_rels.Invoke((MethodInvoker)delegate { bDownload_rels.Enabled = true; });
            string msg = isstop ? "Загрузка прервана" : "Все файлы загружены";
            MessageBox.Show(msg, "Загрузка окончена", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        //parcer python script
        public void run_cmd_python()
        {
            try
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = Properties.Settings.Default.Python_path;
                //start.FileName = "C:\\Python27\\python.exe";
                start.Arguments = Properties.Settings.Default.Chart_parser;
                /*if (!Properties.Settings.Default.CloasePythonAfterFinish)
                    start.Arguments += " qq";*/
                //start.Arguments = "Main.py";

                start.UseShellExecute = false;
                start.RedirectStandardOutput = true;
                start.CreateNoWindow = true;
                Console.WriteLine(start.FileName);

                using (process = Process.Start(start))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        Console.WriteLine(result);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка парсинга", ex.Message);
                Console.WriteLine(ex.ToString());
            }
            MessageBox.Show("Обновление Chart завершено!", "Обновление", MessageBoxButtons.OK, MessageBoxIcon.Information);

            Console.WriteLine("End python");
        }
    }
}
