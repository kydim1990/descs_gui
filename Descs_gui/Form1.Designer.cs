﻿namespace Descs_gui
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_def_index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bConnectDB = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.bCalcTracksForDownload = new System.Windows.Forms.Button();
            this.lSelectedDB = new System.Windows.Forms.Label();
            this.bSelectDB = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bDownload_rels = new System.Windows.Forms.Button();
            this.pDownload = new System.Windows.Forms.ProgressBar();
            this.bStartParsing = new System.Windows.Forms.Button();
            this.bw1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.путьКPythonexeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bSortDGV_default = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bSort_dvg = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.bShowParsingstate = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bw2 = new System.ComponentModel.BackgroundWorker();
            this.bwDownloadRelease = new System.ComponentModel.BackgroundWorker();
            this.bStopTask = new System.Windows.Forms.Button();
            this.bwFilter = new System.ComponentModel.BackgroundWorker();
            this.bwCalcNumberrelease = new System.ComponentModel.BackgroundWorker();
            this.tbErrorLog = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lberrors = new System.Windows.Forms.Label();
            this.lb_ready = new System.Windows.Forms.Label();
            this.lb_all = new System.Windows.Forms.Label();
            this.notiform = new System.Windows.Forms.NotifyIcon(this.components);
            this.Sync_timer = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.llastupdate = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pbFromList = new System.Windows.Forms.ProgressBar();
            this.lReadFromList = new System.Windows.Forms.Label();
            this.tbErrorFromList = new System.Windows.Forms.TextBox();
            this.lErrFromList = new System.Windows.Forms.Label();
            this.tbDownloadFromList = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bPathDownloadFromList = new System.Windows.Forms.Button();
            this.bDowloadListRelease = new System.Windows.Forms.Button();
            this.bCheckListRelease = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lIgnore = new System.Windows.Forms.Label();
            this.bIgnore = new System.Windows.Forms.Button();
            this.lfTagFiles = new System.Windows.Forms.Label();
            this.tbFilter_2 = new System.Windows.Forms.TextBox();
            this.textBoxAPI2 = new System.Windows.Forms.TextBox();
            this.progressBarApi2 = new System.Windows.Forms.ProgressBar();
            this.bStart2 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bTagFiles = new System.Windows.Forms.Button();
            this.lTagFiles = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lResult2 = new System.Windows.Forms.Label();
            this.bResult2 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lPredlisten = new System.Windows.Forms.Label();
            this.lfPredlisten = new System.Windows.Forms.Label();
            this.bPredlisten = new System.Windows.Forms.Button();
            this.tbFilter_1 = new System.Windows.Forms.TextBox();
            this.tabCharts = new System.Windows.Forms.TabPage();
            this.bt_Chart_stop = new System.Windows.Forms.Button();
            this.lbchart_error = new System.Windows.Forms.Label();
            this.pbChart = new System.Windows.Forms.ProgressBar();
            this.tb_chart_log = new System.Windows.Forms.TextBox();
            this.bt_Chart_dounload = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.Chart_comboBox = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btChart_refresh = new System.Windows.Forms.Button();
            this.Charts_bt_calc_download = new System.Windows.Forms.Button();
            this.Charts_dgv = new System.Windows.Forms.DataGridView();
            this.Charts_genre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Charts_sub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Charts_tracks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chart_no_tracks_out = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chart_no_releases_to_download = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chart_no_tracks_to_download = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chart_For_sort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bwDownlodFromFile = new System.ComponentModel.BackgroundWorker();
            this.bwStartAPI2 = new System.ComponentModel.BackgroundWorker();
            this.bw_newparsing = new System.ComponentModel.BackgroundWorker();
            this.bw_chart = new System.ComponentModel.BackgroundWorker();
            this.bt_ChartStartParser = new System.Windows.Forms.Button();
            this.lbChart_last_update = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bw_Chart_start_parser = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabCharts.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Charts_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Col_def_index});
            this.dgv.Location = new System.Drawing.Point(6, 19);
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.Size = new System.Drawing.Size(635, 390);
            this.dgv.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "genre";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "sub";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "no. releases out";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.Column3.Width = 130;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "no. tracks out";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "no. releases to download";
            this.Column5.Name = "Column5";
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "no. tracks to download";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Col_def_index
            // 
            this.Col_def_index.HeaderText = "For sort";
            this.Col_def_index.Name = "Col_def_index";
            this.Col_def_index.Visible = false;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(128, 27);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(140, 20);
            this.dtpFrom.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bConnectDB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtpTo);
            this.groupBox1.Controls.Add(this.dtpFrom);
            this.groupBox1.Location = new System.Drawing.Point(674, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 149);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // bConnectDB
            // 
            this.bConnectDB.Location = new System.Drawing.Point(98, 102);
            this.bConnectDB.Name = "bConnectDB";
            this.bConnectDB.Size = new System.Drawing.Size(131, 32);
            this.bConnectDB.TabIndex = 5;
            this.bConnectDB.Text = "Применить фильтр";
            this.bConnectDB.UseVisualStyleBackColor = true;
            this.bConnectDB.Click += new System.EventHandler(this.bConnectDB_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Период по:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Период с:";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(128, 67);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(140, 20);
            this.dtpTo.TabIndex = 1;
            // 
            // bCalcTracksForDownload
            // 
            this.bCalcTracksForDownload.Location = new System.Drawing.Point(98, 19);
            this.bCalcTracksForDownload.Name = "bCalcTracksForDownload";
            this.bCalcTracksForDownload.Size = new System.Drawing.Size(149, 38);
            this.bCalcTracksForDownload.TabIndex = 6;
            this.bCalcTracksForDownload.Text = "Посчитать количество треков для скачивания";
            this.bCalcTracksForDownload.UseVisualStyleBackColor = true;
            this.bCalcTracksForDownload.Click += new System.EventHandler(this.bCalcTracksForDownload_Click);
            // 
            // lSelectedDB
            // 
            this.lSelectedDB.AutoSize = true;
            this.lSelectedDB.Location = new System.Drawing.Point(46, 72);
            this.lSelectedDB.Name = "lSelectedDB";
            this.lSelectedDB.Size = new System.Drawing.Size(16, 13);
            this.lSelectedDB.TabIndex = 4;
            this.lSelectedDB.Text = "...";
            // 
            // bSelectDB
            // 
            this.bSelectDB.Location = new System.Drawing.Point(47, 19);
            this.bSelectDB.Name = "bSelectDB";
            this.bSelectDB.Size = new System.Drawing.Size(221, 42);
            this.bSelectDB.TabIndex = 3;
            this.bSelectDB.Text = "Выбрать БД";
            this.bSelectDB.UseVisualStyleBackColor = true;
            this.bSelectDB.Click += new System.EventHandler(this.bSelectDB_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bCalcTracksForDownload);
            this.groupBox2.Location = new System.Drawing.Point(674, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(286, 70);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Количество скачиваний";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bSelectDB);
            this.groupBox3.Controls.Add(this.lSelectedDB);
            this.groupBox3.Location = new System.Drawing.Point(674, 251);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(286, 95);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Управление БД";
            // 
            // bDownload_rels
            // 
            this.bDownload_rels.Location = new System.Drawing.Point(6, 422);
            this.bDownload_rels.Name = "bDownload_rels";
            this.bDownload_rels.Size = new System.Drawing.Size(161, 29);
            this.bDownload_rels.TabIndex = 9;
            this.bDownload_rels.Text = "Загрузить релизы";
            this.bDownload_rels.UseVisualStyleBackColor = true;
            this.bDownload_rels.Click += new System.EventHandler(this.bDownload_rels_Click);
            // 
            // pDownload
            // 
            this.pDownload.Location = new System.Drawing.Point(187, 427);
            this.pDownload.Name = "pDownload";
            this.pDownload.Size = new System.Drawing.Size(215, 23);
            this.pDownload.Step = 1;
            this.pDownload.TabIndex = 10;
            // 
            // bStartParsing
            // 
            this.bStartParsing.Location = new System.Drawing.Point(519, 422);
            this.bStartParsing.Name = "bStartParsing";
            this.bStartParsing.Size = new System.Drawing.Size(122, 34);
            this.bStartParsing.TabIndex = 11;
            this.bStartParsing.Text = "Начать парсинг";
            this.bStartParsing.UseVisualStyleBackColor = true;
            this.bStartParsing.Click += new System.EventHandler(this.bStartParsing_Click);
            // 
            // bw1
            // 
            this.bw1.WorkerSupportsCancellation = true;
            this.bw1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1013, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.путьКPythonexeToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // путьКPythonexeToolStripMenuItem
            // 
            this.путьКPythonexeToolStripMenuItem.Name = "путьКPythonexeToolStripMenuItem";
            this.путьКPythonexeToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.путьКPythonexeToolStripMenuItem.Text = "Настройки";
            this.путьКPythonexeToolStripMenuItem.Click += new System.EventHandler(this.путьКPythonexeToolStripMenuItem_Click);
            // 
            // bSortDGV_default
            // 
            this.bSortDGV_default.Location = new System.Drawing.Point(49, 60);
            this.bSortDGV_default.Name = "bSortDGV_default";
            this.bSortDGV_default.Size = new System.Drawing.Size(159, 24);
            this.bSortDGV_default.TabIndex = 13;
            this.bSortDGV_default.Text = "Сортировка по умолчанию";
            this.bSortDGV_default.UseVisualStyleBackColor = true;
            this.bSortDGV_default.Click += new System.EventHandler(this.bSortDGV_default_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bSort_dvg);
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.bSortDGV_default);
            this.groupBox4.Location = new System.Drawing.Point(674, 368);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(286, 90);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Сортировка";
            // 
            // bSort_dvg
            // 
            this.bSort_dvg.Location = new System.Drawing.Point(201, 20);
            this.bSort_dvg.Name = "bSort_dvg";
            this.bSort_dvg.Size = new System.Drawing.Size(85, 23);
            this.bSort_dvg.TabIndex = 15;
            this.bSort_dvg.Text = "Сортировать";
            this.bSort_dvg.UseVisualStyleBackColor = true;
            this.bSort_dvg.Click += new System.EventHandler(this.bSort_dvg_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "no. releases out",
            "no. tracks out",
            "no. releases to download",
            "no. tracks to download"});
            this.comboBox1.Location = new System.Drawing.Point(49, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(145, 21);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.Text = "no. releases out";
            // 
            // bShowParsingstate
            // 
            this.bShowParsingstate.Location = new System.Drawing.Point(898, 477);
            this.bShowParsingstate.Name = "bShowParsingstate";
            this.bShowParsingstate.Size = new System.Drawing.Size(154, 23);
            this.bShowParsingstate.TabIndex = 15;
            this.bShowParsingstate.Text = "Показать состояние парсинга";
            this.bShowParsingstate.UseVisualStyleBackColor = true;
            this.bShowParsingstate.Visible = false;
            this.bShowParsingstate.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(7, 469);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(634, 234);
            this.textBox1.TabIndex = 16;
            // 
            // bw2
            // 
            this.bw2.WorkerSupportsCancellation = true;
            this.bw2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            // 
            // bwDownloadRelease
            // 
            this.bwDownloadRelease.WorkerSupportsCancellation = true;
            this.bwDownloadRelease.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BwDownloadRelease_DoWork);
            // 
            // bStopTask
            // 
            this.bStopTask.Location = new System.Drawing.Point(683, 477);
            this.bStopTask.Name = "bStopTask";
            this.bStopTask.Size = new System.Drawing.Size(180, 23);
            this.bStopTask.TabIndex = 17;
            this.bStopTask.Text = "СТОП";
            this.bStopTask.UseVisualStyleBackColor = true;
            this.bStopTask.Click += new System.EventHandler(this.bStopTask_Click);
            // 
            // bwFilter
            // 
            this.bwFilter.WorkerSupportsCancellation = true;
            this.bwFilter.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwFilter_DoWork);
            // 
            // tbErrorLog
            // 
            this.tbErrorLog.Location = new System.Drawing.Point(659, 536);
            this.tbErrorLog.Multiline = true;
            this.tbErrorLog.Name = "tbErrorLog";
            this.tbErrorLog.ReadOnly = true;
            this.tbErrorLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbErrorLog.Size = new System.Drawing.Size(262, 167);
            this.tbErrorLog.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(659, 517);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Список ошибок";
            // 
            // lberrors
            // 
            this.lberrors.AutoSize = true;
            this.lberrors.Location = new System.Drawing.Point(773, 517);
            this.lberrors.Name = "lberrors";
            this.lberrors.Size = new System.Drawing.Size(13, 13);
            this.lberrors.TabIndex = 20;
            this.lberrors.Text = "0";
            // 
            // lb_ready
            // 
            this.lb_ready.AutoSize = true;
            this.lb_ready.Location = new System.Drawing.Point(422, 434);
            this.lb_ready.Name = "lb_ready";
            this.lb_ready.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lb_ready.Size = new System.Drawing.Size(37, 13);
            this.lb_ready.TabIndex = 21;
            this.lb_ready.Text = "00000";
            // 
            // lb_all
            // 
            this.lb_all.AutoSize = true;
            this.lb_all.Location = new System.Drawing.Point(457, 433);
            this.lb_all.Name = "lb_all";
            this.lb_all.Size = new System.Drawing.Size(12, 13);
            this.lb_all.TabIndex = 22;
            this.lb_all.Text = "/";
            // 
            // notiform
            // 
            this.notiform.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notiform.BalloonTipText = "Decks was hidden";
            this.notiform.BalloonTipTitle = "Desc";
            this.notiform.Icon = ((System.Drawing.Icon)(resources.GetObject("notiform.Icon")));
            this.notiform.Text = "Desc";
            this.notiform.Visible = true;
            this.notiform.DoubleClick += new System.EventHandler(this.notiform_DoubleClick);
            // 
            // Sync_timer
            // 
            this.Sync_timer.Enabled = true;
            this.Sync_timer.Interval = 60000;
            this.Sync_timer.Tick += new System.EventHandler(this.sync_timer_tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 720);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Последнее обновление было:";
            // 
            // llastupdate
            // 
            this.llastupdate.AutoSize = true;
            this.llastupdate.Location = new System.Drawing.Point(164, 720);
            this.llastupdate.Name = "llastupdate";
            this.llastupdate.Size = new System.Drawing.Size(59, 13);
            this.llastupdate.TabIndex = 24;
            this.llastupdate.Text = "last update";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabCharts);
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1013, 772);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgv);
            this.tabPage1.Controls.Add(this.llastupdate);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.lb_all);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.lb_ready);
            this.tabPage1.Controls.Add(this.bDownload_rels);
            this.tabPage1.Controls.Add(this.lberrors);
            this.tabPage1.Controls.Add(this.pDownload);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.bStartParsing);
            this.tabPage1.Controls.Add(this.tbErrorLog);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.bStopTask);
            this.tabPage1.Controls.Add(this.bShowParsingstate);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1005, 746);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Полный парсинг";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pbFromList);
            this.tabPage2.Controls.Add(this.lReadFromList);
            this.tabPage2.Controls.Add(this.tbErrorFromList);
            this.tabPage2.Controls.Add(this.lErrFromList);
            this.tabPage2.Controls.Add(this.tbDownloadFromList);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.bPathDownloadFromList);
            this.tabPage2.Controls.Add(this.bDowloadListRelease);
            this.tabPage2.Controls.Add(this.bCheckListRelease);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1005, 746);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Парсинг из списка";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pbFromList
            // 
            this.pbFromList.Location = new System.Drawing.Point(511, 96);
            this.pbFromList.Name = "pbFromList";
            this.pbFromList.Size = new System.Drawing.Size(431, 23);
            this.pbFromList.TabIndex = 9;
            this.pbFromList.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // lReadFromList
            // 
            this.lReadFromList.AutoSize = true;
            this.lReadFromList.Location = new System.Drawing.Point(508, 63);
            this.lReadFromList.Name = "lReadFromList";
            this.lReadFromList.Size = new System.Drawing.Size(140, 13);
            this.lReadFromList.TabIndex = 8;
            this.lReadFromList.Text = "Загружено релизов 0 из 0";
            // 
            // tbErrorFromList
            // 
            this.tbErrorFromList.Location = new System.Drawing.Point(511, 215);
            this.tbErrorFromList.Multiline = true;
            this.tbErrorFromList.Name = "tbErrorFromList";
            this.tbErrorFromList.ReadOnly = true;
            this.tbErrorFromList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbErrorFromList.Size = new System.Drawing.Size(431, 316);
            this.tbErrorFromList.TabIndex = 7;
            // 
            // lErrFromList
            // 
            this.lErrFromList.AutoSize = true;
            this.lErrFromList.Location = new System.Drawing.Point(508, 181);
            this.lErrFromList.Name = "lErrFromList";
            this.lErrFromList.Size = new System.Drawing.Size(97, 13);
            this.lErrFromList.TabIndex = 6;
            this.lErrFromList.Text = "Список ошибок: 0";
            // 
            // tbDownloadFromList
            // 
            this.tbDownloadFromList.Location = new System.Drawing.Point(30, 215);
            this.tbDownloadFromList.Multiline = true;
            this.tbDownloadFromList.Name = "tbDownloadFromList";
            this.tbDownloadFromList.ReadOnly = true;
            this.tbDownloadFromList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDownloadFromList.Size = new System.Drawing.Size(437, 316);
            this.tbDownloadFromList.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(270, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Выбереите папку";
            // 
            // bPathDownloadFromList
            // 
            this.bPathDownloadFromList.Location = new System.Drawing.Point(30, 25);
            this.bPathDownloadFromList.Name = "bPathDownloadFromList";
            this.bPathDownloadFromList.Size = new System.Drawing.Size(210, 35);
            this.bPathDownloadFromList.TabIndex = 3;
            this.bPathDownloadFromList.Text = "Выбрать папку для сохранения релизов";
            this.bPathDownloadFromList.UseVisualStyleBackColor = true;
            this.bPathDownloadFromList.Click += new System.EventHandler(this.bPathDownloadFromList_Click);
            // 
            // bDowloadListRelease
            // 
            this.bDowloadListRelease.Location = new System.Drawing.Point(30, 144);
            this.bDowloadListRelease.Name = "bDowloadListRelease";
            this.bDowloadListRelease.Size = new System.Drawing.Size(210, 39);
            this.bDowloadListRelease.TabIndex = 2;
            this.bDowloadListRelease.Text = "Загрузить релизы";
            this.bDowloadListRelease.UseVisualStyleBackColor = true;
            this.bDowloadListRelease.Click += new System.EventHandler(this.bDowloadListRelease_Click);
            // 
            // bCheckListRelease
            // 
            this.bCheckListRelease.Location = new System.Drawing.Point(30, 83);
            this.bCheckListRelease.Name = "bCheckListRelease";
            this.bCheckListRelease.Size = new System.Drawing.Size(210, 39);
            this.bCheckListRelease.TabIndex = 1;
            this.bCheckListRelease.Text = "Выбрать файл со списком релизов";
            this.bCheckListRelease.UseVisualStyleBackColor = true;
            this.bCheckListRelease.Click += new System.EventHandler(this.bCheckListRelease_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(270, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Выберите файл";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.lfTagFiles);
            this.tabPage3.Controls.Add(this.tbFilter_2);
            this.tabPage3.Controls.Add(this.textBoxAPI2);
            this.tabPage3.Controls.Add(this.progressBarApi2);
            this.tabPage3.Controls.Add(this.bStart2);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1005, 746);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Проставление тегов";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lIgnore);
            this.groupBox5.Controls.Add(this.bIgnore);
            this.groupBox5.Location = new System.Drawing.Point(16, 290);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(560, 76);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Где хранятся проигнорируемые файлы";
            // 
            // lIgnore
            // 
            this.lIgnore.AutoSize = true;
            this.lIgnore.Location = new System.Drawing.Point(143, 38);
            this.lIgnore.Name = "lIgnore";
            this.lIgnore.Size = new System.Drawing.Size(89, 13);
            this.lIgnore.TabIndex = 3;
            this.lIgnore.Text = "Выберите папку";
            // 
            // bIgnore
            // 
            this.bIgnore.Location = new System.Drawing.Point(18, 33);
            this.bIgnore.Name = "bIgnore";
            this.bIgnore.Size = new System.Drawing.Size(100, 23);
            this.bIgnore.TabIndex = 2;
            this.bIgnore.Text = "Выберите папку";
            this.bIgnore.UseVisualStyleBackColor = true;
            this.bIgnore.Click += new System.EventHandler(this.bIgnore_Click);
            // 
            // lfTagFiles
            // 
            this.lfTagFiles.AutoSize = true;
            this.lfTagFiles.Location = new System.Drawing.Point(596, 126);
            this.lfTagFiles.Name = "lfTagFiles";
            this.lfTagFiles.Size = new System.Drawing.Size(88, 13);
            this.lfTagFiles.TabIndex = 14;
            this.lfTagFiles.Text = "Фильтр файлов";
            // 
            // tbFilter_2
            // 
            this.tbFilter_2.Location = new System.Drawing.Point(596, 163);
            this.tbFilter_2.Name = "tbFilter_2";
            this.tbFilter_2.Size = new System.Drawing.Size(109, 20);
            this.tbFilter_2.TabIndex = 13;
            this.tbFilter_2.Text = "*.m4a";
            // 
            // textBoxAPI2
            // 
            this.textBoxAPI2.Location = new System.Drawing.Point(722, 3);
            this.textBoxAPI2.Multiline = true;
            this.textBoxAPI2.Name = "textBoxAPI2";
            this.textBoxAPI2.ReadOnly = true;
            this.textBoxAPI2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAPI2.Size = new System.Drawing.Size(275, 363);
            this.textBoxAPI2.TabIndex = 12;
            // 
            // progressBarApi2
            // 
            this.progressBarApi2.Location = new System.Drawing.Point(21, 417);
            this.progressBarApi2.Name = "progressBarApi2";
            this.progressBarApi2.Size = new System.Drawing.Size(560, 23);
            this.progressBarApi2.Step = 1;
            this.progressBarApi2.TabIndex = 11;
            // 
            // bStart2
            // 
            this.bStart2.Location = new System.Drawing.Point(162, 377);
            this.bStart2.Name = "bStart2";
            this.bStart2.Size = new System.Drawing.Size(283, 34);
            this.bStart2.TabIndex = 10;
            this.bStart2.Text = "Start";
            this.bStart2.UseVisualStyleBackColor = true;
            this.bStart2.Click += new System.EventHandler(this.bStart2_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bTagFiles);
            this.groupBox6.Controls.Add(this.lTagFiles);
            this.groupBox6.Location = new System.Drawing.Point(16, 111);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(698, 78);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Папка с  файлами для переименования и проставления тегов";
            // 
            // bTagFiles
            // 
            this.bTagFiles.Location = new System.Drawing.Point(18, 33);
            this.bTagFiles.Name = "bTagFiles";
            this.bTagFiles.Size = new System.Drawing.Size(100, 23);
            this.bTagFiles.TabIndex = 1;
            this.bTagFiles.Text = "Выберите папку";
            this.bTagFiles.UseVisualStyleBackColor = true;
            this.bTagFiles.Click += new System.EventHandler(this.bTagFiles_Click);
            // 
            // lTagFiles
            // 
            this.lTagFiles.AutoSize = true;
            this.lTagFiles.Location = new System.Drawing.Point(143, 43);
            this.lTagFiles.Name = "lTagFiles";
            this.lTagFiles.Size = new System.Drawing.Size(89, 13);
            this.lTagFiles.TabIndex = 2;
            this.lTagFiles.Text = "Выберите папку";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lResult2);
            this.groupBox7.Controls.Add(this.bResult2);
            this.groupBox7.Location = new System.Drawing.Point(16, 205);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(560, 65);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Куда сохранять результат";
            // 
            // lResult2
            // 
            this.lResult2.AutoSize = true;
            this.lResult2.Location = new System.Drawing.Point(143, 38);
            this.lResult2.Name = "lResult2";
            this.lResult2.Size = new System.Drawing.Size(89, 13);
            this.lResult2.TabIndex = 2;
            this.lResult2.Text = "Выберите папку";
            // 
            // bResult2
            // 
            this.bResult2.Location = new System.Drawing.Point(18, 33);
            this.bResult2.Name = "bResult2";
            this.bResult2.Size = new System.Drawing.Size(100, 23);
            this.bResult2.TabIndex = 1;
            this.bResult2.Text = "Выберите папку";
            this.bResult2.UseVisualStyleBackColor = true;
            this.bResult2.Click += new System.EventHandler(this.bResult2_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lPredlisten);
            this.groupBox8.Controls.Add(this.lfPredlisten);
            this.groupBox8.Controls.Add(this.bPredlisten);
            this.groupBox8.Controls.Add(this.tbFilter_1);
            this.groupBox8.Location = new System.Drawing.Point(16, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(698, 78);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Исходник (предпрослушка)";
            // 
            // lPredlisten
            // 
            this.lPredlisten.AutoSize = true;
            this.lPredlisten.Location = new System.Drawing.Point(143, 43);
            this.lPredlisten.Name = "lPredlisten";
            this.lPredlisten.Size = new System.Drawing.Size(89, 13);
            this.lPredlisten.TabIndex = 2;
            this.lPredlisten.Text = "Выберите папку";
            // 
            // lfPredlisten
            // 
            this.lfPredlisten.AutoSize = true;
            this.lfPredlisten.Location = new System.Drawing.Point(583, 18);
            this.lfPredlisten.Name = "lfPredlisten";
            this.lfPredlisten.Size = new System.Drawing.Size(88, 13);
            this.lfPredlisten.TabIndex = 5;
            this.lfPredlisten.Text = "Фильтр файлов";
            // 
            // bPredlisten
            // 
            this.bPredlisten.Location = new System.Drawing.Point(18, 33);
            this.bPredlisten.Name = "bPredlisten";
            this.bPredlisten.Size = new System.Drawing.Size(100, 23);
            this.bPredlisten.TabIndex = 1;
            this.bPredlisten.Text = "Выберите папку";
            this.bPredlisten.UseVisualStyleBackColor = true;
            this.bPredlisten.Click += new System.EventHandler(this.bPredlisten_Click);
            // 
            // tbFilter_1
            // 
            this.tbFilter_1.Location = new System.Drawing.Point(583, 52);
            this.tbFilter_1.Name = "tbFilter_1";
            this.tbFilter_1.Size = new System.Drawing.Size(100, 20);
            this.tbFilter_1.TabIndex = 4;
            this.tbFilter_1.Text = "*.mp3";
            // 
            // tabCharts
            // 
            this.tabCharts.Controls.Add(this.lbChart_last_update);
            this.tabCharts.Controls.Add(this.label8);
            this.tabCharts.Controls.Add(this.bt_ChartStartParser);
            this.tabCharts.Controls.Add(this.bt_Chart_stop);
            this.tabCharts.Controls.Add(this.lbchart_error);
            this.tabCharts.Controls.Add(this.pbChart);
            this.tabCharts.Controls.Add(this.tb_chart_log);
            this.tabCharts.Controls.Add(this.bt_Chart_dounload);
            this.tabCharts.Controls.Add(this.groupBox10);
            this.tabCharts.Controls.Add(this.groupBox9);
            this.tabCharts.Controls.Add(this.Charts_dgv);
            this.tabCharts.Location = new System.Drawing.Point(4, 22);
            this.tabCharts.Name = "tabCharts";
            this.tabCharts.Size = new System.Drawing.Size(1005, 746);
            this.tabCharts.TabIndex = 3;
            this.tabCharts.Text = "Charts";
            this.tabCharts.UseVisualStyleBackColor = true;
            // 
            // bt_Chart_stop
            // 
            this.bt_Chart_stop.Location = new System.Drawing.Point(505, 334);
            this.bt_Chart_stop.Name = "bt_Chart_stop";
            this.bt_Chart_stop.Size = new System.Drawing.Size(115, 52);
            this.bt_Chart_stop.TabIndex = 23;
            this.bt_Chart_stop.Text = "Остановить загрузку релиза";
            this.bt_Chart_stop.UseVisualStyleBackColor = true;
            this.bt_Chart_stop.Click += new System.EventHandler(this.bt_Chart_stop_Click);
            // 
            // lbchart_error
            // 
            this.lbchart_error.AutoSize = true;
            this.lbchart_error.Location = new System.Drawing.Point(435, 357);
            this.lbchart_error.Name = "lbchart_error";
            this.lbchart_error.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbchart_error.Size = new System.Drawing.Size(37, 13);
            this.lbchart_error.TabIndex = 22;
            this.lbchart_error.Text = "00000";
            // 
            // pbChart
            // 
            this.pbChart.Location = new System.Drawing.Point(195, 350);
            this.pbChart.Name = "pbChart";
            this.pbChart.Size = new System.Drawing.Size(215, 23);
            this.pbChart.Step = 1;
            this.pbChart.TabIndex = 18;
            // 
            // tb_chart_log
            // 
            this.tb_chart_log.Location = new System.Drawing.Point(8, 392);
            this.tb_chart_log.Multiline = true;
            this.tb_chart_log.Name = "tb_chart_log";
            this.tb_chart_log.ReadOnly = true;
            this.tb_chart_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_chart_log.Size = new System.Drawing.Size(612, 234);
            this.tb_chart_log.TabIndex = 17;
            // 
            // bt_Chart_dounload
            // 
            this.bt_Chart_dounload.Location = new System.Drawing.Point(8, 344);
            this.bt_Chart_dounload.Name = "bt_Chart_dounload";
            this.bt_Chart_dounload.Size = new System.Drawing.Size(161, 29);
            this.bt_Chart_dounload.TabIndex = 16;
            this.bt_Chart_dounload.Text = "Загрузить релизы";
            this.bt_Chart_dounload.UseVisualStyleBackColor = true;
            this.bt_Chart_dounload.Click += new System.EventHandler(this.bt_Chart_dounload_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.button2);
            this.groupBox10.Controls.Add(this.Chart_comboBox);
            this.groupBox10.Controls.Add(this.button3);
            this.groupBox10.Location = new System.Drawing.Point(651, 146);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(286, 90);
            this.groupBox10.TabIndex = 15;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Сортировка";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Сортировать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Chart_comboBox
            // 
            this.Chart_comboBox.FormattingEnabled = true;
            this.Chart_comboBox.Items.AddRange(new object[] {
            "no. releases out",
            "no. tracks out",
            "no. releases to download",
            "no. tracks to download"});
            this.Chart_comboBox.Location = new System.Drawing.Point(49, 20);
            this.Chart_comboBox.Name = "Chart_comboBox";
            this.Chart_comboBox.Size = new System.Drawing.Size(145, 21);
            this.Chart_comboBox.TabIndex = 14;
            this.Chart_comboBox.Text = "no. releases out";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(49, 60);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(159, 24);
            this.button3.TabIndex = 13;
            this.button3.Text = "Сортировка по умолчанию";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btChart_refresh);
            this.groupBox9.Controls.Add(this.Charts_bt_calc_download);
            this.groupBox9.Location = new System.Drawing.Point(651, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(286, 126);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Количество скачиваний";
            // 
            // btChart_refresh
            // 
            this.btChart_refresh.Location = new System.Drawing.Point(27, 21);
            this.btChart_refresh.Name = "btChart_refresh";
            this.btChart_refresh.Size = new System.Drawing.Size(212, 32);
            this.btChart_refresh.TabIndex = 7;
            this.btChart_refresh.Text = "Обновить данные";
            this.btChart_refresh.UseVisualStyleBackColor = true;
            this.btChart_refresh.Click += new System.EventHandler(this.btChart_refresh_Click);
            // 
            // Charts_bt_calc_download
            // 
            this.Charts_bt_calc_download.Location = new System.Drawing.Point(27, 69);
            this.Charts_bt_calc_download.Name = "Charts_bt_calc_download";
            this.Charts_bt_calc_download.Size = new System.Drawing.Size(212, 38);
            this.Charts_bt_calc_download.TabIndex = 6;
            this.Charts_bt_calc_download.Text = "Посчитать количество треков для скачивания";
            this.Charts_bt_calc_download.UseVisualStyleBackColor = true;
            this.Charts_bt_calc_download.Click += new System.EventHandler(this.Charts_bt_calc_download_Click);
            // 
            // Charts_dgv
            // 
            this.Charts_dgv.AllowUserToAddRows = false;
            this.Charts_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Charts_dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Charts_genre,
            this.Charts_sub,
            this.Charts_tracks,
            this.Chart_no_tracks_out,
            this.Chart_no_releases_to_download,
            this.Chart_no_tracks_to_download,
            this.Chart_For_sort});
            this.Charts_dgv.Location = new System.Drawing.Point(3, 3);
            this.Charts_dgv.Name = "Charts_dgv";
            this.Charts_dgv.RowHeadersVisible = false;
            this.Charts_dgv.Size = new System.Drawing.Size(603, 325);
            this.Charts_dgv.TabIndex = 0;
            // 
            // Charts_genre
            // 
            this.Charts_genre.HeaderText = "genre";
            this.Charts_genre.Name = "Charts_genre";
            this.Charts_genre.ReadOnly = true;
            // 
            // Charts_sub
            // 
            this.Charts_sub.HeaderText = "sub";
            this.Charts_sub.Name = "Charts_sub";
            this.Charts_sub.ReadOnly = true;
            // 
            // Charts_tracks
            // 
            this.Charts_tracks.HeaderText = "no. releases out";
            this.Charts_tracks.Name = "Charts_tracks";
            this.Charts_tracks.ReadOnly = true;
            this.Charts_tracks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Chart_no_tracks_out
            // 
            this.Chart_no_tracks_out.HeaderText = "no. tracks out";
            this.Chart_no_tracks_out.Name = "Chart_no_tracks_out";
            this.Chart_no_tracks_out.ReadOnly = true;
            this.Chart_no_tracks_out.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Chart_no_releases_to_download
            // 
            this.Chart_no_releases_to_download.HeaderText = "no. releases to download";
            this.Chart_no_releases_to_download.Name = "Chart_no_releases_to_download";
            this.Chart_no_releases_to_download.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Chart_no_tracks_to_download
            // 
            this.Chart_no_tracks_to_download.HeaderText = "no. tracks to download";
            this.Chart_no_tracks_to_download.Name = "Chart_no_tracks_to_download";
            this.Chart_no_tracks_to_download.ReadOnly = true;
            this.Chart_no_tracks_to_download.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Chart_For_sort
            // 
            this.Chart_For_sort.HeaderText = "For sort";
            this.Chart_For_sort.Name = "Chart_For_sort";
            this.Chart_For_sort.ReadOnly = true;
            this.Chart_For_sort.Visible = false;
            // 
            // bw_newparsing
            // 
            this.bw_newparsing.WorkerSupportsCancellation = true;
            this.bw_newparsing.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_newparsing_DoWork);
            this.bw_newparsing.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_newparsing_RunWorkerCompleted);
            // 
            // bw_chart
            // 
            this.bw_chart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DownloadAll_chart);
            this.bw_chart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Cansel_Download_chart);
            // 
            // bt_ChartStartParser
            // 
            this.bt_ChartStartParser.Location = new System.Drawing.Point(651, 284);
            this.bt_ChartStartParser.Name = "bt_ChartStartParser";
            this.bt_ChartStartParser.Size = new System.Drawing.Size(286, 44);
            this.bt_ChartStartParser.TabIndex = 24;
            this.bt_ChartStartParser.Text = "Начать парсинг Chart";
            this.bt_ChartStartParser.UseVisualStyleBackColor = true;
            this.bt_ChartStartParser.Click += new System.EventHandler(this.bt_ChartStartParser_Click);
            // 
            // lbChart_last_update
            // 
            this.lbChart_last_update.AutoSize = true;
            this.lbChart_last_update.Location = new System.Drawing.Point(169, 720);
            this.lbChart_last_update.Name = "lbChart_last_update";
            this.lbChart_last_update.Size = new System.Drawing.Size(59, 13);
            this.lbChart_last_update.TabIndex = 26;
            this.lbChart_last_update.Text = "last update";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 720);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Последнее обновление было:";
            // 
            // bw_Chart_start_parser
            // 
            this.bw_Chart_start_parser.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Chart_start_parser);
            this.bw_Chart_start_parser.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Chart_parser_done);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 806);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Api Music";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.form_resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabCharts.ResumeLayout(false);
            this.tabCharts.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Charts_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void BackgroundWorker1_RunWorkerCompleted1(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bStartParsing.Enabled = true;
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.bStartParsing.Enabled = true;
        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Button bSelectDB;
        private System.Windows.Forms.Label lSelectedDB;
        private System.Windows.Forms.Button bConnectDB;
        private System.Windows.Forms.Button bCalcTracksForDownload;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bDownload_rels;
        private System.Windows.Forms.ProgressBar pDownload;
        private System.Windows.Forms.Button bStartParsing;
        private System.ComponentModel.BackgroundWorker bw1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem путьКPythonexeToolStripMenuItem;
        private System.Windows.Forms.Button bSortDGV_default;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button bSort_dvg;
        private System.Windows.Forms.Button bShowParsingstate;
        private System.Windows.Forms.TextBox textBox1;
        private System.ComponentModel.BackgroundWorker bw2;
        private System.ComponentModel.BackgroundWorker bwDownloadRelease;
        private System.Windows.Forms.Button bStopTask;
        private System.ComponentModel.BackgroundWorker bwFilter;
        private System.ComponentModel.BackgroundWorker bwCalcNumberrelease;
        private System.Windows.Forms.TextBox tbErrorLog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lberrors;
        private System.Windows.Forms.Label lb_ready;
        private System.Windows.Forms.Label lb_all;
        private System.Windows.Forms.NotifyIcon notiform;
        private System.Windows.Forms.Timer Sync_timer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label llastupdate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button bCheckListRelease;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bDowloadListRelease;
        private System.ComponentModel.BackgroundWorker bwDownlodFromFile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bPathDownloadFromList;
        private System.Windows.Forms.TextBox tbDownloadFromList;
        private System.Windows.Forms.Label lErrFromList;
        private System.Windows.Forms.TextBox tbErrorFromList;
        private System.Windows.Forms.Label lReadFromList;
        private System.Windows.Forms.ProgressBar pbFromList;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lIgnore;
        private System.Windows.Forms.Button bIgnore;
        private System.Windows.Forms.Label lfTagFiles;
        private System.Windows.Forms.TextBox tbFilter_2;
        private System.Windows.Forms.TextBox textBoxAPI2;
        private System.Windows.Forms.ProgressBar progressBarApi2;
        private System.Windows.Forms.Button bStart2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button bTagFiles;
        private System.Windows.Forms.Label lTagFiles;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lResult2;
        private System.Windows.Forms.Button bResult2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lPredlisten;
        private System.Windows.Forms.Label lfPredlisten;
        private System.Windows.Forms.Button bPredlisten;
        private System.Windows.Forms.TextBox tbFilter_1;
        private System.ComponentModel.BackgroundWorker bwStartAPI2;
        private System.ComponentModel.BackgroundWorker bw_newparsing;
        private System.Windows.Forms.TabPage tabCharts;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox Chart_comboBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button Charts_bt_calc_download;
        private System.Windows.Forms.DataGridView Charts_dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Charts_genre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Charts_sub;
        private System.Windows.Forms.DataGridViewTextBoxColumn Charts_tracks;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chart_no_tracks_out;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chart_no_releases_to_download;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chart_no_tracks_to_download;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chart_For_sort;
        private System.Windows.Forms.Button btChart_refresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_def_index;
        private System.Windows.Forms.Button bt_Chart_dounload;
        private System.Windows.Forms.TextBox tb_chart_log;
        private System.ComponentModel.BackgroundWorker bw_chart;
        private System.Windows.Forms.ProgressBar pbChart;
        private System.Windows.Forms.Label lbchart_error;
        private System.Windows.Forms.Button bt_Chart_stop;
        private System.Windows.Forms.Button bt_ChartStartParser;
        private System.Windows.Forms.Label lbChart_last_update;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker bw_Chart_start_parser;
    }
}

