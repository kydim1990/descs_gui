﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data.Common;
using System.Diagnostics;
using System.Net;
using System.IO;
using TagLib;
using System.Threading;
using System.Management.Automation;

namespace Descs_gui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DefaultTable();
            chart.DefaultTable(Charts_dgv);
            NameBD = Properties.Settings.Default.DefaultDB;
            curent_no= Properties.Settings.Default.Current_track;
            pDownload.Minimum = 0;
            lSelectedDB.Text = NameBD;
            bw1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted2;
            lb_ready.Text = "";

            bwDownloadRelease.RunWorkerCompleted += BwDownloadRelease_RunWorkerCompleted;
            bwFilter.RunWorkerCompleted += BwFilter_RunWorkerCompleted;
            bwCalcNumberrelease.RunWorkerCompleted += BwCalcNumberrelease_RunWorkerCompleted;
            bwCalcNumberrelease.WorkerSupportsCancellation = true;
            bwCalcNumberrelease.DoWork += BwCalcNumberrelease_DoWork;

            llastupdate.Text = Properties.Settings.Default.LastUpdate.ToString();
            lbChart_last_update.Text = Properties.Settings.Default.ChartLastUpdate.ToString();

            //from list file
            label5.Text = Properties.Settings.Default.Path_from_list;
            bwDownlodFromFile.DoWork += BwDownlodFromFile_DoWork;
            bwDownlodFromFile.WorkerSupportsCancellation = true;
            bwDownlodFromFile.RunWorkerCompleted += BwDownlodFromFile_RunWorkerCompleted;

            pathDownloadFromList=Properties.Settings.Default.Path_from_list_dir;
            label6.Text = pathDownloadFromList;

            pbFromList.Maximum = 0;
            pbFromList.Step = 1;

            //------API2--------
            Paths = new string[4];
            Paths[0] = lPredlisten.Text = Properties.Settings.Default.Predlisten_default.ToString();
            Paths[1] = lTagFiles.Text = Properties.Settings.Default.Tags_defalt.ToString();
            Paths[2] = lResult2.Text = Properties.Settings.Default.Result_folder.ToString();
            Paths[3] = lIgnore.Text = Properties.Settings.Default.Ignore_folder.ToString();
            Labels.Add(lPredlisten);
            Labels.Add(lTagFiles);
            Labels.Add(lResult2);
            Labels.Add(lIgnore);

             api2= new API2(Paths);

            bwStartAPI2.RunWorkerCompleted += BwStart_RunWorkerCompleted;
            bwStartAPI2.DoWork += BwStart_DoWork;

            //---Charts--
            ClearTable(Charts_dgv);
            chart.SelectData(Charts_dgv);
            lbchart_error.Text = "";

        }

        DateTime d1;
        Charts chart = new Charts();

        public void setfolder(int t)
        {
            var dlg1 = new Ionic.Utils.FolderBrowserDialogEx();
            dlg1.Description = "Выберите папку";
            dlg1.ShowNewFolderButton = true;
            dlg1.ShowEditBox = true;

            //Default path
            switch (t)
            {
                case 0:
                    dlg1.SelectedPath = Properties.Settings.Default.Predlisten_default;
                    break;
                case 1:
                    dlg1.SelectedPath = Properties.Settings.Default.Tags_defalt;
                    break;
                case 2:
                    dlg1.SelectedPath = Properties.Settings.Default.Result_folder;
                    break;
                case 3:
                    dlg1.SelectedPath = Properties.Settings.Default.Ignore_folder;
                    break;
            }

    
            dlg1.ShowFullPathInEditBox = true;
            dlg1.RootFolder = System.Environment.SpecialFolder.MyComputer;

            if (dlg1.ShowDialog() == DialogResult.OK)
            {
                Paths[t] = Labels[t].Text = dlg1.SelectedPath;
                switch (t)
                {
                    case 0:
                        Properties.Settings.Default.Predlisten_default = dlg1.SelectedPath;
                        break;
                    case 1:
                        Properties.Settings.Default.Tags_defalt = dlg1.SelectedPath;
                        break;
                    case 2:
                        Properties.Settings.Default.Result_folder = dlg1.SelectedPath;
                        break;
                    case 3:
                        Properties.Settings.Default.Ignore_folder = dlg1.SelectedPath;
                        break;
                }
                Properties.Settings.Default.Save();
            }
            
        }

        private void BwStart_DoWork(object sender, DoWorkEventArgs e)
        {
            bStart2.Invoke((MethodInvoker)delegate { bStart2.Enabled = false; });
            //bStart.Enabled = false;
            api2.StartDo(textBoxAPI2, tbFilter_1, tbFilter_2, progressBarApi2);
        }

        private void BwStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bStart2.Invoke((MethodInvoker)delegate { bStart2.Enabled = true; });
        }

        private void BwDownlodFromFile_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bPathDownloadFromList.Invoke((MethodInvoker)delegate { bPathDownloadFromList.Enabled = true; });
            bCheckListRelease.Invoke((MethodInvoker)delegate { bCheckListRelease.Enabled = true; });
            bDowloadListRelease.Invoke((MethodInvoker)delegate { bDowloadListRelease.Enabled = true; });
            MessageBox.Show("Все файлы загружены", "Загрузка окончена", MessageBoxButtons.OK);

        }

        private void BwDownlodFromFile_DoWork(object sender, DoWorkEventArgs e)
        {
            Download_from_file();

        }

        private void BwCalcNumberrelease_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ;
        }

        private void BwFilter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bConnectDB.Enabled = true;
        }

        private void BwDownloadRelease_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Все файлы загружены", "Загрузка окончена", MessageBoxButtons.OK);
        }

        private void BackgroundWorker1_RunWorkerCompleted2(object sender, RunWorkerCompletedEventArgs e)
        {
            bStartParsing.Enabled = true;
            llastupdate.Text = DateTime.Now.ToString();
            lbChart_last_update.Text = DateTime.Now.ToString();
            Properties.Settings.Default.LastUpdate = DateTime.Now;
            Properties.Settings.Default.ChartLastUpdate = DateTime.Now;
            Properties.Settings.Default.Save();

            if ((DateTime.Now - d1).Minutes > 5)
                MessageBox.Show("База данных обновлена", "Обновление завершено", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        string NameBD, pathDownloadFromList;
        const int countrows = 16;
        string[] genre = { "house", "techno", "bass", "black", "minimal", "Итого:" }, sub = { "news", "back in stock", "classix" };
        string[] sub_cat = { "new", "back" }; //
        const string Path_mp3 = "MP3_Songs", Path_Artworks= "Artworks";
        int curent_no;
        Process process;
        Random rnd = new Random();
        uint count_errors = 0, loaded_count = 0, count_errors_from_list = 0;
        bool iscanselauto_refresh;
        string[] fromlistlines;
        bool isfromlist = false;

        List<LReleases> LRS = new List<LReleases>();

        //---------API2------
        API2 api2;
        string[] Paths;
        List<Label> Labels = new List<Label>();
        //-------------

        private void DefaultTable()
        {
            
            for (int i = 0; i < countrows; i++)  dgv.Rows.Add();
            
            int i_genre = -1, i_sub = 0;
            for (int i=0;i< countrows;i++)
            {
                //add genre
                if (i % 3 == 0)
                {
                    
                    i_genre++;
                }
                dgv.Rows[i].Cells[0].Value = genre[i_genre];
                //add sub
                if (i!= countrows-1) dgv.Rows[i].Cells[1].Value = sub[i_sub];
                if (i_sub > sub.Length - 2) i_sub = 0;
                else i_sub++;

                if (i>9) dgv.Rows[i].Cells[6].Value = "9"+i.ToString();
                else dgv.Rows[i].Cells[6].Value = i.ToString();
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bSelectDB_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                NameBD = openFileDialog1.FileName;
                lSelectedDB.Text = NameBD;
                Properties.Settings.Default.DefaultDB = NameBD;
                Properties.Settings.Default.Save();
                lSelectedDB.Text = Properties.Settings.Default.DefaultDB;
                //Console.WriteLine(openFileDialog1.FileName);
            }
        }

        private void InsertInDVG(string col,string sw, string sub, string val, bool isclassix=false)
        {
            int row_ins = 0;
            if (isclassix) row_ins += 2;
            else
            {
                switch (sub)
                {
                    case "news":
                        row_ins += 0;
                        break;
                    case "back":
                        row_ins += 1;
                        break;
                    default:
                        //Console.WriteLine("Default case");
                        break;
                }
            }
               
            switch (sw)
            {
                case "house":
                    row_ins += 0;
                    break;
                case "techno":
                    row_ins += 3;
                    break;
                case "bass":
                    row_ins += 6;
                    break;
                case "black":
                    row_ins += 9;
                    break;
                case "minimal":
                    row_ins += 12;
                    break;
                default:
                    //Console.WriteLine("Default case");
                    break;
            }
            int icol = 0;
            switch(col)
            {
                case "no_release":
                    icol = 2;
                    break;
                case "no_track":
                    icol = 3;
                    break;
                case "no_track_to_download":
                    icol = 5;
                    break;
                default:
                    //Console.WriteLine("Default case");
                    break;
            }

            dgv.Rows[row_ins].Cells[icol].Value = val.ToString();
        }

        private void CreateSelect(SQLiteConnection conn, string sub, string subcat = "new", bool isclassix = false)
        {
            string pardate = "substr("+sub+",7)||substr("+sub+",4,2)||substr("+sub+",1,2)";
            using (SQLiteCommand command = new SQLiteCommand(conn))
            {
                command.CommandText = @"select count(*) Count_rel, category From Mp3tag ";
                command.CommandText += "where subcategory='"+ subcat + "' and " + pardate;
                command.CommandText += " between '"+ dtpFrom.Value.ToString("yyyyMMdd")+ "' and '"+ dtpTo.Value.ToString("yyyyMMdd")+"'";
                if (sub == "back" && subcat == "new")
                {
                    pardate = "substr(" + subcat + ",7)||substr(" + subcat + ",4,2)||substr(" + subcat + ",1,2)";
                    command.CommandText += "and not (" + pardate + " between '" + dtpFrom.Value.ToString("yyyyMMdd") +
                        "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "')";
                }
                command.CommandText += " group by category";
                
                command.CommandType = CommandType.Text;
                SQLiteDataReader r = command.ExecuteReader();
                if (subcat == "classix") sub = "back";
                while (r.Read())
                {
                    var category = r["category"].ToString();
                    InsertInDVG("no_release", category, sub, Convert.ToString(r["Count_rel"]), isclassix);
                }
                    
                Console.WriteLine(command.CommandText);
                Console.WriteLine(bwFilter.CancellationPending);
            }
        }

        private int Getrowlimit(string category, string sub, bool isclassix = false)
        {
            int row_ins = 0;
            string sub_and_categ = category + "_" + sub;
            switch (sub_and_categ)
            {
                case "house_new":
                    row_ins = 0;
                    break;
                case "techno_new":
                    row_ins = 3;
                    break;
                case "bass_new":
                    row_ins = 6;
                    break;
                case "black_new":
                    row_ins = 9;
                    break;
                case "house_back":
                    row_ins = 1;
                    break;
                case "techno_back":
                    row_ins = 4;
                    break;
                case "bass_back":
                    row_ins = 7;
                    break;
                case "black_back":
                    row_ins = 10;
                    break;
                case "minimal_new":
                    row_ins = 12;
                    break;
                case "minimal_back":
                    row_ins = 13;
                    break;
                default:
                    //Console.WriteLine("Default case");
                    break;
            }
            if (isclassix) row_ins++;
            /*string res=dgv.Rows[row_ins].Cells[4].Value==null ? null : dgv.Rows[row_ins].Cells[4].Value.ToString();
            if (res == null) return "0";*/
            return row_ins;
        }

        private void CreateSelectTracks(SQLiteConnection conn, string sub, string subcat = "new", bool isclassix = false, bool islimit=false)
        {
            string pardate = "substr(" + sub + ",7)||substr(" + sub + ",4,2)||substr(" + sub + ",1,2)";
            string col = "no_track";
            using (SQLiteCommand command = new SQLiteCommand(conn))
            {
                command.CommandText = @"select count(*) Count_rel, category From Mp3tag mp,Tracks tr   ";
                command.CommandText += "where tr.url_release=mp.url and subcategory='" + subcat + "' and " + pardate;
                command.CommandText += " between '" + dtpFrom.Value.ToString("yyyyMMdd") + "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "'";
                if (sub == "back" && subcat == "new")
                {
                    pardate = "substr(" + subcat + ",7)||substr(" + subcat + ",4,2)||substr(" + subcat + ",1,2)";
                    command.CommandText += "and not (" + pardate + " between '" + dtpFrom.Value.ToString("yyyyMMdd") +
                        "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "')";
                }
                command.CommandText += " group by category";
                /*if (islimit)
                {
                    string lim = Getrowlimit(sub, subcat, isclassix);
                    command.CommandText += " limit "+ lim;
                    col = "no_track_to_download";
                }*/

                command.CommandType = CommandType.Text;
                SQLiteDataReader r = command.ExecuteReader();
                if (subcat == "classix") sub = "back";
                while (r.Read())
                {
                    InsertInDVG(col, Convert.ToString(r["category"]), sub, Convert.ToString(r["Count_rel"]), isclassix);
                    pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
                    //pDownload.PerformStep();
                }
                Console.WriteLine(command.CommandText);
            }
        }

        //Users select
        private void CreateSelectTracks_Users(SQLiteConnection conn,string category, string sub, string subcat = "new", bool isclassix = false)
        {
            string pardate = "substr(" + sub + ",7)||substr(" + sub + ",4,2)||substr(" + sub + ",1,2)";
            string col = "no_track_to_download";
            int lim = Getrowlimit(category, sub, isclassix);
            if (subcat == "classix")
            {
                sub = "back";
            }
            string limstr = "";
            //-----------
            string res = dgv.Rows[lim].Cells[4].Value == null ? null : dgv.Rows[lim].Cells[4].Value.ToString();
            if (res == null) limstr = "0";
            else limstr = res;
            
            //-------- 
            using (SQLiteCommand command = new SQLiteCommand(conn))
            {
                command.CommandText = @"select count(*) count_track from Tracks where url_release in ( select  url   From Mp3tag mp ";
                command.CommandText += "where  category='" + category+"' ";
                command.CommandText += "and subcategory='" + subcat + "' and " + pardate;
                command.CommandText += " between '" + dtpFrom.Value.ToString("yyyyMMdd") + "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "'";
                if (sub == "back" && subcat == "new")
                {
                    string s = "new";
                    pardate = "substr(" + s + ",7)||substr(" + s + ",4,2)||substr(" + s + ",1,2)";
                    command.CommandText += "and not (" + pardate + " between '" + dtpFrom.Value.ToString("yyyyMMdd") +
                        "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "')";
                }
                command.CommandText += " order by "+sub+" desc limit "+ limstr + ")";

                command.CommandType = CommandType.Text;
                SQLiteDataReader r = command.ExecuteReader();

                while (r.Read())
                {
                    var count_track = r["count_track"].ToString();
                    Console.WriteLine(count_track);
                    dgv.Rows[lim].Cells[5].Value = count_track.ToString();
                    //InsertInDVG(col, Convert.ToString(r["category"]), sub, Convert.ToString(r["Count_rel"]), isclassix);
                    
                }

                Console.WriteLine(command.CommandText);
            }
        }

        private void SelectData(bool all=true)
        {
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + NameBD;
                    connection.Open();
                    List<string> Count_releases = new List<string>();
                     
                    if (all)
                    {
                        //count new
                        if (bwFilter.CancellationPending) return;
                        CreateSelect(connection, "new");
                        if (bwFilter.CancellationPending) return;
                        CreateSelectTracks(connection, "new");
                        //count back 
                        CreateSelect(connection, "back");
                        if (bwFilter.CancellationPending) return;
                        CreateSelectTracks(connection, "back");
                        if (bwFilter.CancellationPending) return;
                        //count classix 
                        CreateSelect(connection, "back", "classix", true);
                        if (bwFilter.CancellationPending) return;
                        CreateSelectTracks(connection, "back", "classix", true);
                    }
                    else
                    {
                        
                        foreach (var g in genre)
                        {
                            if (g == "Итого:") continue;
                            foreach (string s in sub_cat)
                            {
                                if (bwCalcNumberrelease.CancellationPending) return;
                                CreateSelectTracks_Users(connection, g, s, "new");
                                //pDownload.PerformStep();
                                pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
                            }
                            if (bwCalcNumberrelease.CancellationPending) return;
                            CreateSelectTracks_Users(connection,g, "back", "classix", true);
                            //pDownload.PerformStep();
                            pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
                        }
                        //CreateSelectTracks_Users(connection,"house", "back", "classix", true);
                        /*CreateSelectTracks_Users(connection, "techno", "new", "new");
                        CreateSelectTracks_Users(connection, "bass", "new", "new");
                        CreateSelectTracks_Users(connection, "black", "new", "new");*/
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            for (int i=2;i<6; i++)
            {
                int sum = 0;
                for (int j = 0; j < countrows - 1; j++)
                    sum += Convert.ToInt32(dgv.Rows[j].Cells[i].Value);
                dgv.Rows[countrows - 1].Cells[i].Value = sum.ToString();
            }
        }

        //get artists
        private string[] GetArrfromstr(string s)
        {
            string[] a = new string[] { s };
            return a;
        }

        private string GetAlbum(string s)
        {
            int start = s.IndexOf('-');
            int end = s.IndexOf('-', start + 1);
            Console.WriteLine(start.ToString() + " " + end.ToString());
            if (end < start) end = s.Length - 1;
            s = s.Substring(start, end - start).Replace("-", "").Trim();
            return s;
        }

        //--------------------------------
        public void BWSDowloads(int track, Delegate d)
        {
            // do something

           
        }
        //--------------------------------

        //download mp3s
        private void DowloadMp3(string category, string sub, string subcat = "new", bool isclassix = false)
        {
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection conn = (SQLiteConnection)factory.CreateConnection())
            {
                conn.ConnectionString = @"Data Source = " + NameBD;
                conn.Open();

                string pardate = "substr(" + sub + ",7)||substr(" + sub + ",4,2)||substr(" + sub + ",1,2)";
                string col = "no_track_to_download";
                int lim = Getrowlimit(category, sub, isclassix);
                if (subcat == "classix")
                {
                    sub = "back";
                }
                string limstr = "";
                //-----------
                string res = dgv.Rows[lim].Cells[4].Value == null ? null : dgv.Rows[lim].Cells[4].Value.ToString();
                if (res == null) limstr = "0";
                else limstr = res;

                string oldurl_release = null;
                string rndnew = rnd.Next(1, 100000000).ToString();

                //-------- 
                using (SQLiteCommand command = new SQLiteCommand(conn))
                {
                    command.CommandText = @"select Tracks.url url,Tracks.track_artist track_artist,Tracks.url_release url_release, Mp3tag.artwork artwork,  Mp3tag.title title ";
                    command.CommandText += ", Mp3tag.artist artist,Mp3tag.genre genres, Mp3tag.sub sub, Mp3tag.info info,Tracks.name  name, Tracks.no_track no_track, ";
                    command.CommandText += "Mp3tag.new new, Mp3tag.catno catno, Mp3tag.label label, Mp3tag.year year, Mp3tag.grouping grouping ";
                    command.CommandText += " from Tracks, Mp3tag  where Mp3tag.url=Tracks.url_release and  Tracks.url_release in ( select  url   From Mp3tag mp ";
                    command.CommandText += "where  category='" + category + "' ";
                    command.CommandText += "and subcategory='" + subcat + "' and " + pardate;
                    command.CommandText += " between '" + dtpFrom.Value.ToString("yyyyMMdd") + "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "'";
                    if (sub == "back" && subcat == "new")
                    {
                        string s = "new";
                        pardate = "substr(" + s + ",7)||substr(" + s + ",4,2)||substr(" + s + ",1,2)";
                        command.CommandText +="and not ("+ pardate + " between '" + dtpFrom.Value.ToString("yyyyMMdd") +
                            "' and '" + dtpTo.Value.ToString("yyyyMMdd") + "')";
                    }

                    command.CommandText += " order by " + sub + " desc limit " + limstr + ") order by Tracks.url_release, Mp3tag.title";

                    command.CommandType = CommandType.Text;

                    Console.WriteLine(command.CommandText);
                    SQLiteDataReader r = command.ExecuteReader();

                    //tbErrorLog.Text = count_tracks.ToString();

                    List<BackgroundWorker> BWS = new List<BackgroundWorker>();


                    while (r.Read())
                    {
                        if (bwDownloadRelease.CancellationPending) return;
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка " + Convert.ToString(r["url"] + "\n")); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                        pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
                        //sortcomposer
                        string sortcomposer = "";
                        try
                        {
                            sortcomposer = Convert.ToDateTime(r["new"]).ToString("yyyy.MM.dd");
                        }
                        catch {
                            tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНеверный sortcomposer " + Convert.ToString(r["url"])); });
                            tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n----------------------\n"); });
                            count_errors++;
                            lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });
                            continue;
                        }

                        /*string test = Convert.ToString(r["url_release"]);
                         //Convert.ToString(r["track_artist"]) == "" ? Convert.ToString(r["artist"]) : Convert.ToString(r["track_artist"]);

                        if (test != "http://www.decks.de/t/various_artists-unboxed_brain/c94-iw")
                        {
                            continue;
                        }*/
                      

                        //download mp3
                        string mp3_path = Download_one(Convert.ToString(r["url"]), sortcomposer);

                        if (mp3_path == "")
                            continue;

                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("mp3_path= " + mp3_path + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                        Console.WriteLine("Url " + Convert.ToString(r["url"]));
                        //
                        string url_release = Convert.ToString(r["url_release"]);
                        Console.WriteLine("Url_Release " + url_release);
                        Console.WriteLine("Artwork " + Convert.ToString(r["url_release"]));
                       
                        //download artwork
                        string artworks_path = Download_one(Convert.ToString(r["artwork"]), sortcomposer, true);
                        
                        Console.WriteLine(artworks_path);
                       
                        //Set mp3 tags
                        Console.WriteLine("filepath= " + mp3_path);
                        //album
                        string album_ = Convert.ToString(r["title"]);
 
                        if (album_.ToUpper().Trim() == "V/A" || album_.ToUpper().Trim() == "Various Artists".ToUpper())
                            album_ = "Various Artists";
                        else if (album_.Split('/').Length > 1)
                            album_ = album_.Split('/')[0];

                        Console.WriteLine("album= " + album_);

                        string artist = Convert.ToString(r["track_artist"]) == "" ? Convert.ToString(r["artist"]) : Convert.ToString(r["track_artist"]);

                        Console.WriteLine("artists= " + GetArrfromstr(artist)[0]);
                        Console.WriteLine("artwork= " + artworks_path);
                        //genre
                        string genre = Convert.ToString(r["genres"]);
                        string sub_genre = Convert.ToString(r["sub"]);
                        string res_genre = "";
                        if (genre.Trim(' ') == "")
                            res_genre = sub_genre;
                        else
                            if (genre.Length > 2)
                                res_genre = genre + ", " + sub_genre;
                            else
                                res_genre = sub_genre;
                        Console.WriteLine("genres= " + GetArrfromstr(res_genre)[0]);
                        Console.WriteLine("lyrics= " + Convert.ToString(r["info"]));
                        Console.WriteLine("name= " + Convert.ToString(r["name"]));

                        string albumsort = Convert.ToString(r["label"]);

                        Console.WriteLine("sort_composer= " + GetArrfromstr(sortcomposer)[0]);
                        //sort album artist
                        string sortalbumartist = Convert.ToString(r["catno"]).Trim();
                        Console.WriteLine("albumArtistsSort= " + GetArrfromstr(sortalbumartist)[0]);
                        Console.WriteLine("albumSort= " + albumsort);
                        Console.WriteLine("year= " + Convert.ToString(r["year"]));
                        Console.WriteLine("grouping= " + Convert.ToString(r["grouping"]));
                        //string[] Cur_track = GetArrfromstr(Properties.Settings.Default.Current_track.ToString());

                        if (oldurl_release != null && oldurl_release != url_release)
                            rndnew = rnd.Next(1, 100000000).ToString();

                        oldurl_release = url_release;

                        string[] Cur_track = GetArrfromstr(rndnew);

                        Console.WriteLine("sort_artist= " + Cur_track);
                        //track NO
                        /*if (prev_album != null && prev_album == album_) track_no_++;
                        else track_no_ = 1;
                        prev_album= album_;*/
                        uint track_no_ = 0;
                        try
                        {
                            track_no_ = Convert.ToUInt32(r["no_track"].ToString());
                        }
                        catch {
                            track_no_ = 0;
                        }
                        Console.WriteLine("track_no_= " + track_no_);

                        //--
                        SetMp3Tags(file: mp3_path, album: album_, artists: GetArrfromstr(artist),
                            artwork: artworks_path, genres: GetArrfromstr(res_genre),
                            lyrics: Convert.ToString(r["info"]), name: Convert.ToString(r["name"]),
                            sort_composer: GetArrfromstr(sortcomposer),
                            albumArtistsSort: GetArrfromstr(sortalbumartist), albumSort: albumsort,
                            year: Convert.ToUInt32(r["year"]), grouping: Convert.ToString(r["grouping"]), sort_artist: Cur_track,
                            track_no: track_no_, albumartist: GetArrfromstr(url_release)
                        );
                        //save curent NO
                        /*Properties.Settings.Default.Current_track++;
                        Properties.Settings.Default.Save();*/
                        
                        loaded_count++;
                        lb_ready.Invoke((MethodInvoker)delegate { lb_ready.Text = loaded_count.ToString(); });
                        //pDownload.PerformStep();

                    }

                    //Console.WriteLine(command.CommandText);
                }
            }
        }

        private void DowloadThread(string url, string url_rel, string new_, string artwork, string title, string artist,
            string genres,string sub,string info_, string name, string catno, string label, uint year, string grouping,
            string prev_album, uint track_no_)
        {
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка " + Convert.ToString(url + "\n")); });
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });

            Console.WriteLine("Url " + Convert.ToString(url));
            //
            string url_release = Convert.ToString(url_rel);
            Console.WriteLine("Url_Release " + url_release);
            Console.WriteLine("Artwork " + Convert.ToString(url_rel));
            //sortcomposer
            string sortcomposer = Convert.ToDateTime(new_).ToString("yyyy.MM.dd");
            //download artwork
            string artworks_path = Download_one(Convert.ToString(artwork), sortcomposer, true);
            Console.WriteLine(artworks_path);
            //download mp3
            string mp3_path = Download_one(Convert.ToString(url), sortcomposer);
            //Set mp3 tags
            Console.WriteLine("filepath= " + mp3_path);
            //album
            string album_ = Convert.ToString(title);

            Console.WriteLine("album= " + album_);
            Console.WriteLine("artists= " + GetArrfromstr(Convert.ToString(artist))[0]);
            Console.WriteLine("artwork= " + artworks_path);
            //genre
            string genre = Convert.ToString(genres);
            string sub_genre = Convert.ToString(sub);
            string res_genre = "";
            if (genre.Trim(' ') == "")
                res_genre = sub_genre;
            else
                if (genre.Length > 2)
                    res_genre = genre + ", " + sub_genre;
                else
                    res_genre = sub_genre;
            Console.WriteLine("genres= " + GetArrfromstr(res_genre)[0]);
            Console.WriteLine("lyrics= " + Convert.ToString(info_));
            Console.WriteLine("name= " + Convert.ToString(name));

            Console.WriteLine("sort_composer= " + GetArrfromstr(sortcomposer)[0]);
            //sort album artist
            string sortalbumartist = Convert.ToString(catno).Trim();
            Console.WriteLine("albumArtistsSort= " + GetArrfromstr(sortalbumartist)[0]);
            Console.WriteLine("albumSort= " + Convert.ToString(label));
            Console.WriteLine("year= " + Convert.ToString(year));
            Console.WriteLine("grouping= " + Convert.ToString(grouping));
            string[] Cur_tarack = GetArrfromstr(rnd.Next(1,100000000).ToString());

            Console.WriteLine("sort_artist= " + Cur_tarack);

            Console.WriteLine("track_no_= " + track_no_.ToString());

            //--
            SetMp3Tags(file: mp3_path, album: album_, artists: GetArrfromstr(Convert.ToString(artist)),
                artwork: artworks_path, genres: GetArrfromstr(res_genre),
                lyrics: Convert.ToString(info_), name: Convert.ToString(name),
                sort_composer: GetArrfromstr(sortcomposer),
                albumArtistsSort: GetArrfromstr(sortalbumartist), albumSort: Convert.ToString(label),
                year: Convert.ToUInt32(year), grouping: Convert.ToString(grouping), sort_artist: Cur_tarack,
                track_no: track_no_, albumartist: GetArrfromstr(url_release)
            );
            //save curent NO
            Properties.Settings.Default.Current_track++;
            Properties.Settings.Default.Save();
            pDownload.Invoke((MethodInvoker)delegate { pDownload.PerformStep(); });
        }

        private string Download_one(string url, string new_date, bool is_artwork=false, bool islist=false)
        {
            string newfilepath = "";
            try
            {
                   
                string dir_save = getDirToSave(new_date,is_artwork, islist);
                using (var client = new WebClient())
                {
                    string link = url;
                    string[] splitlink = link.Split('/');

                    newfilepath = Path.Combine(dir_save, splitlink[splitlink.Length - 1]);
                    //autoriation
                    client.DownloadFile(link, newfilepath);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Не удалось загрузить файл\n");

                if (isfromlist)
                {
                    tbErrorFromList.Invoke((MethodInvoker)delegate { tbErrorFromList.AppendText("\nНе удалось загрузить файл " + url+ "\n"); });
                    tbErrorFromList.Invoke((MethodInvoker)delegate { tbErrorFromList.AppendText("\n----------------------\n"); });

                    count_errors_from_list++;
                    lberrors.Invoke((MethodInvoker)delegate { lErrFromList.Text = "Список ошибок: " + count_errors_from_list.ToString(); });

                    lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });
                   
                }
                else
                {
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНе удалось загрузить файл " + url); });
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n----------------------\n"); });

                    lErrFromList.Text = "Список ошибок: ";

                    count_errors++;
                    lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });
                }
         
                
                //MessageBox.Show("Не найдена ссылка " + url, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Console.WriteLine(e.ToString());
                try
                {
                    using (StreamWriter sw = System.IO.File.AppendText("Download_error.txt"))
                    {
                        sw.WriteLine("\nНе удалось загрузить файл " + url);
                        sw.WriteLine("\n--------------------------");
                        sw.WriteLine(e.ToString());
                    }
                }
                catch { }
                
            }
            return newfilepath;
        }

        private void RunFilter()
        {
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Maximum = countrows - 1; });
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = 0; });
            SelectData();
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = pDownload.Maximum == pDownload.Value ? pDownload.Value : pDownload.Maximum; });
            //pDownload.Value = pDownload.Maximum == pDownload.Value ? pDownload.Value : pDownload.Maximum;
        }

        private void bwFilter_DoWork(object sender, DoWorkEventArgs e)
        {
            RunFilter();

        }

        
        private void BwCalcNumberrelease_DoWork(object sender, DoWorkEventArgs e)
        {
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Maximum = countrows - 1; });
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = 0; });
            SelectData(false);

        }

        private void ClearTable(DataGridView dgv)
        {
            for (int i = 0; i < dgv.RowCount; i++)
                for (int j = 2; j < dgv.ColumnCount; j++)
                    dgv.Rows[i].Cells[j].Value = 0;
        }
        
        private void bConnectDB_Click(object sender, EventArgs e)
        {
            ClearTable(dgv);
            //RunFilter();
            bConnectDB.Enabled = false; ;
            bwFilter.RunWorkerAsync();
            /*pDownload.Maximum = countrows-1;
            pDownload.Value = 0;
            SelectData();
            pDownload.Value = pDownload.Maximum == pDownload.Value ? pDownload.Value : pDownload.Maximum;*/
        }

        private string getDirToSave(string newdate,bool is_artworks=false, bool islist=false)
        {
            string subdir = "";
            if (islist)
                subdir = pathDownloadFromList;
            else
            {
                string myeardir = newdate.Substring(2, 2) + newdate.Substring(5, 2);
                //newdate.ToString("yyMM");
                if (is_artworks)
                {
                    subdir = Path.Combine(myeardir, Path_Artworks);
                }
                else
                {
                    subdir = myeardir;
                }
            }
           
                
            DirectoryInfo di = Directory.CreateDirectory(Path.Combine(Properties.Settings.Default.Path_out_MP3, subdir));
            return di.FullName;
        }

        private void bCalcTracksForDownload_Click(object sender, EventArgs e)
        {
            pDownload.Maximum = countrows - 1;
            pDownload.Value = 0;
            //SelectData(false);
            bwCalcNumberrelease.RunWorkerAsync();
        }

        private void StartParsing()
        {
            //MessageBox.Show("Ready");
            bStartParsing.Enabled = false;
            //
            bw1.RunWorkerAsync();
            bw2.RunWorkerAsync();
        }



        private void bStartParsing_Click(object sender, EventArgs e)
        {
            if (true || !Properties.Settings.Default.UseNewVersionParsing)
            {
                System.IO.File.WriteAllText(@"output.txt", string.Empty);
                bStartParsing.Enabled = false;
                //run_cmd_python();               
                killpython();

                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    if (i > 1)
                        for (int j = 0; j < dgv.Rows.Count; j++)
                        {
                            dgv.Rows[j].Cells[i].Value = "";
                        }
                }

                StartParsing();
            }
            else
            {
                textBox1.Clear();
                bw_newparsing.RunWorkerAsync();
            }


        }

        private void killpython()
        {
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                PowerShellInstance.AddScript("stop-process -name python -force");
                PowerShellInstance.Invoke();
            }
        }

        //run python script
        private void run_cmd_python()
        {
            try
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = Properties.Settings.Default.Python_path;
                //start.FileName = "C:\\Python27\\python.exe";
                start.Arguments = Properties.Settings.Default.Main_parser;
                /*if (!Properties.Settings.Default.CloasePythonAfterFinish)
                    start.Arguments += " qq";*/
                //start.Arguments = "Main.py";

                start.UseShellExecute = false;
                start.RedirectStandardOutput = true;
                start.CreateNoWindow = true;
                Console.WriteLine(start.FileName);

                using (process = Process.Start(start))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        Console.WriteLine(result);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка парсинга", ex.Message);
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("End python");
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            d1 = DateTime.Now;
            int i = 1;
            isStop = false;
            while ((DateTime.Now - d1).Minutes < 5 && !isStop)
            {
                //killpython();    
                textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Попытка " + i.ToString() + "\n"); });
                run_cmd_python();
                
                i++;
            }
            isStop = true;
        }

        private void BwDownloadRelease_DoWork(object sender, DoWorkEventArgs e)
        {
            SelectData(false);
            //Progress bar
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Maximum = Convert.ToInt32(dgv.Rows[countrows - 1].Cells[5].Value); });
            lb_all.Invoke((MethodInvoker)delegate { lb_all.Text = "/ "+ dgv.Rows[countrows - 1].Cells[5].Value.ToString(); });
            pDownload.Invoke((MethodInvoker)delegate { pDownload.Value = 0; });

            textBox1.Invoke((MethodInvoker)delegate { textBox1.Clear(); });

            //SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            List<BackgroundWorker> BWS = new List<BackgroundWorker>();
            for (int i = 0; i < (genre.Length-1)*sub_cat.Length + genre.Length-1; i++)
            {
                var b = (new BackgroundWorker());
                b.WorkerSupportsCancellation = true;
                BWS.Add(b);
            }
                
            int ib = 0;
           // //using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                /*connection.ConnectionString = @"Data Source = " + NameBD;
                connection.Open();*/
                foreach (var g in genre)
                {
                    if (g == "Итого:") continue;
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка категории " + g + "\n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                    if (bwDownloadRelease.CancellationPending) return;
                    
      
                    foreach (string s in sub_cat)
                    {
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка подкатегории " + s + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                        if (bwDownloadRelease.CancellationPending) return;
                        try
                        {
                            BWS[ib].DoWork += delegate {
                                DowloadMp3( g, s, "new");
                            };
                            BWS[ib].WorkerSupportsCancellation = true;
                            BWS[ib].RunWorkerAsync();

                            //DowloadMp3(connection, g, s, "new");
                        }
                         catch(Exception ex)
                        {
                            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Ошибка" + ex.ToString() + "\n"); });
                            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                            Console.WriteLine(BWS[ib].ToString() + " Ошибка" + ex.ToString());
                        }
                        finally { ib++; }
                       
                    }
                    if (bwDownloadRelease.CancellationPending) return;
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Загрузка подкатегории classix \n"); });
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                    try
                    {
                        //var bw = new BackgroundWorker();
                        BWS[ib].DoWork += delegate {
                            DowloadMp3(g, "back", "classix", true);
                        };
                        BWS[ib].RunWorkerAsync();
                        //DowloadMp3(connection, g, "back", "classix", true);
                    }
                    catch (Exception ex)
                    {
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Ошибка" + ex.ToString() + "\n"); });
                        textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("------------------------------\n"); });
                        Console.WriteLine(BWS[ib].ToString() + " Ошибка" + ex.ToString());
                    }
                    finally { ib++; }
                }
            }
            int count_live=BWS.Count(b => b.IsBusy);
            while (count_live > 0 && pDownload.Value < pDownload.Maximum-1)
            {
                Thread.Sleep(1000);
                Console.WriteLine("Sleep, Count_live= "+count_live.ToString());
                count_live = BWS.Count(b => b.IsBusy);
            }
            foreach (var b in BWS) { b.CancelAsync(); }
            bDownload_rels.Invoke((MethodInvoker)delegate { bDownload_rels.Enabled = true; });
            MessageBox.Show("Все файлы загружены", "Загрузка окончена", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

        }

        private void путьКPythonexeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2();
            frm.Show();
        }

        private void sortdgv(DataGridView dgv1, int countrows_, ComboBox cmb)
        {
            int i = 0;
            foreach (DataGridViewTextBoxColumn col in dgv1.Columns)
            {
                if (col.HeaderText == cmb.Text)
                {
                    i = col.Index;
                    break;
                }

            }

            for (int j = 0; j < countrows_ - 1; j++)
                dgv1.Rows[j].Cells[i].Value = Convert.ToInt32(dgv1.Rows[j].Cells[i].Value);

            var EndRow = dgv1.Rows[dgv1.RowCount - 1];
            dgv1.Rows.RemoveAt(countrows_ - 1);
            dgv1.Sort(dgv1.Columns[i], ListSortDirection.Ascending);
            dgv1.Rows.Add(EndRow);
        }

        private void bSort_dvg_Click(object sender, EventArgs e)
        {
            sortdgv(dgv, countrows, comboBox1);
        }

        private void bSortDGV_default_Click(object sender, EventArgs e)
        {
            var EndRow = dgv.Rows[dgv.RowCount - 1];
            dgv.Rows.RemoveAt(countrows - 1);
            dgv.Sort(dgv.Columns[6], ListSortDirection.Ascending);

            dgv.Rows.Add(EndRow);
        }

        //read file log
        private void ReadLogs()
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader("output.txt"))
                {
                    Thread.Sleep(500);
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    //textBox1.Text = line;
                    textBox1.Invoke((MethodInvoker)delegate { textBox1.Text = line; });
                    Console.WriteLine(line);
                }
                
                textBox1.Invoke((MethodInvoker)delegate { textBox1.SelectionStart = textBox1.Text.Length; });
                textBox1.Invoke((MethodInvoker)delegate { textBox1.ScrollToCaret(); });
            }
            catch (Exception ex)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var d1 = DateTime.Now;
            Parser parser = new Parser(textBox1);
            string testurl = "http://www.decks.de/t/samuel_rohrer-range_of_regularity_remixes/c9u-e0";
            //var res = parser.Getinforelease(testurl);
            //parser.Getalllinks();
            //parser.Getinforelease(testurl);
            bStartParsing.Invoke((MethodInvoker)delegate { textBox1.AppendText("testurl=" + testurl + "\n"); });
            var d2 = DateTime.Now;
            var diff = (d2 - d1).TotalMinutes;
            textBox1.AppendText("Total minutes= " + diff.ToString());
            /*foreach(var k in res.Keys)
            {
                bStartParsing.Invoke((MethodInvoker)delegate { textBox1.AppendText("key=" + k +"\n"); });
            }*/
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(1000);
            while (!bStartParsing.Enabled)
            {
                ReadLogs();
                Thread.Sleep(1000);
            }
                

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //if (process != null) process.Kill();
                killpython();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите выйти?", "Выход",  MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
            
        }
    

        private void form_resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notiform.Visible = true;
                notiform.ShowBalloonTip(1);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notiform.Visible = false;
            }
        }

        private void notiform_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }


        private void sync_timer_tick(object sender, EventArgs e)
        {
            Console.WriteLine("sync_timer_tick time=" + (DateTime.Today + Properties.Settings.Default.SyncTime).ToString());
            Console.WriteLine("DateTime.Now.TimeOfDay= " + DateTime.Now.TimeOfDay.ToString());
           

            if (Properties.Settings.Default.Autorefresh_isshoemwessages && Properties.Settings.Default.isSync &&
                Math.Round(DateTime.Now.TimeOfDay.TotalMinutes) == Math.Round(Properties.Settings.Default.SyncTime.TotalMinutes) - 1)
            {
                iscanselauto_refresh = false;
                Console.WriteLine("Prev sync");
                DialogResult dialogResult = MessageBox.Show("Через 60 секунд начнется автоматическое обновление. Выполнить?",
                    DateTime.Now.TimeOfDay.ToString(@"hh\:mm") +" Автообновление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dialogResult == DialogResult.No)
                {
                    iscanselauto_refresh = true;
                    //MessageBox.Show("no");
                }
            }

            if (!iscanselauto_refresh && Properties.Settings.Default.isSync 
                && Math.Round(DateTime.Now.TimeOfDay.TotalMinutes) == Math.Round(Properties.Settings.Default.SyncTime.TotalMinutes) )
            {
                Console.WriteLine("Start sync");
                //MessageBox.Show("iscanselauto_refresh="+ iscanselauto_refresh.ToString());
                if (false && Properties.Settings.Default.UseNewVersionParsingAuto)
                {
                    StartParcingNew();
                }
                else
                    StartParsing();
            }
            //StartParsing();
            //MessageBox.Show("timer");
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Download_from_file()
        {
            string curdate =  String.Format("!{0}", DateTime.Now.ToString("yyyy.MM.dd"));
            count_errors_from_list = 0;
            isfromlist = true;
            fromlistlines = System.IO.File.ReadAllLines(label5.Text);

            fromlistlines = fromlistlines.Where(val => val.Trim() != "").ToArray();

            pbFromList.Invoke((MethodInvoker)delegate { pbFromList.Maximum = fromlistlines.Length; });
            pbFromList.Invoke((MethodInvoker)delegate { pbFromList.Value = 0; });

            tbDownloadFromList.Invoke((MethodInvoker)delegate { tbDownloadFromList.Clear(); });

            lReadFromList.Invoke((MethodInvoker)delegate { lReadFromList.Text= "Загружено релизов  0 из " + fromlistlines.Length.ToString(); });

            for (int i=0; i< fromlistlines.Length; i++)
            {

                //string urlrel = "http://www.decks.de/t/100hz-saverio_celestri-timelink/c8h-sa"; // fromlistlines[i];
                string urlrel = fromlistlines[i];

                tbDownloadFromList.Invoke((MethodInvoker)delegate { tbDownloadFromList.AppendText("------------------------------\n"); });
                tbDownloadFromList.Invoke((MethodInvoker)delegate { tbDownloadFromList.AppendText("Загрузка релиза " + urlrel + "\n"); });
   

                LReleases lrs = new LReleases(urlrel);
                string rndnew = rnd.Next(1, 100000000).ToString();
                foreach (var tr in lrs.tracks)
                {
                    Console.WriteLine("tr.Name= " + tr.Name);
                    Console.WriteLine("tr.Url= " + tr.Url);
                    Console.WriteLine("tr.Artist= " + tr.Artist);

                    tbDownloadFromList.Invoke((MethodInvoker)delegate { tbDownloadFromList.AppendText("\tЗагрузка трека " + tr.Name + "\n"); });

                    string sortcomposer = "";

                    try
                    {
                        if (lrs._new == null)
                            sortcomposer = curdate;
                        else
                            sortcomposer = Convert.ToDateTime(lrs._new).ToString("yyyy.MM.dd");
                    }
                    catch
                    {
                        /*tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНеверный sortcomposer " + Convert.ToString(r["url"])); });
                        tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n----------------------\n"); });
                        count_errors++;
                        lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });*/
                        Console.WriteLine("Error sortcomposer");
                        continue;
                    }

                    string mp3_path = Download_one(tr.Url, sortcomposer, false, true);
                    Console.WriteLine("mp3_path= "+ mp3_path);
                    if (mp3_path == "") continue;

                    string url_release = urlrel;
                    Console.WriteLine("Url_Release " + url_release);
                    Console.WriteLine("Artwork " + lrs._artworkurl);

                    //download artwork
                    string artworks_path = Download_one(lrs._artworkurl, sortcomposer, true, true);

                    Console.WriteLine(artworks_path);

                    //Set mp3 tags
                    Console.WriteLine("filepath= " + mp3_path);
                    //album
                    string album_ = lrs._title;
                    if (album_.ToUpper().Trim() == "V/A" || album_.ToUpper().Trim() == "Various Artists".ToUpper())
                        album_ = "Various Artists";
                    else if (album_.Split('/').Length > 1)
                        album_ = album_.Split('/')[0];

                    Console.WriteLine("album= " + album_);

                    string artist = tr.Artist == "" ? lrs._artist : tr.Artist;

                    Console.WriteLine("artists= " + GetArrfromstr(artist)[0]);
                    Console.WriteLine("artwork= " + artworks_path);
                    //genre
                    string genre = lrs._genre;
                    string sub_genre = lrs._sub;
                    string res_genre = "";
                    if (genre.Trim(' ') == "")
                        res_genre = sub_genre;
                    else
                        if (genre.Length > 2)
                            res_genre = genre + ", " + sub_genre;
                        else
                            res_genre = sub_genre;
                    //res_genre = sub_genre.Length > 0 ? genre + ", " + sub_genre : genre;
                    /*if (res_genre.Length > 2)
                        res_genre = genre.Substring(0, res_genre.Length - 2);*/
                    Console.WriteLine("genres= " + GetArrfromstr(res_genre)[0]);
                    //Console.WriteLine("lyrics= " + Convert.ToString(r["info"]));
                    //Console.WriteLine("name= " + Convert.ToString(r["name"]));

                    string albumsort = lrs._label;

                    Console.WriteLine("sort_composer= " + GetArrfromstr(sortcomposer)[0]);
                    //sort album artist
                    string sortalbumartist = lrs._catno.Trim();
                    Console.WriteLine("albumArtistsSort= " + GetArrfromstr(sortalbumartist)[0]);
                    Console.WriteLine("albumSort= " + albumsort);
                    Console.WriteLine("year= " + Convert.ToString(lrs._year));
                    Console.WriteLine("grouping= " + Convert.ToString(lrs._grouping));
                    //string[] Cur_track = GetArrfromstr(Properties.Settings.Default.Current_track.ToString());


                    string[] Cur_track = GetArrfromstr(rndnew);

                    Console.WriteLine("sort_artist= " + Cur_track);
                    //track NO
                    /*if (prev_album != null && prev_album == album_) track_no_++;
                    else track_no_ = 1;
                    prev_album= album_;*/
                    uint track_no_ = 0;
                    try
                    {
                        track_no_ = Convert.ToUInt32((tr.No_track+1).ToString());
                    }
                    catch
                    {
                        track_no_ = 0;
                    }
                    Console.WriteLine("track_no_= " + track_no_);

                    SetMp3Tags(file: mp3_path, album: album_, artists: GetArrfromstr(artist),
                                artwork: artworks_path, genres: GetArrfromstr(res_genre),
                                lyrics: Convert.ToString(lrs._info), name: Convert.ToString(tr.Name),
                                sort_composer: GetArrfromstr(sortcomposer),
                                albumArtistsSort: GetArrfromstr(sortalbumartist), albumSort: albumsort,
                                year: Convert.ToUInt32(lrs._year), grouping: Convert.ToString(lrs._grouping), sort_artist: Cur_track,
                                track_no: track_no_, albumartist: GetArrfromstr(url_release)
                            );
                }

                pbFromList.Invoke((MethodInvoker)delegate { pbFromList.PerformStep(); });
                lReadFromList.Invoke((MethodInvoker)delegate { lReadFromList.Text = "Загружено релизов  " + (i + 1).ToString() + " из " + fromlistlines.Length.ToString(); });
            }

        }

        private void bDowloadListRelease_Click(object sender, EventArgs e)
        {
            //Download_from_file();

            bwDownlodFromFile.RunWorkerAsync();

            bPathDownloadFromList.Enabled = false;
            bCheckListRelease.Enabled = false;
            bDowloadListRelease.Enabled = false;

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void bPredlisten_Click(object sender, EventArgs e)
        {
            setfolder(0);
        }

        private void bTagFiles_Click(object sender, EventArgs e)
        {
            setfolder(1);
        }

        private void bResult2_Click(object sender, EventArgs e)
        {
            setfolder(2);
        }

        private void bIgnore_Click(object sender, EventArgs e)
        {
            setfolder(3);
        }

        private void bStart2_Click(object sender, EventArgs e)
        {
            bwStartAPI2.RunWorkerAsync();
            //api2.StartDo(textBoxAPI2, tbFilter_1, tbFilter_2, progressBarApi2);
        }

        private void bCheckListRelease_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            
            if (ofd.ShowDialog() == DialogResult.OK)
            {
               
                label5.Text = ofd.FileName;
                Properties.Settings.Default.Path_from_list = ofd.FileName;
                Properties.Settings.Default.Save();

                fromlistlines = System.IO.File.ReadAllLines(label5.Text);
                foreach (var s in fromlistlines)
                {
                    Console.WriteLine(s);
                }
            }

        }

        private void StartParcingNew()
        {

            var dt_start = DateTime.Now;
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Начало = " + dt_start.ToShortTimeString() +"\n"); });
            Parser parser = new Parser(textBox1);
            bStartParsing.Invoke((MethodInvoker)delegate { bStartParsing.Enabled = false; });
            parser.FullTables();

            //Dictionary<string, ResParser> text = parser.Getinforelease("https://www.decks.de/t/johannes_heil-exile008/c9l-to");

            llastupdate.Invoke((MethodInvoker)delegate { llastupdate.Text = DateTime.Now.ToString(); });
            lbChart_last_update.Invoke((MethodInvoker)delegate { lbChart_last_update.Text = DateTime.Now.ToString(); });
            Properties.Settings.Default.LastUpdate = DateTime.Now;
            Properties.Settings.Default.Save();
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Окончание = " + DateTime.Now.ToShortTimeString() + "\n"); });
            textBox1.Invoke((MethodInvoker)delegate { textBox1.AppendText("Прододжительность = " + Math.Round((DateTime.Now - dt_start).TotalSeconds).ToString() + "\n"); });
            MessageBox.Show("База данных обновлена", "Обновление завершено", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void bw_newparsing_DoWork(object sender, DoWorkEventArgs e)
        {
            StartParcingNew();
        }

        private void bw_newparsing_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bStartParsing.Invoke((MethodInvoker)delegate { bStartParsing.Enabled = true; });
        }

        private void Charts_bt_calc_download_Click(object sender, EventArgs e)
        {
            chart.SelectData(Charts_dgv, false);
        }

        private void btChart_refresh_Click(object sender, EventArgs e)
        {
            ClearTable(Charts_dgv);
            chart.SelectData(Charts_dgv);
            //RunFilter();

            /*bConnectDB.Enabled = false; ;
            bwFilter.RunWorkerAsync();*/

            /*pDownload.Maximum = countrows-1;
            pDownload.Value = 0;
            SelectData();
            pDownload.Value = pDownload.Maximum == pDownload.Value ? pDownload.Value : pDownload.Maximum;*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sortdgv(Charts_dgv, chart.CountRows, Chart_comboBox);
        }

        private void bt_Chart_dounload_Click(object sender, EventArgs e)
        {
            //chart.DownloadMp3(Charts_dgv, "techno", "buzz");
            //chart.DownloadAll(Charts_dgv, tb_chart_log);
            bt_Chart_dounload.Enabled = false;
            bw_chart.RunWorkerAsync();
        }

        private void DownloadAll_chart(object sender, DoWorkEventArgs e)
        {
            chart.DownloadAll(Charts_dgv, tb_chart_log, pbChart, lbchart_error);
        }

        private void Cansel_Download_chart(object sender, RunWorkerCompletedEventArgs e)
        {
            bt_Chart_dounload.Invoke((MethodInvoker)delegate { bt_Chart_dounload.Enabled = true; }); 
        }

        private void bt_Chart_stop_Click(object sender, EventArgs e)
        {
            chart.killthreads();
            bt_Chart_dounload.Enabled = true;
        }

        private void bt_ChartStartParser_Click(object sender, EventArgs e)
        {
            bw_Chart_start_parser.RunWorkerAsync();
        }

        private void Chart_start_parser(object sender, DoWorkEventArgs e)
        {
            bt_ChartStartParser.Invoke((MethodInvoker)delegate { bt_ChartStartParser.Enabled = false; });
            chart.run_cmd_python();
        }

        private void Chart_parser_done(object sender, RunWorkerCompletedEventArgs e)
        {
            //llastupdate.Text = DateTime.Now.ToString();
            lbChart_last_update.Invoke((MethodInvoker)delegate { lbChart_last_update.Text = DateTime.Now.ToString(); });
            bt_ChartStartParser.Invoke((MethodInvoker)delegate { bt_ChartStartParser.Enabled = true; });
            
            Properties.Settings.Default.ChartLastUpdate = DateTime.Now;
            Properties.Settings.Default.Save();
        }

        private void bPathDownloadFromList_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog foldialog = new FolderBrowserDialog();
            foldialog.SelectedPath = Properties.Settings.Default.Path_from_list_dir;
            if (foldialog.ShowDialog() == DialogResult.OK)
            {
                pathDownloadFromList = foldialog.SelectedPath;
                label6.Text = pathDownloadFromList;
                Properties.Settings.Default.Path_from_list_dir = pathDownloadFromList;
                Properties.Settings.Default.Save();
            }
        }

        bool isStop = false;
        private void bStopTask_Click(object sender, EventArgs e)
        {
            bw_newparsing.CancelAsync();

            bwFilter.CancelAsync();
            bwDownloadRelease.CancelAsync();
            bw1.CancelAsync();
            bw2.CancelAsync();
            bwCalcNumberrelease.CancelAsync();
            try
            {
                if (process != null) process.Kill();
                bw1.CancelAsync();
                bw2.CancelAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            bDownload_rels.Enabled = true;
            //bStartParsing.Enabled = true;
            isStop = true;
        }



        //set mp3 tag
        private void SetMp3Tags(string file, string album, string[] artists, string artwork, string[] genres, string lyrics, string name,
                                string[] sort_composer,string[] albumArtistsSort, string albumSort, uint year, string grouping,
                                string[] sort_artist, uint track_no, string[] albumartist)
        {
            /*  Info of tags
            albom:		audioFile.Tag.Album	"albom_333"	string
            artist:		-		Artists	{string[1]}	string[]
            artwork:		-		Pictures	{TagLib.Id3v2.AttachedPictureFrame[1]}	TagLib.IPicture[] {TagLib.Id3v2.AttachedPictureFrame[]}
            Genre:-		Genres	{string[1]}	string[]  (		JoinedGenres	"Alternative"	string)
            Lyrics:		Lyrics	"Lyrics_333"	string
            Name:		Title	"name_333"	string
            Sort composer: -		ComposersSort	{string[1]}	string[]
            sort album artist:-		AlbumArtistsSort	{string[1]}	string[]
            sort album:		AlbumSort	"Sort_albom_333"	string
            year:		Year	1816	uint
            grouping:		Grouping	"grouping_333"	string
            sort artist:		PerformersSort	"sort_artist_333"	string

            */
            try
            {
                var audioFile = TagLib.File.Create(@file);
                audioFile.Tag.Album = album;
                audioFile.Tag.Track = track_no;
                audioFile.Tag.Artists = artists;
                //audioFile.Tag.JoinedPerformersSort  - sort artist
                //artwork
                try {
                    IPicture newArt = new Picture(artwork);
                    audioFile.Tag.Pictures = new IPicture[1] { newArt };
                }
                catch (Exception e)
                {
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n-----------------------------\n"); });
                    tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\nНе удалось загрузить artwork="+ artwork+"\n"); });
                    //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(e.ToString());
                }
        
                //--------------
                audioFile.Tag.Genres = genres;
                audioFile.Tag.Lyrics = lyrics;
                audioFile.Tag.Title = name;
                audioFile.Tag.ComposersSort = sort_composer;
                audioFile.Tag.AlbumArtistsSort = albumArtistsSort;
                audioFile.Tag.AlbumSort = albumSort;
                audioFile.Tag.Year = year;
                audioFile.Tag.Grouping = grouping;
                audioFile.Tag.PerformersSort = sort_artist;
                audioFile.Tag.Comment = "";
                audioFile.Tag.AlbumArtists = albumartist;
                audioFile.Save();
            }
            catch (Exception e) {
                //tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText(e.ToString()); });

                tbErrorLog.Invoke((MethodInvoker)delegate { tbErrorLog.AppendText("\n-----------------------------\n"); });
                //MessageBox.Show("Не найдена ссылка "+, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(e.ToString());
                /*count_errors++;
                lberrors.Invoke((MethodInvoker)delegate { lberrors.Text = count_errors.ToString(); });*/
                try
                {
                    using (StreamWriter sw = System.IO.File.AppendText("Download_error.txt"))
                    {
                        sw.WriteLine(e.ToString());
                        sw.WriteLine("--------------------------\n");
                    }
                }
                catch { }
               
            }
            
        }

        private void bDownload_rels_Click(object sender, EventArgs e)
        {
            /*string a = "Thee J Johanz - Move Your Butty  - Ballyhoo Records / Ball103B - Decks Records - Online Vinyl Shop";
            string b = GetAlbum(a);
            Console.WriteLine(b);*/

            loaded_count = 0;
            lb_ready.Text = loaded_count.ToString();
            lberrors.Text = "";

            lberrors.Text = "0";
            bwDownloadRelease.RunWorkerAsync();
            bDownload_rels.Enabled = false;

            /*SelectData(false);
            //Progress bar
            pDownload.Maximum = Convert.ToInt32(dgv.Rows[countrows-1].Cells[5].Value);
            pDownload.Value = 0;

            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = @"Data Source = " + NameBD;
                connection.Open();
                foreach (var g in genre)
                {
                    if (g == "Итого:") continue;
                    foreach (string s in sub_cat)
                    {
                        if (g != "techno") continue;
                        DowloadMp3(g, s, "new");
                    }
                    //DowloadMp3( g, "back", "classix", true);
                }
            }
            MessageBox.Show("Все файлы загружены", "Загрузка окончена", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);*/
        }

    }
}
