﻿namespace Descs_gui
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bPython = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lPython = new System.Windows.Forms.Label();
            this.bSavesettings = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chUseNewVersion = new System.Windows.Forms.CheckBox();
            this.cbClosePythonAfterFinish = new System.Windows.Forms.CheckBox();
            this.cbShowConsole = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chUseNewVersionAutoRefresh = new System.Windows.Forms.CheckBox();
            this.cbRefreshShowMessage = new System.Windows.Forms.CheckBox();
            this.cbAutosync = new System.Windows.Forms.CheckBox();
            this.dtpSynctime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lb_chart_parser = new System.Windows.Forms.Label();
            this.bt_Chart_select_script = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Установить начальное значение  sort artist";
            // 
            // bPython
            // 
            this.bPython.Location = new System.Drawing.Point(6, 19);
            this.bPython.Name = "bPython";
            this.bPython.Size = new System.Drawing.Size(187, 23);
            this.bPython.TabIndex = 1;
            this.bPython.Text = "Выбрать путь к Python.exe";
            this.bPython.UseVisualStyleBackColor = true;
            this.bPython.Click += new System.EventHandler(this.bPython_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(227, 61);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // lPython
            // 
            this.lPython.AutoSize = true;
            this.lPython.Location = new System.Drawing.Point(224, 24);
            this.lPython.Name = "lPython";
            this.lPython.Size = new System.Drawing.Size(35, 13);
            this.lPython.TabIndex = 3;
            this.lPython.Text = "label2";
            // 
            // bSavesettings
            // 
            this.bSavesettings.Location = new System.Drawing.Point(334, 499);
            this.bSavesettings.Name = "bSavesettings";
            this.bSavesettings.Size = new System.Drawing.Size(210, 39);
            this.bSavesettings.TabIndex = 4;
            this.bSavesettings.Text = "Сохранить";
            this.bSavesettings.UseVisualStyleBackColor = true;
            this.bSavesettings.Click += new System.EventHandler(this.bSavesettings_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chUseNewVersion);
            this.groupBox1.Controls.Add(this.cbClosePythonAfterFinish);
            this.groupBox1.Controls.Add(this.cbShowConsole);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.bPython);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.lPython);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(532, 235);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Основные настройки";
            // 
            // chUseNewVersion
            // 
            this.chUseNewVersion.AutoSize = true;
            this.chUseNewVersion.Location = new System.Drawing.Point(9, 205);
            this.chUseNewVersion.Name = "chUseNewVersion";
            this.chUseNewVersion.Size = new System.Drawing.Size(174, 17);
            this.chUseNewVersion.TabIndex = 13;
            this.chUseNewVersion.Text = "Использовать новую версию";
            this.chUseNewVersion.UseVisualStyleBackColor = true;
            // 
            // cbClosePythonAfterFinish
            // 
            this.cbClosePythonAfterFinish.AutoSize = true;
            this.cbClosePythonAfterFinish.Location = new System.Drawing.Point(227, 182);
            this.cbClosePythonAfterFinish.Name = "cbClosePythonAfterFinish";
            this.cbClosePythonAfterFinish.Size = new System.Drawing.Size(257, 17);
            this.cbClosePythonAfterFinish.TabIndex = 12;
            this.cbClosePythonAfterFinish.Text = "Закрывать окно парсинга после завершения";
            this.cbClosePythonAfterFinish.UseVisualStyleBackColor = true;
            this.cbClosePythonAfterFinish.Visible = false;
            // 
            // cbShowConsole
            // 
            this.cbShowConsole.AutoSize = true;
            this.cbShowConsole.Location = new System.Drawing.Point(9, 182);
            this.cbShowConsole.Name = "cbShowConsole";
            this.cbShowConsole.Size = new System.Drawing.Size(166, 17);
            this.cbShowConsole.TabIndex = 11;
            this.cbShowConsole.Text = "Показывать окно парсинга";
            this.cbShowConsole.UseVisualStyleBackColor = true;
            this.cbShowConsole.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(224, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "label2";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(9, 132);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(184, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Выбрать папку для загрузки";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Выбрать скрипт для запуска";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chUseNewVersionAutoRefresh);
            this.groupBox2.Controls.Add(this.cbRefreshShowMessage);
            this.groupBox2.Controls.Add(this.cbAutosync);
            this.groupBox2.Controls.Add(this.dtpSynctime);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 258);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 138);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Автообновление";
            // 
            // chUseNewVersionAutoRefresh
            // 
            this.chUseNewVersionAutoRefresh.AutoSize = true;
            this.chUseNewVersionAutoRefresh.Location = new System.Drawing.Point(9, 119);
            this.chUseNewVersionAutoRefresh.Name = "chUseNewVersionAutoRefresh";
            this.chUseNewVersionAutoRefresh.Size = new System.Drawing.Size(174, 17);
            this.chUseNewVersionAutoRefresh.TabIndex = 13;
            this.chUseNewVersionAutoRefresh.Text = "Использовать новую версию";
            this.chUseNewVersionAutoRefresh.UseVisualStyleBackColor = true;
            // 
            // cbRefreshShowMessage
            // 
            this.cbRefreshShowMessage.AutoSize = true;
            this.cbRefreshShowMessage.Location = new System.Drawing.Point(9, 88);
            this.cbRefreshShowMessage.Name = "cbRefreshShowMessage";
            this.cbRefreshShowMessage.Size = new System.Drawing.Size(143, 17);
            this.cbRefreshShowMessage.TabIndex = 3;
            this.cbRefreshShowMessage.Text = "Показывать соощения";
            this.cbRefreshShowMessage.UseVisualStyleBackColor = true;
            // 
            // cbAutosync
            // 
            this.cbAutosync.AutoSize = true;
            this.cbAutosync.Location = new System.Drawing.Point(9, 28);
            this.cbAutosync.Name = "cbAutosync";
            this.cbAutosync.Size = new System.Drawing.Size(161, 17);
            this.cbAutosync.TabIndex = 2;
            this.cbAutosync.Text = "Включить автообновление";
            this.cbAutosync.UseVisualStyleBackColor = true;
            // 
            // dtpSynctime
            // 
            this.dtpSynctime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpSynctime.Location = new System.Drawing.Point(111, 56);
            this.dtpSynctime.Name = "dtpSynctime";
            this.dtpSynctime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpSynctime.ShowUpDown = true;
            this.dtpSynctime.Size = new System.Drawing.Size(91, 20);
            this.dtpSynctime.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Время старта:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lb_chart_parser);
            this.groupBox3.Controls.Add(this.bt_Chart_select_script);
            this.groupBox3.Location = new System.Drawing.Point(12, 403);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(532, 69);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Charts";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // lb_chart_parser
            // 
            this.lb_chart_parser.AutoSize = true;
            this.lb_chart_parser.Location = new System.Drawing.Point(224, 35);
            this.lb_chart_parser.Name = "lb_chart_parser";
            this.lb_chart_parser.Size = new System.Drawing.Size(35, 13);
            this.lb_chart_parser.TabIndex = 11;
            this.lb_chart_parser.Text = "label5";
            // 
            // bt_Chart_select_script
            // 
            this.bt_Chart_select_script.Location = new System.Drawing.Point(9, 29);
            this.bt_Chart_select_script.Name = "bt_Chart_select_script";
            this.bt_Chart_select_script.Size = new System.Drawing.Size(184, 23);
            this.bt_Chart_select_script.TabIndex = 10;
            this.bt_Chart_select_script.Text = "Выбрать скрипт для запуска";
            this.bt_Chart_select_script.UseVisualStyleBackColor = true;
            this.bt_Chart_select_script.Click += new System.EventHandler(this.bt_Chart_select_script_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 550);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bSavesettings);
            this.Name = "Form2";
            this.Text = "Настройки";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bPython;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lPython;
        private System.Windows.Forms.Button bSavesettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbClosePythonAfterFinish;
        private System.Windows.Forms.CheckBox cbShowConsole;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpSynctime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbAutosync;
        private System.Windows.Forms.CheckBox cbRefreshShowMessage;
        private System.Windows.Forms.CheckBox chUseNewVersion;
        private System.Windows.Forms.CheckBox chUseNewVersionAutoRefresh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lb_chart_parser;
        private System.Windows.Forms.Button bt_Chart_select_script;
    }
}