﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Descs_gui
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            pathpython = Properties.Settings.Default.Python_path;
            lPython.Text = pathpython;
            textBox1.Text = Properties.Settings.Default.Current_track.ToString();
            folder_out_mp3 = Properties.Settings.Default.Path_out_MP3;
            label3.Text = folder_out_mp3;
            python_script = Properties.Settings.Default.Main_parser;
            label2.Text = python_script;
            cbShowConsole.Checked = Properties.Settings.Default.ShowConsolePython;
            cbClosePythonAfterFinish.Checked= Properties.Settings.Default.CloasePythonAfterFinish;
            chUseNewVersion.Checked = Properties.Settings.Default.UseNewVersionParsing;

            //autosync
            cbAutosync.Checked = Properties.Settings.Default.isSync;
            dtpSynctime.Value = DateTime.Today + Properties.Settings.Default.SyncTime;
            cbRefreshShowMessage.Checked = Properties.Settings.Default.Autorefresh_isshoemwessages;
            chUseNewVersionAutoRefresh.Checked = Properties.Settings.Default.UseNewVersionParsingAuto;

            //chart
            chart_python_script = Properties.Settings.Default.Chart_parser;
            lb_chart_parser.Text = chart_python_script;
        }

        string pathpython, python_script, folder_out_mp3, chart_python_script;

        private void bSavesettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Python_path = pathpython;
            Properties.Settings.Default.Main_parser = python_script;
            Properties.Settings.Default.Current_track = Convert.ToInt32(textBox1.Text);
            Properties.Settings.Default.ShowConsolePython = cbShowConsole.Checked;
            Properties.Settings.Default.CloasePythonAfterFinish = cbClosePythonAfterFinish.Checked;
            Properties.Settings.Default.Path_out_MP3 = folder_out_mp3;
            Properties.Settings.Default.UseNewVersionParsing = chUseNewVersion.Checked;

            //autosync
            Properties.Settings.Default.isSync = cbAutosync.Checked;
            Properties.Settings.Default.SyncTime = dtpSynctime.Value.TimeOfDay;
            Properties.Settings.Default.Autorefresh_isshoemwessages = cbRefreshShowMessage.Checked;
            Properties.Settings.Default.UseNewVersionParsingAuto = chUseNewVersionAutoRefresh.Checked;

            Properties.Settings.Default.Chart_parser = python_script;

            Properties.Settings.Default.Save();
            this.Close();
        }

        private void bPython_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pathpython = openFileDialog1.FileName;
                lPython.Text = pathpython;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void bt_Chart_select_script_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                chart_python_script = openFileDialog1.FileName;
                lb_chart_parser.Text = chart_python_script;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog foldialog = new FolderBrowserDialog();
            if (foldialog.ShowDialog() == DialogResult.OK)
            {
                folder_out_mp3 = foldialog.SelectedPath;
                label3.Text = folder_out_mp3;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                python_script = openFileDialog1.FileName;
                label2.Text = python_script;
            }
        }
    }
}
