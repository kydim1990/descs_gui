﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HAP = HtmlAgilityPack;
using Newtonsoft.Json;

namespace Descs_gui
{

    class LReleases
    {

        public string _title = "";
        public string _artist = "";
        public string _artworkurl = "";
        public string _info = "";
        public string _catno = "";
        public string _label = "";
        public string _genre = "";
        public string _new = "";
        public string _back = "";
        public string _sub = "";
        public string _year = "";
        public string _grouping = "";
        private int _div_no_track = 0;

        private string _html;
        private string _url;
        
        private HAP.HtmlDocument _doc;

        public List<Track> tracks = new List<Track>();

        private Dictionary<string, string> _tracks = new Dictionary<string, string>();

        private const string GROUPING_TEXT = "decks.de pre-listen ";

        private static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return input;
                //throw new ArgumentException("ARGH!");
            }
                
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        private string TagFromsrc(string s)
        {
            string a = s.Split('/').Last();
            string b = a.Split('_').Last();
            return b.Split('.').First();
        }

        public LReleases(string url)
        {
            _url = url;
            _doc = new HAP.HtmlDocument();

            WebClient client = new WebClient();

            string html = client.DownloadString(url);

            _html = html;
            _doc.LoadHtml(_html);

            //title
            _title = SetValue("div", "class", "detail_titel", true, "title");
            
            //artist
            _artist = SetValue("div", "class", "detail_artist", true);

            //artworkurl
            _artworkurl = SetArtworkurl();

            //info
            _info = SetValue("div", "id", "info_memo");

            //label
            //List<string> labelpage  = SetValueArr("div", "class", "txt_label");
            string[] labelpage = SetValue("div", "class", "detail_label").Split('/');
            _label = labelpage[0].Trim();

            //catno
            if (labelpage.Length > 1) _catno = labelpage[1].Trim();

            //genre
            _genre = SetValue("div", "class", "LStylehead");

            List<string> sub_arr = new List<string>();

            foreach (var cell in _doc.DocumentNode.SelectNodes("//div[@class='LStyle']").
                Where(p => p.InnerText.Trim().ToUpper() != _genre.ToUpper())  )
            {
                string val = cell.InnerText.Trim();
                Console.WriteLine("cell "+ val);
                sub_arr.Add(val);
            }

            if (sub_arr.Count() > 0)
                _sub = String.Join(", ", sub_arr);

            var info_date = _doc.DocumentNode
                    .Descendants("div")
                    .Where(d =>
                        d.Attributes.Contains("class")
                            &&
                        d.Attributes["class"].Value.Contains("info_dateformat")
                    ).First();

            var infodateval = info_date.Descendants("div")
                    .Where(d =>
                        d.Attributes.Contains("class")
                            &&
                        d.Attributes["class"].Value.Contains("infodateval")
                    );

            _new = infodateval.First().InnerText.Trim();

            if (infodateval.Count() > 1)
                _back = infodateval.Last().InnerText.Trim();
            
            var year_arr = _new.Split('.');

            if (year_arr.Length > 2)
            {
                _year = year_arr[2];
                _grouping = GROUPING_TEXT + year_arr[2].Substring(2) + year_arr[1];
            }

            // find tracks
            var tracks_ = _doc.DocumentNode
                    .Descendants("div")
                    .Where(d =>
                        d.Attributes.Contains("class")
                            &&
                        d.Attributes["class"].Value.Contains("listenMaxWidth")
                    );

            /*if (tracks_.Count() == 0)
            {
                tracks_ = _doc.DocumentNode
                .Descendants("div")
                .Where(d =>
                    d.Attributes.Contains("class")
                        &&
                    d.Attributes["class"].Value.Contains("soundtxt")
                );

                if (tracks_.Count() == 0)
                    return;
            }*/

            var is_defis = true;
            int a = tracks_.Count();
            foreach (var t in tracks_)
            {
                var text = t.InnerText;

                var tmp_defis = text.Replace('\x96', '-').Replace('\x97', '-').Trim();
                if (tmp_defis.IndexOf('-') < 0)
                {
                    is_defis = false;
                    break;
                }
            }

            foreach (var t in tracks_)
            {
                var text1 = t.InnerText.Trim();
                var span_res = t.Descendants("span")
                   .Where(d =>
                       d.Attributes.Contains("class")
                           &&
                       d.Attributes["class"].Value.Contains("RecPos")
                   ).First().InnerText.Trim();

                text1 = text1.Substring(span_res.Length);
                
                string href = null;
                bool is_notlogin = false;
                try
                {
                    href = t.Attributes["href"].Value;
                }
                catch (NullReferenceException e)
                {
                    //not autorization, use div
                    HAP.HtmlNode mdnode = _doc.DocumentNode.SelectSingleNode("//meta[@property='og:url']");

                    if (mdnode != null)
                    {
                        HAP.HtmlAttribute desc;
                        desc = mdnode.Attributes["content"];
                        href = desc.Value;
                        is_notlogin = true;
                        Console.WriteLine("DIV href: " + href);
                    }
                }

                int no_track=0;
                
                no_track = Convert.ToInt16(t.Attributes["data-track"].Value.Trim());
                Console.WriteLine("no_track= " + no_track.ToString());
                var text = t.LastChild.InnerText.Trim();
                //Geturl(href);
                string[] tts = GetTitleTrack(text1, is_defis, no_track, span_res, _artist);
                string url_track = Geturl(href, is_notlogin);
                Track tr = new Track(url_track, tts[0], tts[1], no_track);
                if (tr.Name.IndexOf("Please login for sounds") > -1)
                {
                    _div_no_track--;
                    continue;
                }
                   
                tracks.Add(tr);
                //no_track = str(int(t.get('href').rsplit('-', 1)[1]) + 1)
            }


            Console.WriteLine("title= " + _title);
            Console.WriteLine("_artist= " + _artist);
            Console.WriteLine("_artworkurl= " + _artworkurl);
            Console.WriteLine("_info= " + _info);
            Console.WriteLine("_label= " + _label);
            Console.WriteLine("_catno= " + _catno);
            Console.WriteLine("NEW=" + _new);
        }

        private string Geturl(string text, bool isnotlogin=false)
        {
            string URLTRACK = "http://www.decks.de/decks-sess/rpc/getAudio.php?client=none&id={0}&t={1}";
            string url = null;
            if (isnotlogin)
            {
                url = String.Format(URLTRACK, text.Substring(text.LastIndexOf('/')+1), ++_div_no_track);
            }
            else
            {
                string s_text = text.Substring(text.LastIndexOf('/'));
                string s_text_1 = s_text.Substring(1, s_text.LastIndexOf('-') - 1);
                string s_text_2 = s_text.Substring(s_text.LastIndexOf('-') + 1);

                string num = (Convert.ToInt32(s_text_2) + 1).ToString();

                url = String.Format(URLTRACK, s_text_1, num);
                Console.WriteLine("s_text_1= " + s_text_1);
                Console.WriteLine("s_text_2= " + s_text_2);
            }

        
            Console.WriteLine("url= " + url);

            string html = new WebClient().DownloadString(url);
            dynamic json = JsonConvert.DeserializeObject<dynamic>(html);

            var b=json.Property("sound");

            string res = json["sound"];
            Console.WriteLine("htmlurl= " + html);
            Console.WriteLine("json= " + json["sound"]);

            return res;
        }

        private string[] GetTitleTrack(string text, bool is_defis =true, int no_track=-1, string span_res=null, string artist_rel = null)
        {
            string _name = "";

            string _artist = artist_rel, _nt = null;
            var _text = text.Replace('\x96', '-').Replace('\x97', '-').Trim();
            _name = _text;
   
            if (_name == null)
            {
                return new string[] { FirstCharToUpper(_text) + " ("+ _nt+")", this._artist }; ;
            }

            string tmp = "";

            if (_name.IndexOf("-") >= 0 && is_defis)
            {
                tmp = _name.Substring(_name.IndexOf('-') + 1);
                int indexif_ = _name.IndexOf(tmp);
                if (indexif_ > 2)
                    _artist = _name.Substring(0, indexif_ - 2).Trim();
                else
                    _artist = tmp.Trim();
                //_name = _name.Substring(tmp.IndexOf('-') + 1);
                _name = _name.Split('-')[1].Trim();
            }

            if (span_res.Length > 0)
                _name = String.Format("{0} ({1})", _name, FirstCharToUpper(span_res));            

            string[] res = new string[] { FirstCharToUpper(_name), FirstCharToUpper(_artist) };

            return res;
        }

        private int CountWords_def(string s, string s0)
        {
            int count = 0;
            try
            {
                count = (s.Length - s.Replace(s0, "").Length) / s0.Length;
            }
            catch
            {
                return 0;
            }
            return count;
        }

        private int CountWords(string s, string s0)
        {
            int count = (s.Length - s.Replace(s0, "").Length) / s0.Length;
            return count;
        }

        private string SetValue(string tag, string containts, string valcont, bool isCap = false, string name = null)
        {
            var tmp = _doc.DocumentNode
                    .Descendants(tag)
                    .Where(d =>
                        d.Attributes.Contains(containts)
                            &&
                        d.Attributes[containts].Value.Contains(valcont)
                    ).First().InnerText.Trim();

            if (name == "title")
                tmp = tmp.ToUpper() == "various artists".ToUpper() || tmp.ToUpper() == "V/A" ? "Various Artists" : tmp;

            if (isCap) tmp = FirstCharToUpper(tmp);
            return tmp;
        }

        //return two value
        private List<string> SetValueArr(string tag, string containts, string valcont, bool isCap = false, string name = null)
        {
            //_doc.LoadHtml(_html);
            var tmp = _doc.DocumentNode
                    .Descendants(tag)
                    .Where(d =>
                        d.Attributes.Contains(containts)
                            &&
                        d.Attributes[containts].Value.Contains(valcont)
                    );


            List<string> res = new List<string>();

            foreach (var i in tmp)
            {
                Console.WriteLine(i.InnerText.Trim());
                res.Add(i.InnerText.Trim());
            }

            return res;
        }

        private string SetArtist()
        {
            var tmp = _doc.DocumentNode
                    .Descendants("h1")
                    .Where(d =>
                        d.Attributes.Contains("class")
                            &&
                        d.Attributes["class"].Value.Contains("txt_artist")
                    ).First().InnerText.Trim();

            return FirstCharToUpper(tmp);
        }

        private string SetArtworkurl()
        {
            var tmp = _doc.DocumentNode
                    .Descendants("img")
                    .Where(d =>
                        d.Attributes.Contains("id")
                            &&
                        d.Attributes["id"].Value.Equals("zoom_front")
                    ).First().GetAttributeValue("src", "");

            return tmp;
        }



    }
}
