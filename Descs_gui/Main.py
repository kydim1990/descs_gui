# -*- coding: latin-1 -*-

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db import Songslinks, Mp3tag, Base, Tracks, change_status
from Parser import getalllinks, getinforelease, print_to_file
from sqlalchemy import delete, update
from datetime import datetime
import os
import traceback
from charts import update_chart


fmt = '%Y-%m-%d %H:%M:%S'

database_file = os.path.join(os.path.dirname(__file__), 'Deck.db')
engine = create_engine('sqlite:///%s' % database_file,)
engine.raw_connection().connection.text_factory = str
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


def addsongslinks():
    try:
        alllinks = getalllinks()
    except Exception, e:
        print traceback.format_exc()
    for b in alllinks:
        try:
            print "Write in db", b
            print_to_file("Write in db")
            for c in alllinks[b]:
                for d in c:
                    # print d
                    sl = Songslinks(category=b, url=d, isold=False)
                    try:
                        session.merge(sl)
                    except:
                        if session.is_active:
                            session.rollback()
            try:
                session.commit()
            except:
                print 'error in AddSongslinks'
                if session.is_active:
                    session.rollback()
        except:
            pass


def setoldsongs():
    session = DBSession()
    s_upd = update(Songslinks).values(isold=True)
    session.execute(s_upd)
    session.commit()


def addmp3():
    Alllinks = Songslinks.query.all()
    i = 0
    for s in Alllinks:
        try:
            i += 1
            print 'Write mp3 in DB=', i
            print_to_file('Write mp3 in DB= '+str(i))
            Mp3tag_exist = Mp3tag.query.filter_by(url=s.url).first()
            if Mp3tag_exist:
                continue
            print s.url
            info_mp3 = getinforelease(s.url)
            # print info_mp3
            if info_mp3 is None:
                continue

            for inm, rowmp3 in info_mp3.iteritems():
                url_ = inm
                category_ = s.category.split('_')[0]
                subcategory_ = s.category.split('_')[1]
                title_ = rowmp3['title']
                artist_ = rowmp3['artist']
                artwork_ = rowmp3['artwork']
                catno_ = rowmp3['catno']
                genre_ = rowmp3['genre']
                sub_ = rowmp3['sub']
                info_ = rowmp3['info']
                back_ = rowmp3['back']
                new_ = rowmp3['new']
                label_ = rowmp3['label']
                year_ = rowmp3['year']
                grouping_ = rowmp3['grouping']
                # add mp3 in db
                title_tmp = title_
                artist_rmp = artist_
                try:
                    title_sql = title_tmp.decode('utf8')
                    artist_sql = artist_rmp.decode('utf8')
                except:
                    title_sql = title_tmp
                    artist_sql = artist_rmp

                mp3row = Mp3tag(url=url_, category=category_, subcategory=subcategory_, title=title_sql, artist=artist_sql, catno=catno_,
                              artwork=artwork_, genre=genre_, sub=sub_, info=info_, back=back_, new=new_, label=label_, year=year_,
                              grouping=grouping_)

                try:
                    session.merge(mp3row)
                except:
                    session.rollback()
                    continue
                # write tracks
                tracks_d = rowmp3['tracks_dict']
                # print tracks_d
                for tr in tracks_d:
                    # print tr
                    # print tracks_d[tr]
                    name_tmp = tracks_d[tr][0][0]
                    track_artist_rmp = tracks_d[tr][0][1]
                    try:
                        name_sql = name_tmp.decode('utf8')
                        track_artist_sql = track_artist_rmp.decode('utf8')
                    except:
                        name_sql = name_tmp
                        track_artist_sql = track_artist_rmp

                    track_row = Tracks(url=tr, url_release=url_, name=name_sql,
                                       no_track=tracks_d[tr][1],
                                       track_artist=track_artist_sql)
                    try:
                        session.merge(track_row)
                        # session.commit()
                    except Exception, e:
                        print str(e.message)
                        session.rollback()

            try:
                session.commit()
            except Exception, e:
                print 'error commit'
                print str(e)
                session.rollback()

        except Exception, e:
            print str(e)
            print traceback.format_exc()
            print 'error AddMp3'
        # print s.url, s.category


def deleteold():
    session_db = DBSession()
    links_to_delete = Songslinks.query.filter_by(isold=True)
    links_to_delete_url = links_to_delete.with_entities(Songslinks.url)
    track_to_delete = delete(Tracks).where(Tracks.url_release.in_(links_to_delete_url))
    track_to_mp3 = delete(Mp3tag).where(Mp3tag.url.in_(links_to_delete_url))

    session_db.execute(track_to_delete)
    session_db.execute(track_to_mp3)
    Songslinks_to_delete = delete(Songslinks).where(Songslinks.isold)
    session_db.execute(Songslinks_to_delete)

    try:
        session_db.commit()
    except:
        print 'error commit DeleteOld Chart'
        if session_db.is_active:
            session_db.rollback()

    # session = DBSession()
    # links_to_delete = Songslinks.query.filter_by(isold=True)
    # for l in links_to_delete:
    #     try:
    #         # delete tracks
    #         print 'delete old tracks '
    #         print_to_file('delete old tracks ')
    #         track_to_delete = delete(Tracks).where(Tracks.url_release == l.url)
    #         session.execute(track_to_delete)
    #         # session.commit()
    #         #delete mp3 links
    #         print 'delete old mp3s', l.url
    #         print_to_file('delete old tracks ')
    #         mp3links = delete(Mp3tag).where(Mp3tag.url == l.url)
    #         session.execute(mp3links)
    #         # session.commit()
    #         # delete links
    #         print 'delete old links', l.url
    #         print_to_file('delete old tracks ')
    #         Songslinks_to_delete = delete(Songslinks).where(Songslinks.url == l.url)
    #         session.execute(Songslinks_to_delete)
    #         try:
    #             session.commit()
    #         except:
    #             print 'error commit DeleteOld'
    #             if session.is_active:
    #                 session.rollback()
    #     except:
    #         pass


def fulltables():
    open('./output.txt', 'w').close()
    d1 = datetime.now()
    print d1
    print_to_file(str(d1))
    try:
        change_status(oper='main', status='running')
        # set all link as old
        setoldsongs()
        # add new and update exist rows
        addsongslinks()
        # # delete old
        deleteold()
        # add mp3
        addmp3()
        change_status(oper='main', status='finish')
    except Exception as e:
        print str(e)
    d2 = datetime.now()
    print d2
    print "Scrapping has been finished, time ", d2-d1
    # raw_input("Press Enter to Exit...")


if __name__=="__main__":
    # print str(sys.argv)
    # raw_input("Press Enter to Exit...")
    # if len(sys.argv) > 1:
    #     raw_input("Press Enter to Exit...")
    update_chart()
    fulltables()
    # AddSongslinks()
    # d1=datetime.now()
    # AddMp3()
    # d2 = datetime.now()
    # print "Scrapping has been finished, time ", d2-d1
    # setoldsongs()
    # DeleteOld()