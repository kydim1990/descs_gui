﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data.Common;
using HAP = HtmlAgilityPack;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;

namespace Descs_gui
{
    class Parser
    {
        bool IsFirstValues = false;
        int break_value = 5;

        public TextBox tblog;

        private List<SQLiteCommand> sqlcommands = new List<SQLiteCommand>();

        private HAP.HtmlDocument doc = new HAP.HtmlDocument();

        private string[] categories= new string[]{"techno_new", "techno_classix", "house_new", "house_classix", "black_new", "black_classix", "bass_new", "bass_classix"};

        private Dictionary<string, string> TECHNO = new Dictionary<string, string>{
            { "new", "ten" },
            { "classix", "tec" }
        };

        private Dictionary<string, string> HOUSE = new Dictionary<string, string>{
            { "new", "hon" },
            { "classix", "hoc" }
        };

        private Dictionary<string, string> BASS = new Dictionary<string, string>{
            { "new", "ban" },
            { "classix", "bac" }
        };

        private Dictionary<string, string> BLACK = new Dictionary<string, string>{
            { "new", "bln" },
            { "classix", "blc" }
        };

        private string MAINHEAD = "http://www.decks.de/decks-sess/workfloor/lists/list.php?wo=CATEGORYNAME&now_Sub=zz&now_Was=news&now_Date=nodate&aktuell=";
        private string FIRSTHEAD = "http://www.decks.de/decks-sess/workfloor/lists/list.php?wo=CATEGORYNAME&now_Sub=zz";
        private string FINDTEG = "CATEGORYNAME";
        private string URLTRACK = "http://www.decks.de/decks-sess/rpc/getAudio.php?client=none&id={0}&t={1}";

        private string GROUPING_TEXT = "decks.de pre-listen ";

        public Parser(TextBox tb)
        {
            tblog = tb;
        }

        private static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return input;
                //throw new ArgumentException("ARGH!");
            }

            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        private void SetOld()
        {
            Console.WriteLine("Start SetOld");
            AddLog("Старт установка старых значений");
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format("UPDATE Songslinks set isold=1");
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("End SetOld");
            AddLog("Окончание установка старых значений");
        }

        private Dictionary<string, Dictionary<string, string>> Getalldefoualurls(bool isall=false)
        {
            Dictionary<string, Dictionary<string, string>> defres = new Dictionary<string, Dictionary<string, string>>();
            string tag = isall ? MAINHEAD : FIRSTHEAD;

            defres.Add("techno", new Dictionary<string, string>{ { "new", tag.Replace(FINDTEG, TECHNO["new"])},   { "classix", tag.Replace(FINDTEG, TECHNO["classix"]) } });
            defres.Add("house", new Dictionary<string, string> { { "new", tag.Replace(FINDTEG, HOUSE["new"]) }, { "classix", tag.Replace(FINDTEG, HOUSE["classix"]) } });
            defres.Add("black", new Dictionary<string, string> { { "new", tag.Replace(FINDTEG, BLACK["new"]) }, { "classix", tag.Replace(FINDTEG, BLACK["classix"]) } });
            defres.Add("bass", new Dictionary<string, string> { { "new", tag.Replace(FINDTEG, BASS["new"]) }, { "classix", tag.Replace(FINDTEG, BASS["classix"]) } });

            if (!isall)
            {
                defres["bass"]["new"] = defres["bass"]["new"].Replace("list.php", "list_db.php");
                defres["bass"]["classix"] = defres["bass"]["classix"].Replace("list.php", "list_db.php");

                /*foreach (var a in defres["bass"])
                {
                    defres["bass"][a.Key] = defres["bass"][a.Key].Replace("list.php", "list_db.php");
                }*/
            }

            return defres;
        }

        private int Getcountpage(string url)
        {
            string favorite = "0";
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                    string r = client.DownloadString(url);
                    doc.LoadHtml(r);
                    var maxpages = doc.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("nav-textmen"));
                    

                    foreach(var m in maxpages)
                    {
                        var b = m.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("linkTextMen"));
                        foreach(var i in b)
                        {
                            string i_text = i.InnerText.Trim();
                            if (i_text.IndexOf("...") > -1)
                            {
                                favorite = i_text.Replace(".", "");
                                break;
                            }
                            else
                            {
                                if (i_text.Length == 1) favorite = i_text;
                                else
                                {
                                    try
                                    {
                                        if (Convert.ToInt32(i_text) > Convert.ToInt32(favorite))
                                            favorite = i_text;
                                    }
                                    catch(Exception ex) { Console.WriteLine(ex.Message); }
                                }
                            }
                        }
                    }

                }
            }
            catch (WebException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return Convert.ToInt32(favorite);
        }

        private List<string> Getlinksfrompage(string url)
        {
            List<string> res = new List<string>();
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                    string r = client.DownloadString(url);
                    doc.LoadHtml(r);
                    //var songs = doc.DocumentNode.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_artist"));
                    var songs = doc.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("cover1"));

                    foreach ( var s in songs)
                    {
                        var songref = s.Descendants("a").First();
                        res.Add(songref.GetAttributeValue("href", ""));
                        //res.Add(s.GetAttributeValue("href", ""));
                    }

                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return res;
           
        }

        public Dictionary<string, List<List<string>>> Getalllinks()
        {
            Dictionary<string, Dictionary<string, string>> alldefs = Getalldefoualurls();
            Dictionary<string, Dictionary<string, string>> allurls = Getalldefoualurls(true);

            Dictionary<string, List<List<string>>> res = new Dictionary<string, List<List<string>>>();
            int count_ = 0;
            foreach(var a in alldefs)
            {
                var subcateg = alldefs[a.Key];
                int maxcount;
                foreach (var b in subcateg)
                {
                    maxcount = Getcountpage(subcateg[b.Key]);
                    AddLog(" ");
                    AddLog("    Найдено " + a.Key+ "_"+ b.Key+ " количество страниц = " + maxcount.ToString());
                    List<List<string>> resarr = new List<List<string>>();
                    AddLog("        Просмотр страницы  ", false);
                    for (int i = 0; i < maxcount; i++)
                    {
                        AddLog("_" + (i + 1).ToString(),false);
                        var cururl = allurls[a.Key][b.Key] + i.ToString();
                        if (a.Key == "bass")
                            cururl = cururl.Replace("list.php", "list_db.php");

                        List<string> restemp;
                        try
                        {
                            //AddLog("   Getlinksfrompage =" + cururl);
                            restemp = Getlinksfrompage(cururl);
                        }
                        catch 
                        {
                            continue;
                        }
                        resarr.Add(restemp);
                        count_++;
                        if (count_ % 10 == 0)
                        {
                            //AddLog("   Уже Получено ссылок =" + count_.ToString());
                            //break;
                        }
                        if (IsFirstValues && i > break_value) break;

                            
                    }
                    //AddLog("   Получено ссылок =" + subcateg.Count());
                    res.Add(a.Key + "_" + b.Key, resarr);
                }
            }
            AddLog(" ");
            return res;
        }

        private void DeleteOld()
        {
            Console.WriteLine("Start DeleteOld");
            AddLog("Старт удаление старых записей");
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format("delete from  Tracks where url_release in (select url from Songslinks where isold=1)");
                        SQLiteDataReader r = command.ExecuteReader();
                    }

                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format("delete from  Mp3tag where url in (select url from Songslinks where isold=1)");
                        SQLiteDataReader r = command.ExecuteReader();
                    }

                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format("delete from Songslinks where isold=1");
                        SQLiteDataReader r = command.ExecuteReader();
                    }

                        /*command.CommandText = String.Format("select * from  Songslinks where isold=1");
                        SQLiteDataReader r = command.ExecuteReader();
                        while (r.Read())
                        {
                            Console.WriteLine("delete old tracks ");
                            string url_release = Convert.ToString(r["url_release"]);
                            //delete old tracks
                            command.CommandText = String.Format("DELETE from Tracks where url_release = @url_release");
                            command.Parameters.AddWithValue("@url_release", url_release);
                            command.ExecuteNonQuery();
                            //delete mp3 links
                            command.CommandText = String.Format("DELETE from Mp3tag where url = @url_release");
                            command.Parameters.AddWithValue("@url", url_release);
                            command.ExecuteNonQuery();
                            //delete links
                            command.CommandText = String.Format("DELETE from Songslinks where url = @url_release");
                            command.Parameters.AddWithValue("@url", url_release);
                            command.ExecuteNonQuery();
                        }*/
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("End DeleteOld");
            AddLog("Окончание удаление старых записей");
        }

        private string Tagfromsrc(string s)
        {
            string a = s.Split('/').Last();
            string b = a.Split('_').Last();
            string c = b.Split('.').First();
            return c;
        }

        private string Geturl(WebClient client, string text)
        {

            var s_text = text.Substring(text.LastIndexOf('/')+1);
            var LastIndexOf = s_text.LastIndexOf('-');
            string num = (Convert.ToInt32(s_text.Substring(LastIndexOf+1) + 1)).ToString();
            string id = s_text.Substring(0, LastIndexOf);
            string url = String.Format(URLTRACK,id, num);

            try
            {
                //using (WebClient client = new WebClient())
                {
                    //client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                    string r = client.DownloadString(url);
                    var res = JsonConvert.DeserializeObject<Dictionary<string, string>>(r);
                    return res["sound"].Replace("\\", "");
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return null;
        }
           
        private string[] Get_title_track(string text_orig, bool is_defis = true, string no_track = null)
        {
            char char_split = '.';
            string artist = null;
            string nt = null;
            string name = null, subname =null;
            string text = text_orig.Replace("\u0150", "-").Replace("\u0151", "-");
            if (text.Split(char_split).Length > 2)
                text = text.Split(char_split)[1].Trim() + "." + text.Split(char_split)[2].Trim();

            var a = text.Split(':');
            if (a.Length > 1)
            {
                name = a[1];
                if (a[0].Split(char_split).Length > 2)
                    subname = a[0].Split(char_split)[1];
                else
                    subname = a[0].Trim();
            }
            else
            {
                var b = a[0].Split('.');
                if (b.Length > 1)
                    name = b[1];
                else if (text.Split('-').Length > 1)
                {
                    var c = text.Split('-');
                    name = c[1].Trim();
                    artist = c[0].Trim();
                }
                else
                    name = "";
            }
            if (no_track != null)
                nt = no_track;
                 //nt = String.Format("{0:0.00}", Convert.ToInt16(no_track));
            
            //name = name.Replace("\u", "");
            if (name.IndexOf('-') > 0 && is_defis)
            {
                var tmp = name.Substring(name.IndexOf('-') + 1);
                artist = name.Substring(0, name.IndexOf('-') - 1 ).Trim();
                artist = FirstCharToUpper(artist);
                name = name.Substring(name.IndexOf('-') + 1);
            }
            if (subname != null &&  subname.Split(char_split).Length > 1)
            {
                subname = subname.Split('.')[1];
            }

            if (name.Length == 0)
                name = text;
            else if (subname == null)
                name = FirstCharToUpper(name.Trim());
            else
                name = FirstCharToUpper(name.Trim() + '(' + subname.Trim() + ")");

            if (name.IndexOf('(') == 0)
                name = name.Trim('(').Trim(')');
            if (nt != null)
                name += "_" + nt;

            string[] res = new string[3] { name, artist, nt };
            return res;

        }

        public Dictionary<string, ResParser> Getinforelease(WebClient client, string url)
        {
            Console.WriteLine("Getinforelease url=" + url);
            Dictionary<string, ResParser> res = new Dictionary<string, ResParser>();
            try
            {
                //using (WebClient client = new WebClient())
                {
                    //client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                    string r = client.DownloadString(url);
                    HAP.HtmlDocument doc = new HAP.HtmlDocument();
                    doc.LoadHtml(r);
                    var doc_node = doc.DocumentNode;
                    var title = doc_node.Descendants("h2").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_titel"))
                        .First().InnerText.Trim();
                    if (title.ToUpper() == "various artists".ToUpper() || title.ToUpper() == "V/A")
                        title = "Various Artists";

                    title = FirstCharToUpper(title);
                    //Console.WriteLine("title=" + title);

                    var artist = doc_node.Descendants("h1").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_artist"))
                        .First().InnerText.Trim();
                    artist = FirstCharToUpper(artist);

                    string artwork = doc_node.Descendants("img").Where(d => d.Attributes.Contains("src") && d.Attributes["src"].Value.Contains("http"))
                        .First().GetAttributeValue("src", "");
                        //soup.find('img', { 'src': re.compile('^http*')}).get('src')
                    //Console.WriteLine("artist=" + artist);

                    var info = doc_node.Descendants("p").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_memo"))
                        .First().InnerText.Trim();

                    var labelpage = doc_node.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_label"));
                    string catno = "";
                    var label = labelpage.First().InnerText.Trim();
                    if (labelpage.Count() > 1)
                    {
                        catno = labelpage.ElementAt(1).InnerText.Trim();
                    }
                    //Console.WriteLine("catno=" + catno);
                    
                    var table = doc_node.Descendants("table").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("txt_memo")).First();
                    var trs = table.Descendants("tr");

                    string genre = "", new_ = "", back = "", sub = "", year = "", grouping = "";

                    List<string> sub_arr = new List<string>();
                    string taggromsrc = "";
                    foreach (var t in trs)
                    {
                        var img_src = t.Descendants("img");
                        if (img_src.Count() == 0 ) continue;
                        var img = img_src.First();
                        taggromsrc = Tagfromsrc(img.GetAttributeValue("src", ""));
                        // sub_arr
                        if (taggromsrc == "sub"){
                            var a = t.Descendants("a");
                            foreach(var i in a)
                            {
                                sub_arr.Add(i.InnerText.Trim());
                            }
                        }
                        //genre
                        if (taggromsrc == "genre")
                        {
                            var tmp = t.Descendants("strong").First().InnerText.Trim();
                            if (sub.IndexOf(tmp) < 0)
                            {
                                genre = tmp;
                            }
                        }
                        //new
                        if (taggromsrc == "new")
                        {
                            new_ = t.InnerText.Trim();
                        }
                        //back
                        if (taggromsrc == "back")
                        {
                            back = t.InnerText.Trim();
                        }
                    }
                    //-------------
                    if (sub_arr.IndexOf(genre) > -1)
                        genre = "";

                    string[] year_arr = new_.Split('.');
                    if (year_arr.Length > 2)
                    {
                        year = year_arr[2];
                        grouping = GROUPING_TEXT + year_arr[2].Substring(2) + year_arr[1];
                    }

                    foreach(var s in sub_arr.Where(s=>s.IndexOf("decks") > -1))
                    {
                        sub += s;
                    }

                    if (sub.Length > 2)
                        sub = sub.Substring(0, sub.Length - 2);

                    //find tracks
                    var tracks = doc_node.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("soundtxt"));
                    if (tracks.Count() == 0)
                        return null;

                    Dictionary<string, string[]> tracks_dict = new Dictionary<string, string[]>();
                    
                    var count_defis = tracks.Where(t => t.InnerText.Trim().Replace("\u0150", "-").Replace("\u0151", "-").IndexOf('-') == -1).Count();
                    bool is_defis = count_defis > 0;

                    /*foreach (var t in tracks)
                    {
                        string tmp_defis = t.InnerText.Trim().Replace("\u0150", "-").Replace("\u0151", "-");
                        if (tmp_defis.IndexOf('-') == -1)
                        {
                            is_defis = false;
                            break;
                        }
                    }*/

                    // ------1-------
                    //return null;

                    //Console.WriteLine("is_defis=" + is_defis.ToString());
                    foreach (var t in tracks)
                    {
                        //Console.WriteLine("track text =" + t.InnerText.Trim());
                        var no_track = (Convert.ToInt32(t.GetAttributeValue("href", "").Split('-').Last()) + 1).ToString();
                        string geturl = Geturl(client, t.GetAttributeValue("href", ""));
                        //*if (url == "http://www.decks.de/t/various_artists-boombox_2/c9s-uq")                   ;

                        tracks_dict.Add(geturl, Get_title_track(t.InnerText.Trim(), is_defis, no_track));
                    }

                    ResParser respar = new ResParser();
                    respar.fields.Add("title", title);
                    respar.fields.Add("artist", artist);
                    respar.fields.Add("catno", catno);
                    respar.fields.Add("artwork", artwork);
                    respar.fields.Add("info", info);
                    respar.fields.Add("label", label);
                    respar.fields.Add("genre", genre);
                    respar.fields.Add("sub", sub);
                    respar.fields.Add("new", new_);
                    respar.fields.Add("back", back);
                    respar.fields.Add("year", year);
                    respar.fields.Add("grouping", grouping);

                    respar.tracks_dict = tracks_dict;

                    res[url] = respar;

                    return res;
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return null;
            }
            
        }

        private void AddLog(string text, bool newline=true)
        {
            string text_ = newline ? text + "\n" : text;
            tblog.Invoke((MethodInvoker)delegate { tblog.AppendText(text_); });
            Console.WriteLine(text_);
        }

        private List<string[]> GetMP3ToInsert()
        {
            List<string[]> urls = new List<string[]>();
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                    connection.Open();
                  
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format("select * from  Songslinks");
                        using (SQLiteDataReader r = command.ExecuteReader())
                            while (r.Read())
                            {
                                string url = Convert.ToString(r["url"]);
                                string category = Convert.ToString(r["category"]);
                                urls.Add(new string[] { url, category });
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);

            }
            return urls;
        }

        private Tuple<HashSet<string>, HashSet<string>> InsertMp3tag(IEnumerable<string[]> urls)
        {
            AddLog("InsertMp3tag   ");
            Tuple<HashSet<string>, HashSet<string>> mp3sinserts = new Tuple<HashSet<string>, HashSet<string>>(new HashSet<string>(), new HashSet<string>());
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            int i = 0;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                foreach (var url in urls)
                {
                    i++;
                    if (i > 100) break;
                    Console.WriteLine("Write mp3 in DB= " + i.ToString());
                    //var info_mp3s = Getinforelease(url.Key);
                    var info_mp3s = Getinforelease(client, url[0]);
                    //continue;
                    if (info_mp3s == null) continue;
                    var info_mp3 = info_mp3s.First();
                    // add mp3 in db
                    //AddLog("        Start Add mp3 in db " + url.Key);


                    var categ_split = url[1].Split('_');
                    string category = categ_split[0];
                    string subcategory = categ_split[1];

                    string fields = string.Join("','", info_mp3.Value.fields.Select(x => x.Value).ToArray());
                    //string strcomand = "INSERT or REPLACE into Mp3tag values(";
                    string strcomand = "('{0}', '{1}', '{2}','"+ fields+"')";
                    //string strcomand = "('{0}', ', @subcategory, @title, @artist, @catno, @artwork, @genre, @sub,@info, @back, @new, @label, @year, @grouping)";
                    strcomand = String.Format(strcomand, url[0], category, subcategory);

                    //Console.WriteLine("strcomand="+ strcomand);
                    mp3sinserts.Item1.Add(strcomand);
                

                    //write tracks

                    var tracks = info_mp3.Value.tracks_dict;
                    foreach (var tr in tracks)
                    {
                        string command_trac = String.Format("('{0}', '{1}', '{2}', '{3}', '{4}')", tr.Key, url[0],
                             string.IsNullOrEmpty(tr.Value[0]) ? tr.Value[0] : tr.Value[0].Replace("'", " "),
                              string.IsNullOrEmpty(tr.Value[0]) ? tr.Value[2] : tr.Value[0].Replace("'", " "),
                              string.IsNullOrEmpty(tr.Value[0]) ? tr.Value[1] : tr.Value[0].Replace("'", " "));
                        //Console.WriteLine("command_trac=" + command_trac);
                        mp3sinserts.Item2.Add(command_trac);
                    }
                    AddLog("Mp3Inserts add " + i.ToString());

                }
            }
            return mp3sinserts;
        }

        private void InsertMp3(List<string> comands_str, string insert_)
        {
            Console.WriteLine("Start InsertMp3 ");
            string strcomand = insert_;
            foreach(var s in comands_str)
            {
                strcomand += s + ",";
            }
            strcomand = strcomand.TrimEnd(',');
            //Console.WriteLine("strcomand=" + strcomand);

            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {
                    connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText = String.Format(strcomand);
                        command.ExecuteNonQuery();
                    }
                }
            
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                ;
            }
        }

        private void AddMp3(string cat)
        {
            Console.WriteLine("Start AddMp3 ");
            AddLog("Старт добавления треков категория "+ cat);
            try
            {
                SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                {

                    connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                    connection.Open();
                    //Dictionary<string, string> urls = new Dictionary<string, string>();
                    List<string[]> urls = new List<string[]>();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        //command.CommandText = String.Format("select * from  Songslinks where category = @category");
                        command.CommandText = String.Format("select * from  Songslinks");
                        //command.Parameters.AddWithValue("@category", cat);
                        using (SQLiteDataReader r = command.ExecuteReader()) 
                            while (r.Read())
                            {
                                string url = Convert.ToString(r["url"]);
                                string category = Convert.ToString(r["category"]);
                                //urls.Add(url, category);
                                urls.Add(new string[] { url, category });
                            }
                    }

                    int i = 0;

                    //Finish:
                    /*try
                    {
                        
                        {
                            using (SQLiteCommand commandMp3tag = new SQLiteCommand(connection))
                            {
                                try
                                {

                                    using (var transaction = connection.BeginTransaction())
                                    //using (System.Transactions.Wo tran = new TransactionScope())
                                    {

                                        using (WebClient client = new WebClient())
                                        {
                                            client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2");
                                            foreach (var url in urls)
                                            {
                                                try
                                                {
                                                    i++;
                                                    //if (i > 100) break;
                                                    Console.WriteLine("Write mp3 in DB= " + i.ToString());
                                                    //var info_mp3s = Getinforelease(url.Key);
                                                    var info_mp3s = Getinforelease(client, url[0]);
                                                    //continue;
                                                    if (info_mp3s == null) continue;
                                                    var info_mp3 = info_mp3s.First();
                                                    // add mp3 in db
                                                    //AddLog("        Start Add mp3 in db " + url.Key);

                                                    /*string category = url.Value.Split('_')[0];
                                                    string subcategory = url.Value.Split('_')[1];/

                                                    var categ_split = url[1].Split('_');
                                                    string category = categ_split[0];
                                                    string subcategory = categ_split[1];

                                                    string strcomand = "INSERT or REPLACE into Mp3tag values(";
                                                    strcomand += "@url, @category, @subcategory, @title, @artist, @catno, @artwork, @genre, @sub,";
                                                    strcomand += "@info, @back, @new, @label, @year, @grouping";
                                                    strcomand += ")";
                                                    commandMp3tag.CommandText = String.Format(strcomand);
                                                    commandMp3tag.Parameters.AddWithValue("@url", url[0]);
                                                    commandMp3tag.Parameters.AddWithValue("@category", category);
                                                    commandMp3tag.Parameters.AddWithValue("@subcategory", subcategory);
                                                    foreach (var f in info_mp3.Value.fields)
                                                    {
                                                        commandMp3tag.Parameters.AddWithValue("@" + f.Key, f.Value);
                                                        Console.WriteLine(f.Key + "= " + f.Value);
                                                    }

                                                    //sqlcommands.Add(commandMp3tag);
                                                    commandMp3tag.ExecuteNonQuery();

                                                    //AddLog("       Добавлен релиз в БД " + i.ToString() + "__" + url.Key);
                                                    AddLog("       Добавлен релиз в БД " + i.ToString() + "__" + url[0]);
                                                    //-----------------------
                                                    //write tracks

                                                    var tracks = info_mp3.Value.tracks_dict;
                                                    foreach (var tr in tracks)
                                                    {
                                                        commandMp3tag.CommandText = String.Format("INSERT or REPLACE into Tracks values(@url, @url_release, @name, @no_track, @track_artist)");
                                                        commandMp3tag.Parameters.AddWithValue("@url", tr.Key);
                                                        commandMp3tag.Parameters.AddWithValue("@url_release", url[0]);
                                                        commandMp3tag.Parameters.AddWithValue("@name", tr.Value[0]);
                                                        //artist
                                                        commandMp3tag.Parameters.AddWithValue("@no_track", tr.Value[2]);
                                                        commandMp3tag.Parameters.AddWithValue("@track_artist", tr.Value[1]);

                                                        //sqlcommands.Add(commandMp3tag);
                                                        commandMp3tag.ExecuteNonQuery();
                                                        //AddLog("        - Добавлен трек в БД " + tr.Key);
                                                    }
                                                    /if (i % 10 == 0)
                                                    {
                                                        AddLog("Commit  " + cat);
                                                        //transaction.Commit();
                                                    }/
                                                }


                                                catch (Exception ex)
                                                {
                                                    Console.WriteLine(ex.Message);
                                                    Console.WriteLine(ex.StackTrace);
                                                    continue;
                                                }

                                            }
                                        }
                                        AddLog("Commit  " + cat);
                                        transaction.Commit();

                                    }


                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    Console.WriteLine(ex.StackTrace);
                
                                }

                            }
                            //---end useing ---
                      
                        }

                        
                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(ex.StackTrace);
   
                    }*/

        }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                ;
            }
            
    
        }

        private void AddSongslinks()
        {
            Console.WriteLine("Start AddSongslinks");
            AddLog("Старт добавление ссылок на релизы");
            AddLog("    Старт получения ссылок");
            var alllinks = Getalllinks();
            AddLog("    Окончание получения ссылок");
            AddLog("    Старт получения ссылок релизов");
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
            {
                connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                connection.Open();

                foreach (var b in alllinks)
                {
                    int k = 0;
                    AddLog("    Добавление ссылок " + b.Key);
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        using (var transaction = connection.BeginTransaction())
                        {
                            foreach (var c in b.Value)
                            {
                                k++;
                                if (IsFirstValues && k > break_value) break;
                                try
                                {
                                    int j = 0;
                                    foreach (var d in c)
                                    {
                                        try
                                        {
                                            if (IsFirstValues && j > break_value) break;
                                            command.CommandText = String.Format("INSERT or REPLACE into Songslinks values(@category, @url, 0)");
                                            command.Parameters.AddWithValue("@category", d);
                                            command.Parameters.AddWithValue("@url", b.Key);
                                            command.ExecuteNonQuery();
                                            Console.WriteLine("    Добавлена ссылка " + d.ToString());
                                            if (j % 50 == 0)
                                                AddLog(".", false);
                                            j++;

                                            //AddLog("    Добавлена ссылка " + d.ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                        }
                                    }
                                    //AddLog("    Добавлено ссылок по категории " + b.Key + " = " + c.Count());
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            transaction.Commit();
                        }
                    }
                    AddLog("");
                    AddLog("    Добавлено ссылок " + b.Key + " =>" + b.Value.Count());
                    //Console.WriteLine("    Добавлено ссылок " + b.Key + " =>" + b.Value.Count());

                }
            }
            AddLog("    Окончание добавления ссылок релизов");
            Console.WriteLine("End AddSongslinks");
            AddLog("Окончание добавления ссылок на релизы");
        }

        public void FullTables()
        {
            try
            {
                /*AddLog("Старт заполнения таблиц");
                SetOld();
                AddSongslinks();
                DeleteOld();*/

                var mp3_to_insert = GetMP3ToInsert();
                //var mp3sinserts = InsertMp3tag(mp3_to_insert);

                List<string> List_mp3_to_insert = new List<string>();
                List<string> List_mp3_to_insert_tracs = new List<string>();
                //AddLog("mp3sinserts counts= "+ mp3sinserts.Count());
                //AddMp3("test");
                List<BackgroundWorker> BWS = new List<BackgroundWorker>();
                var count_ins = mp3_to_insert.Count();
                var step = count_ins / 20;
                foreach (string cat in categories)
                for (int i=0; i< 20; i++)
                {
                    var b = (new BackgroundWorker());
                    b.WorkerSupportsCancellation = true;
                    b.DoWork += delegate {
                        //AddMp3(cat);
                        //var s = mp3_to_insert.Where(m=> m[1] == cat);

                        IEnumerable<string[]> s;
                        if (i ==0 )
                            s = mp3_to_insert.Take(i* step);
                        else
                            s = mp3_to_insert.Skip(step * (i-1)).Take(step * i);

                        var inss = InsertMp3tag(s);
                        var ins_track = InsertMp3tag(s);
                        foreach (var s1 in inss.Item1)
                            List_mp3_to_insert.Add(s1);

                        foreach(var s2 in inss.Item2)
                        {
                            List_mp3_to_insert_tracs.Add(s2);
                        }
                    };
                    b.RunWorkerAsync();
                    BWS.Add(b);
                }
                int count_live = BWS.Count(b => b.IsBusy);
                while (count_live > 0)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Sleep, Count_live= " + count_live.ToString());
                    count_live = BWS.Count(b => b.IsBusy);
                }

                //insert MP3
                InsertMp3(List_mp3_to_insert, "INSERT or REPLACE into Mp3tag values ");

                //insert tracks
                InsertMp3(List_mp3_to_insert_tracs, "INSERT or REPLACE into Tracks values ");

                //, "INSERT or REPLACE into Tracks values "

                //insert mp3tracks


                //AddLog("Count sqlcommands="+sqlcommands.Count().ToString());

                /*foreach (var com in sqlcommands)
                {
                    SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
                    using (SQLiteConnection connection = (SQLiteConnection)factory.CreateConnection())
                    {
                        connection.ConnectionString = @"Data Source = " + Properties.Settings.Default.DefaultDB;
                        connection.Open();
                        com.ExecuteNonQuery();
                    }
                }*/

                AddLog("List_mp3_to_insert count= " + List_mp3_to_insert.Count().ToString());

                AddLog("Окончание заполнения таблиц");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error FullTables: " +ex.Message);
            }
        }
    }
}
