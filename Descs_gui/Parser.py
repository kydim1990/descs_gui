# -*- coding: latin-1 -*-

import requests
from bs4 import BeautifulSoup
from config import TECHNO, HOUSE, BASS, BLACK, MAINHEAD, SITE, FIRSTHEAD, GROUPING_TEXT, URLTRACK, MINIMAL
import re
import ast


headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
session = requests.session()
tnew = MAINHEAD.format(TECHNO['classix']) + '0'

# r=session.get('http://www.decks.de/decks-sess/workfloor/lists/list.php?wo=ten&nowstyle=zz')
# r=session.get('http://www.decks.de/decks-sess/workfloor/lists/list.php?wo=ten&now_Sub=zz&now_Was=news&now_Date=nodate&aktuell=0')
# soup = BeautifulSoup(r.text, "html.parser")


def print_to_file(text):
    try:
        f1 = open('./output.txt', "a")
        f1.writelines(text+"\n")
        f1.close()
    except:
        pass


# get maxpage
def getcountpage(url):
    r = session.get(url, headers=headers)
    # print r.text
    soup = BeautifulSoup(r.text, "html.parser")
    maxpages = soup.find_all('a', {'class': 'pagerLink'})
    if len(maxpages) > 1:
        res = int(maxpages[-2].text) - 1
    else:
        return 0

    return int(res)


# get  info from one page
def getlinksfrompage(url, iscountrelese=False):
    res = []
    r = session.get(url, headers=headers)
    soup = BeautifulSoup(r.text, "html.parser")
    songs = soup.find_all('div', {'class': 'coverBox'})
    for s in songs:
        # print s
        songref = s.find('a')
        song = songref.get('href')
        song = "{0}{1}".format(SITE, song)
        res.append(song)
        # res[s.get('href')]=s.text
    if iscountrelese:
        return len(songs), res
    return res


def getalldefoualurls(isall=False):
    if isall:
        tag = MAINHEAD
    else:
        tag = FIRSTHEAD

    defres = {
        'techno': {'new': tag.format(TECHNO['new']), 'classix': tag.format(TECHNO['classix'])},
        'house': {'new': tag.format(HOUSE['new']), 'classix': tag.format(HOUSE['classix'])},
        'black': {'new': tag.format(BLACK['new']), 'classix': tag.format(BLACK['classix'])},
        'bass': {'new': tag.format(BASS['new']), 'classix': tag.format(BASS['classix'])},
        'minimal': {'new': tag.format(MINIMAL['new']), 'classix': tag.format(MINIMAL['classix'])},
    }

    print 'check allowed of links start'
    for k in defres:
        for i in ('new', 'classix'):
            url = defres[k]
            r = session.get(url[i], headers=headers)
            soup = BeautifulSoup(r.text, "html.parser")
            # print r.text
            vinyls = soup.find('div', {'class': 'allVinyls'})
            if vinyls is None:
                url = defres[k]
                url_db = url[i].replace('list.php', 'list_db.php')
                r = session.get(url_db, headers=headers)
                soup = BeautifulSoup(r.text, "html.parser")
                vinyls = soup.find('div', {'class': 'allVinyls'})

                if vinyls is not None:
                    defres[k][i] = url_db
        print ' {} =  {}'.format(k, defres[k])
    print 'check allowed of links end'
    return defres


def getalllinks():
    alldefs = getalldefoualurls()
    res = {}
    # print alldefs
    # find for each type
    count_ = 0
    for a in alldefs:
        # if a != 'minimal':
        #     continue
        subcateg = alldefs[a]
        for b in subcateg:
            # print a,b
            maxcount = getcountpage(subcateg[b])
            # print maxcount
            resarr = []
            for i in xrange(0, maxcount):
                cururl = "{0}&aktuell={1}".format(alldefs[a][b], i)
                restemp = getlinksfrompage(cururl)
                resarr.append(restemp)
                log = "{0} {1} {2} current {3}".format(count_, a, b, i)
                print log
                print_to_file(log)
                count_ += 1
                # if count_>10:break #test
            res[a+"_"+b] = resarr
    return res


def getinforelease(url):
    res = {}
    r = session.get(url, headers=headers)
    soup = BeautifulSoup(r.text, "html.parser")
    title = soup.find('div', {'class': 'detail_titel'}).text
    if title.upper() == 'various artists'.upper() or title.upper() == 'V/A'.upper():
        title = "Various Artists"
    title = title.title()
    print title
    artist = soup.find('div', {'class': 'detail_artist'}).text.title()
    print "artist=%s" % artist
    artwork = soup.find('img', {'id': 'zoom_front'}).get('src')
    info = soup.find('div', {'id': 'info_memo'}).text.strip()
    labelpage = soup.find('div', {'class': 'detail_label'}).text
    catno = ''
    # print labelpage
    label_split = labelpage.split('/')
    label = label_split[0].strip()
    if len(label_split) > 1:
        catno = label_split[1].strip()
    back = year = grouping = ''
    genre = soup.find('div', {'class': 'LStylehead'}).text.strip()
    sub_arr = [i.text.strip() for i in soup.find_all('div', {'class': 'LStyle'})
               if i.text.strip().upper() != genre.upper()]
    sub = ",".join(sub_arr)
    sub_arr = []
    info_date = soup.find('div', {'class': 'info_dateformat'})
    infodateval = info_date.find_all('div', {'class': 'infodateval'})
    new = infodateval[0].text.strip()
    if len(infodateval) > 1:
        back = infodateval[1].text.strip()
    print "sub_arr=%s " % sub_arr
    year_arr = new.split('.')
    if len(year_arr) > 2:
        year = year_arr[2]
        grouping = GROUPING_TEXT+year_arr[2][2:]+year_arr[1]
    # find tracks
    tracks = soup.find_all('div', {'class': 'listenMaxWidth'})
    # print tracks
    if len(tracks) == 0:
        return None
    tracks_dict = {}
    is_defis = True
    for t in tracks:
        inner_text = t.find_all(text=True)[-1]
        tmp_defis = inner_text.replace(u'�', '-').replace(u'\x96', '-').replace(u'\x97', '-')
        if tmp_defis.find('-') == -1:
            is_defis = False
            break
    print "is_defis=%s" % is_defis
    for t in tracks:
        inner_text = t.find_all(text=True)[-1]
        span_res = t.find('span', {'class': 'RecPos'}).text.strip()
        # print "track text=%s" % inner_text
        no_track = int(t.get('data-track')) + 1
        # no_track = str(int(t.get('href').rsplit('-', 1)[1]) + 1)
        # print no_track
        # print t.get('href')
        url_track = geturl(t.find('a').get('href'), no_track)
        no_track = str(no_track)
        title_tr = get_title_track(inner_text, is_defis, no_track, span_res=span_res, artist_rel=artist)
        tracks_dict[url_track] = (title_tr, no_track)
    print tracks_dict
    res[url] = {"title": title, "artist": artist, "artwork": artwork, "info": info, "catno": catno, "label": label,
                "genre": genre, "sub": sub, "new": new, "back": back, "year": year,
                "grouping": grouping, "tracks_dict": tracks_dict}
    # print tracks_dict
    return res


def tagfromsrc(s):
    a = s.split('/')[-1]
    b = a.split('_')[-1]
    c = b.split('.')[0]
    return c


def geturl(text, no_track):
    print text
    s_text = text.rsplit('/', 1)[1].rsplit('-', 1)
    # print 's_text=%s' % s_text
    num = str(int(s_text[1])+1)
    url = URLTRACK.format(s_text[0], num)
    # print url
    resp = session.get(url, headers=headers).text
    # print resp
    res = ast.literal_eval(resp)
    hrefmp3 = [s for i, s in enumerate(res["sound"]) if i < no_track]
    # print res["sound"]
    sound = ','.join(hrefmp3)
    # res = res["sound"][0]
    result = sound
    result = sound.replace('\\', '')
    return result


def get_title_track(text, is_defis=True, no_track=None, span_res=None, artist_rel=None):
    artist = artist_rel
    name = text.replace(u'�', '-').replace(u'\x96', '-').replace(u'\x97', '-')

    if name.find('-') > -1 and is_defis:
        tmp = name[name.find('-')+1:]
        artist = name[:name.find(tmp)-2].strip().title()
        name = name[name.find('-')+1:]

    if len(span_res) > 0:
        name = '{0} ({1})'.format(name.encode('utf-8'), span_res.encode('utf-8'))

    res = (name, artist)
    try:
        print "name = {}, artist= {}".format(name, artist.encode('utf-8'))
    except (UnicodeEncodeError, UnicodeDecodeError):
        print 'name or artist has difficult symbols'
    return res


if __name__=="__main__":
    # r=session.get('http://www.decks.de/t/osunlade-envision_remixes/bz8-er')
    # soup = BeautifulSoup(r.text, "html.parser")
    # tracks=soup.find_all('a',{'class':'soundtxt'})
    # for t in tracks:
    #     print   findhttpinonckick(t.get('onclick')), t.text
    # test='1. a1: the build up'
    # print get_title_track('Fixmer / Mccarthy')
    url = 'https://www.decks.de/t/v-a-gold_edition_vol_2/bt5-t0'
    url_for_get = 'https://www.decks.de/play/c9l-08-1'
    url_for_get_kink = 'http://www.decks.de/decks/workfloor/lists/list_db.php?wo=hon&now_Sub=zz&aktuell=0'
    # url = 'http://www.decks.de/t/omar_s-side_trakx_vol_1/blf-2e'
    # print r.text
    getinforelease(url)
    # test='http://www.decks.de/t/various_artists-sound_sampler_vol_1/byq-wr'
    # getinfoRelease(test)
    # geturl(url_for_get)
    # print getalllinks()
    url_page = 'http://www.decks.de/decks/workfloor/lists/list_db.php?wo=min&now_Sub=zz'
    # print getcountpage(url_page)
    # print getalldefoualurls()
    # print getlinksfrompage(url_for_get_kink)