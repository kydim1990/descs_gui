﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Descs_gui.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.1.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string DefaultDB {
            get {
                return ((string)(this["DefaultDB"]));
            }
            set {
                this["DefaultDB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("10000000")]
        public int Current_track {
            get {
                return ((int)(this["Current_track"]));
            }
            set {
                this["Current_track"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\"C:\\\\Python27\\\\python.exe\"")]
        public string Python_path {
            get {
                return ((string)(this["Python_path"]));
            }
            set {
                this["Python_path"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Main.py")]
        public string Main_parser {
            get {
                return ((string)(this["Main_parser"]));
            }
            set {
                this["Main_parser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("MP3_Songs")]
        public string Path_out_MP3 {
            get {
                return ((string)(this["Path_out_MP3"]));
            }
            set {
                this["Path_out_MP3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ShowConsolePython {
            get {
                return ((bool)(this["ShowConsolePython"]));
            }
            set {
                this["ShowConsolePython"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool CloasePythonAfterFinish {
            get {
                return ((bool)(this["CloasePythonAfterFinish"]));
            }
            set {
                this["CloasePythonAfterFinish"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool isSync {
            get {
                return ((bool)(this["isSync"]));
            }
            set {
                this["isSync"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("05:35:00")]
        public global::System.TimeSpan SyncTime {
            get {
                return ((global::System.TimeSpan)(this["SyncTime"]));
            }
            set {
                this["SyncTime"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Autorefresh_isshoemwessages {
            get {
                return ((bool)(this["Autorefresh_isshoemwessages"]));
            }
            set {
                this["Autorefresh_isshoemwessages"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::System.DateTime LastUpdate {
            get {
                return ((global::System.DateTime)(this["LastUpdate"]));
            }
            set {
                this["LastUpdate"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("\"\"")]
        public string Path_from_list {
            get {
                return ((string)(this["Path_from_list"]));
            }
            set {
                this["Path_from_list"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("MP3_Songs")]
        public string Path_from_list_dir {
            get {
                return ((string)(this["Path_from_list_dir"]));
            }
            set {
                this["Path_from_list_dir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Predlisten_default {
            get {
                return ((string)(this["Predlisten_default"]));
            }
            set {
                this["Predlisten_default"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Tags_defalt {
            get {
                return ((string)(this["Tags_defalt"]));
            }
            set {
                this["Tags_defalt"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Result_folder {
            get {
                return ((string)(this["Result_folder"]));
            }
            set {
                this["Result_folder"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:/")]
        public string Ignore_folder {
            get {
                return ((string)(this["Ignore_folder"]));
            }
            set {
                this["Ignore_folder"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UseNewVersionParsing {
            get {
                return ((bool)(this["UseNewVersionParsing"]));
            }
            set {
                this["UseNewVersionParsing"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool UseNewVersionParsingAuto {
            get {
                return ((bool)(this["UseNewVersionParsingAuto"]));
            }
            set {
                this["UseNewVersionParsingAuto"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("charts.py")]
        public string Chart_parser {
            get {
                return ((string)(this["Chart_parser"]));
            }
            set {
                this["Chart_parser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::System.DateTime ChartLastUpdate {
            get {
                return ((global::System.DateTime)(this["ChartLastUpdate"]));
            }
            set {
                this["ChartLastUpdate"] = value;
            }
        }
    }
}
