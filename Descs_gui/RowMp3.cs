﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descs_gui
{
    class RowMp3
    {
        public string url, url_rel, new_, artwork, title, artist, genres, sub, info_, name, catno,
            label, grouping;
        public uint year;

        public RowMp3(string _url, string _url_rel, string _new_, string _artwork, string _title, string _artist, string _genres, string _sub,
            string _info_, string _name, string _catno, string _label, string _grouping, uint _year)
        {
            url = _url;
            url_rel = _url_rel;
            new_ = _new_;
            artwork = _artwork;
            title = _title;
            artist = _artist;
            genres = _genres;
            sub = _sub;
            info_ = _info_;
            name = _name;
            catno = _catno;
            label = _label;
            grouping = _grouping;
            year = _year;
        }

    }
}
