﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Descs_gui
{
    class Track
    {
        private string url;
        private string name;
        private string artist;
        private int no_track;

        public string Url
        {
            get { return url; }
        }


        public string Name
        {
            get { return name; }
        }

        public string Artist
        {
            get { return artist; }
        }

        public int No_track
        {
            get { return no_track; }
        }

        public Track(string url_, string name_, string artist_, int no_track_)
        {
            url = url_;
            name = name_;
            artist = artist_;
            no_track = no_track_;
        }
    }
}
