# -*- coding: utf-8 -*-

import requests
from sqlalchemy import create_engine, delete, update
from sqlalchemy.orm import sessionmaker
from db import Base, Charttag, Charttracks, change_status
from config import CHARTS_URL_MASK, CHARTS_GENRE, CHARTS_SUB
from Parser import getlinksfrompage, getinforelease
import os
import traceback
from datetime import datetime

fmt = '%Y-%m-%d %H:%M:%S'

database_file = os.path.join(os.path.dirname(__file__), 'Deck.db')
engine = create_engine('sqlite:///%s' % database_file,)
engine.raw_connection().connection.text_factory = str
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session_db = DBSession()

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
session = requests.session()


def getrelease_chart():
    print 'chart start'
    releaese = {}
    for i in CHARTS_GENRE:
        for j in CHARTS_SUB:
            url = CHARTS_URL_MASK.format(j, i)
            print url
            getlfrom = getlinksfrompage(url)
            releaese[url] = getlfrom
            for pos, url_rel in enumerate(getlfrom):
                print 'category ={0} ; sub = {1}, {2} - write in db {3}'.format(i, j, pos, url_rel)
                writemp3_indb(url_rel, i, j, pos)
                # r = session.get(url_tr, headers=headers)
                # soup = BeautifulSoup(r.text, "html.parser")
                # tracks = soup.find_all('div', {'class': 'listenMaxWidth'})

    # print 'count_rel={0}, count_tracks={1}'.format(count_rel, count_tracks)
    # return count_rel, count_tracks
    print 'chart end'
    return len(releaese[url])


def writemp3_indb(url, category, subcategory, chartpos):
    charttag_exists = Charttag.query.filter_by(url=url).first()
    if charttag_exists:
        charttag_exists.isold = False
        if charttag_exists.category == category and charttag_exists.subcategory == subcategory\
                and charttag_exists.chartpos != chartpos:
            charttag_exists.chartpos = chartpos
        try:
            session_db.merge(charttag_exists)
            session_db.commit()
        except Exception, e:
            print 'error commit charttag_exists'
            print str(e)
            session_db.rollback()
        finally:
            return

    info_mp3 = getinforelease(url)
    # print info_mp3
    if info_mp3 is None:
        return

    for inm, rowmp3 in info_mp3.iteritems():
        url_ = inm
        category_ = category
        subcategory_ = subcategory
        title_ = rowmp3['title']
        artist_ = rowmp3['artist']
        artwork_ = rowmp3['artwork']
        catno_ = rowmp3['catno']
        genre_ = rowmp3['genre']
        sub_ = rowmp3['sub']
        info_ = rowmp3['info']
        back_ = rowmp3['back']
        new_ = rowmp3['new']
        label_ = rowmp3['label']
        year_ = rowmp3['year']
        grouping_ = rowmp3['grouping']
        # add Charttag in db

        title_tmp = title_
        artist_rmp = artist_
        try:
            title_sql = title_tmp.decode('utf8')
            artist_sql = artist_rmp.decode('utf8')
        except:
            title_sql = title_tmp
            artist_sql = artist_rmp

        mp3row = Charttag(url=url_, category=category_, subcategory=subcategory_, title=title_sql, artist=artist_sql,
                        catno=catno_,
                        artwork=artwork_, genre=genre_, sub=sub_, info=info_, back=back_, new=new_, label=label_,
                        year=year_,
                        grouping=grouping_, chartpos=chartpos)

        try:
            session_db.merge(mp3row)
        except:
            session_db.rollback()
            continue
        # write tracks
        tracks_d = rowmp3['tracks_dict']
        # print tracks_d
        for tr in tracks_d:
            # print tr
            name_tmp = tracks_d[tr][0][0]
            track_artist_rmp = tracks_d[tr][0][1]
            try:
                name_sql = name_tmp.decode('utf8')
                track_artist_sql = track_artist_rmp.decode('utf8')
            except:
                name_sql = name_tmp
                track_artist_sql = track_artist_rmp

            track_row = Charttracks(url=tr, url_release=url_, name=name_sql, no_track=tracks_d[tr][1], track_artist=track_artist_sql)

            try:
                session_db.merge(track_row, unicode)
                # session.commit()
            except Exception, e:
                print str(e.message)
                session_db.rollback()
    else:
        pass
    try:
        session_db.commit()
    except Exception, e:
        print 'error commit'
        print str(e)
        session_db.rollback()


def setoldsongs():
    session = DBSession()
    s_upd = update(Charttag).values(isold=True)
    session.execute(s_upd)
    try:
        session.commit()
    except Exception, e:
        print 'error commit'
        print str(e)
        session_db.rollback()


def deleteold():
    links_to_delete = Charttag.query.filter_by(isold=True)
    links_to_delete_url = links_to_delete.with_entities(Charttag.url)
    track_to_delete = delete(Charttracks).where(Charttracks.url_release.in_(links_to_delete_url))

    session_db.execute(track_to_delete)
    Charttag_to_delete = delete(Charttag).where(Charttag.isold)
    session_db.execute(Charttag_to_delete)

    try:
        session_db.commit()
    except:
        print 'error commit DeleteOld Chart'
        if session_db.is_active:
            session_db.rollback()


def update_chart():
    try:
        change_status(oper='chart', status='running')
        setoldsongs()
        getrelease_chart()
        deleteold()
        change_status(oper='chart', status='finish')
    except Exception, e:
        print 'error update_chart'
        print traceback.format_exc()
        print str(e)


if __name__ == '__main__':
    url_test = 'http://www.decks.de/decks/workfloor/lists/findTrack.php?code=cab-84'
    d1 = datetime.now()
    update_chart()
    # deleteold()
    # writemp3_indb(url_test, 'test', 'test')
    d2 = datetime.now()
    print "Scrapping has been finished, time ", d2-d1
