# -*- coding: latin-1 -*-
TECHNO = {"new": 'ten', 'classix': 'tec'}
HOUSE = {"new": 'hon', 'classix': 'hoc'}
BASS = {"new": 'ban', 'classix': 'bac'}
BLACK = {"new": 'bln', 'classix': 'blc'}
MINIMAL = {"new": 'min', 'classix': 'mic'}

SITE = 'http://www.decks.de'

MAINHEAD = 'http://www.decks.de/decks/workfloor/lists/list.php?wo={0}&now_Sub=zz&now_Was=news&now_Date=nodate&aktuell='
FIRSTHEAD = 'http://www.decks.de/decks/workfloor/lists/list.php?wo={0}&now_Sub=zz'
FINDTEG = 'CATEGORYNAME'
URLTRACK = 'http://www.decks.de/decks/rpc/getAudio.php?client=none&id={0}&t={1}'

GROUPING_TEXT = "decks.de pre-listen "

CHARTS_URL_MASK = 'http://www.decks.de/decks/workfloor/charts/charts.php?was={0}&wo={1}'
CHARTS_GENRE = ['techno', 'minimal', 'house']
CHARTS_SUB = ["buzz", "sales", "presale", "alltime"]
