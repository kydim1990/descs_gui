# -*- coding: latin-1 -*-
import os
from sqlalchemy import Column, ForeignKey, Integer, String, Float,DATE,BOOLEAN
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

database_file = os.path.join(os.path.dirname(__file__), 'Deck.db')
engine = create_engine('sqlite:///%s' % database_file)
engine.raw_connection().connection.text_factory = str
db_session = scoped_session(sessionmaker(autocommit=True, autoflush=False, bind=engine))
Base = declarative_base()


# Songslinks
class Songslinks(Base):
    __tablename__ = 'Songslinks'
    url = Column(String(100), primary_key=True)
    category = Column(String(200), nullable=True)
    isold = Column(BOOLEAN, nullable=True)


# Mp3tag
class Mp3tag(Base):
    __tablename__ = 'Mp3tag'
    # id=Column(Integer,  autoincrement=True)
    url = Column(String(200), nullable=True,primary_key=True)
    category = Column(String(20), nullable=True)
    subcategory = Column(String(20), nullable=True)
    title = Column(String(100), nullable=True)
    artist = Column(String(100), nullable=True)
    artwork = Column(String(100), nullable=True)
    genre = Column(String(100), nullable=True)
    sub = Column(String(100), nullable=True)
    info = Column(String(10000), nullable=True)
    back = Column(String(100), nullable=True)
    new = Column(String(100), nullable=True) #Sort composer
    catno = Column(String(100), nullable=True) #sort album
    label = Column(String(100), nullable=True)
    year = Column(String(100), nullable=True)
    grouping = Column(String(100), nullable=True)


# Tracks
class Tracks(Base):
    __tablename__ = 'Tracks'
    url = Column(String(200), primary_key=True)
    url_release = Column(String(200), nullable=True)
    name = Column(String(200), nullable=True)
    no_track = Column(String(200), nullable=True)
    track_artist = Column(String(200), nullable=True)


# Chart
class Charttag(Base):
    __tablename__ = 'Charttag'
    # id=Column(Integer,  autoincrement=True)
    url = Column(String(200), nullable=True,primary_key=True)
    category = Column(String(20), nullable=True)
    subcategory = Column(String(20), nullable=True)
    title = Column(String(100), nullable=True)
    artist = Column(String(100), nullable=True)
    artwork = Column(String(100), nullable=True)
    genre = Column(String(100), nullable=True)
    sub = Column(String(100), nullable=True)
    info = Column(String(10000), nullable=True)
    back = Column(String(100), nullable=True)
    new = Column(String(100), nullable=True) #Sort composer
    catno = Column(String(100), nullable=True) #sort album
    label = Column(String(100), nullable=True)
    year = Column(String(100), nullable=True)
    grouping = Column(String(100), nullable=True)
    isold = Column(BOOLEAN, nullable=True, default=False)
    chartpos = Column(Integer, nullable=True)


# Charttracks
class Charttracks(Base):
    __tablename__ = 'Charttracks'
    url = Column(String(200), primary_key=True)
    url_release = Column(String(200), nullable=True)
    name = Column(String(200), nullable=True)
    no_track = Column(String(200), nullable=True)
    track_artist = Column(String(200), nullable=True)


class Info(Base):
    __tablename__ = 'Info'
    oper = Column(String(200), primary_key=True)
    status = Column(String(200), nullable=True)


def change_status(oper, status):
    DBSession = sessionmaker(bind=engine)
    session_db = DBSession()
    info_update = Info.query.filter_by(oper=oper).first()
    info_update.status = status
    try:
        session_db.merge(info_update)
        session_db.commit()
    except Exception, e:
        print str(e.message)
        session_db.rollback()
    session_db.close()


# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
Base.query = db_session.query_property()


if __name__ == '__main__':
    DBSession = sessionmaker(bind=engine)
    session_db = DBSession()

    main_parcer = Info(oper='main', status='init')
    chart = Info(oper='chart', status='init')

    try:
        session_db.merge(main_parcer)
        session_db.merge(chart)
        session_db.commit()
    except Exception, e:
        print str(e.message)
        session_db.rollback()
